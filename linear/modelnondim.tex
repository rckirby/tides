\section{Description of finite element tidal model}
\label{se:model}
We start with the nondimensional linearized rotating shallow water
model with linear drag and forcing on a (possibly curved) two
dimensional surface $\Omega$, given by
\begin{equation}
\begin{split}
u_t + \frac{f}{\epsilon} u^\perp + \frac{\beta}{\epsilon^2} \nabla
\left( \eta - \eta^\prime \right) + C u & = 0, \\
\eta_t + \nabla \cdot \left( H u \right) & = 0,
\end{split}
\end{equation}
where $u$ is the nondimensional two dimensional velocity field tangent
to $\Omega$, $u^\perp=(-u_2,u_1)$ is the velocity rotated by $\pi/2$, $\eta$ is the nondimensional free surface elevation above
the height at state of rest, $\nabla\eta'$ is the (spatially varying)
tidal forcing, $\epsilon$ is the Rossby number (which is small for
global tides), $f$ is the spatially-dependent non-dimensional Coriolis
parameter which is equal to the sine of the latitude (or which can be
approximated by a linear or constant profile for local area models),
$\beta$ is the Burger number (which is also small), $C$ is the (spatially varying) nondimensional drag
coefficient and $H$ is the (spatially varying) nondimensional fluid
depth at rest, and $\nabla$ and $\nabla\cdot$ are the intrinsic
gradient and divergence operators on the surface $\Omega$,
respectively.

We will work with a slightly generalized version of the forcing term,
which will be necessary for our later error analysis.  Instead of
assuming forcing of the form 
$\frac{\beta}{\epsilon^2} \nabla \eta^\prime$, 
we assume some $F \in L^2$, giving our model as
\begin{equation}
\begin{split}
u_t + \frac{f}{\epsilon} u^\perp + \frac{\beta}{\epsilon^2} \nabla
 \eta  + C u & = F, \\
\eta_t + \nabla \cdot \left( H u \right) & = 0.
\end{split}
\end{equation}


It also becomes useful to work in terms of the linearized momentum
$\widetilde{u} = H u$ rather than velocity. After making this substitution and dropping the
tildes, we obtain
\begin{equation}
\begin{split}
\frac{1}{H}u_t + \frac{f}{H\epsilon} u^\perp + \frac{\beta}{\epsilon^2} \nabla
\eta  + \frac{C}{H} u & = F, \\
\eta_t + \nabla \cdot  u & = 0.
\end{split}
\label{eq:thepde}
\end{equation}
A natural weak formulation of this equations is to seek $u \in \hdiv$
and $\eta \in L^2$ so that
\begin{equation}
\begin{split}
\left( \frac{1}{H}u_t , v \right) 
+ \frac{1}{\epsilon} \left( \frac{f}{H} u^\perp , v \right) 
- \frac{\beta}{\epsilon^2} \left( \eta ,
\nabla \cdot v \right) + \left( \frac{C}{H} u , v \right) & = 
\left( F , v \right)
, \quad \forall v \in \hdiv, \\
\left( \eta_t , w \right) + \left( \nabla \cdot u  , w \right)& = 0, \quad
\forall w \in L^2.
\end{split}
\label{eq:mixed}
\end{equation}
%For the purposes of our analysis, it will also be useful if we do not
%restrict the forcing term to take the form of a gradient, and hence
%will replace $\nabla\eta'$ with some prescribed vector-valued function
%$F: \Omega \rightarrow \left( L^2 \right)^2$.  Our equations become
%\begin{equation}
%\begin{split}
%\left( \frac{1}{H}u_t , v \right) 
%+ \frac{1}{\epsilon} \left( \frac{f}{H} u^\perp , v \right) 
%- \frac{\beta}{\epsilon^2} \left( \eta ,
%\nabla \cdot v \right) + \left( \frac{C}{H} u , v \right) & = 
%\left( F , v \right)
%, \quad \forall v \in \hdiv, \\
%\left( \eta_t , w \right) + \left( \nabla \cdot u  , w \right)& = 0,
%\quad \forall w \in L^2.
%\end{split}
%\end{equation}


We now develop mixed discretizations with $V_h \subset \hdiv$ and $W_h
\subset L^2$.  Conditions on the spaces are the commuting projection
and divergence mapping $V_h$ onto $W_h$. We define $u_h \subset V_h$
and $\eta_h \subset W_h$ as solutions of the discrete variational
problem
\begin{equation}
\begin{split}
\left( \frac{1}{H}u_{h,t} , v_h \right) 
+ \frac{1}{\epsilon} \left( \frac{f}{H} u_h^\perp , v_h \right) 
- \frac{\beta}{\epsilon^2} \left( \eta_h ,
\nabla \cdot v_h \right) + \left( \frac{C}{H} u_h , v_h \right) & =
\left( F , v_h \right)
, \\
\left( \eta_{h,t} , w_h \right) + \left( \nabla \cdot u_h  , w_h \right)& = 0.
\end{split}
\label{eq:discrete_mixed}
\end{equation}

We will eventually obtain stronger estimates by working with an
equivalent second-order form.  
If we take the time derivative of the
first equation in~(\ref{eq:discrete_mixed}) and use the fact that
$\nabla \cdot V_h = W_h$, we have
\begin{equation}
\left( \frac{1}{H}u_{h,tt} , v_h \right) 
+ \frac{1}{\epsilon} \left( \frac{f}{H} u_{h,t}^\perp , v_h \right) 
+ \frac{\beta}{\epsilon^2} \left( \nabla \cdot u_h , \nabla \cdot v_h \right)
+ \left( \frac{C}{H} u_{h,t} , v_h \right)  = 
\left( \widetilde{F} ,
v_h \right),
\label{eq:secondorderdiscrete}
\end{equation}
where $\widetilde{F} = F_t$.  This is a restriction of
\begin{equation}
\left( \frac{1}{H}u_{tt} , v \right) 
+ \frac{1}{\epsilon} \left( \frac{f}{H} u_{t}^\perp , v \right) 
+ \frac{\beta}{\epsilon^2} \left( \nabla \cdot u , \nabla \cdot v \right)
+ \left( \frac{C}{H} u_t , v \right)  = 
\left( \widetilde{F} ,
v_h \right),
\end{equation}
which is the variational form of
\begin{equation}
\frac{1}{H} u_{tt} + \frac{f}{H} u^\perp_t 
- \frac{\beta}{\epsilon^2} \nabla \left( \nabla
\cdot u \right)
+ \frac{C}{H} u_t = \widetilde{F},
\end{equation}
to the mixed finite element spaces. Note that here, we have taken the
time derivative at the discrete level, and so this is an equivalent
formulation to~(\ref{eq:discrete_mixed}), making use of the compatible
spaces, and so the problems associated with discretising the equations
in wave equation form (such as the methods described in
\cite{LyGr79,KiGr85}) do not arise.

We have already discussed mixed finite elements' application to tidal
models in the geophysical literature, but this work also builds on 
existing literature for mixed discretization of the acoustic
equations. The first such investigation is due to
Geveci~\cite{geveci1988application}, where exact energy conservation
and optimal error estimates are given for the semidiscrete first-order
form of the model wave equation.  Later
analysis~\cite{cowsar1990priori,jenkins2003priori} considers a second 
order in time wave equation with an auxillary flux at each time step.
In~\cite{kirbykieu}, Kirby and Kieu return to the first-order
formulation, giving additional estimates
beyond~\cite{geveci1988application} and also analyzing the symplectic
Euler method for time discretization.  From the standpoint of this
literature, our model~(\ref{eq:thepde}) appends additional terms for
the Coriolis force and damping to the simple acoustic model.
We restrict ourselves to semidiscrete analysis in this work, but pay
careful attention the extra terms in our estimates, showing how study
of an equivalent second-order equation in $\hdiv$ proves proper
long-term behavior of the model.
