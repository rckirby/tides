\section{Energy estimates}
\label{se:energy}
%Before turning to stability of the time-dependent problem, we first
%handle the steady problem~\eqref{eq:elliptic}.  By restricting to
%$\nabla \cdot u_h = 0$ and selecting $v_h = u_h$, we see that the skew
%term $(u_h^\perp, u_h$ vanishes and so the operator is coercive on the
%divergence-free subspace.  Moreover, we will give full $L^2$ stability
%of this operator by means of a duality argument.
In this section, we develop in stability estimates for our system, obtained by energy techniques.
Supposing that there is no forcing
or damping ($F = C = 0$), we pick $v_h = u_h$ and 
$w_h =\frac{\beta}{\epsilon^2} \eta_h$ 
in~(\ref{eq:discrete_mixed}), and find that
\begin{equation}
\begin{split}
\left( \frac{1}{H} u_{h,t} , u_h \right)
+ \frac{1}{\epsilon} \left( \frac{f}{H} u_h^\perp , u_h \right)
- \frac{\beta}{\epsilon^2}\left( \eta_h , \nabla \cdot u_h \right) & = 0, \\
\frac{\beta}{\epsilon^2} \left( \eta_{h,t} , \eta_h \right)
+ \frac{\beta}{\epsilon^2} \left( \nabla \cdot u_h , \eta_h \right) &
= 0.
\end{split}
\end{equation}
Since $u_h^\perp \cdot u_h = 0$ pointwise, we add these two
equations together to find
\begin{equation}
\frac{1}{2} \frac{d}{dt} \weightednorm{u_h}{\frac{1}{H}}^2 
+ \frac{\beta}{2\epsilon^2} \frac{d}{dt} \norm{\eta_h}^2 = 0.
\end{equation}
Hence, we have the following.
\begin{proposition}
In the absence of damping or forcing, the quantity
\begin{equation}
E_1(t) = \frac{1}{2} \weightednorm{ u_h}{\frac{1}{H}}^2 
+ \frac{\beta}{2\epsilon^2} \norm{\eta_h}^2 
\label{eq:firstorderenergy}
\end{equation}
is conserved exactly for all time.
\label{prop:cons}
\end{proposition}

Now suppose that $F = 0$ still but that $0 < C_* \leq C \leq C <
\infty$ pointwise in $\Omega$.  The same considerations now lead
to
\begin{equation}
\frac{1}{2} \frac{d}{dt} \weightednorm{u_h}{\frac{1}{H}}^2
+ \frac{\beta}{2\epsilon^2} \frac{d}{dt} \norm{\eta_h}^2 
+ \weightednorm{u_h}{\frac{C}{H}}^2 = 0,
\end{equation}
so that
\begin{proposition}
In the absence of forcing, but with $0 < C_* \leq C \leq C < \infty$,
the quantity $E_1(t)$ defined in~(\ref{eq:firstorderenergy})
satisfies
\[
\frac{d}{dt} E_1(t) \leq 0.
\]
\label{prop:monotonic}
\end{proposition}

In the presence of forcing and dissipation, it is also possible to
make estimates showing worst-case linear accumulation of the energy
over time. 
\begin{proposition}
\label{prop:firstorderstability}
With nonzero $F$, we have that for all time $t$,
\begin{equation}
E_1(t) \leq E_1(0) + \frac{1}{2C_*} \int_0^t \weightednormattime{F}{H}{s}^2 ds
\end{equation}
\end{proposition}
\begin{proof}
We choose $w_h$ and $v_h$ as without forcing, and find that
\[
\frac{d}{dt} E_1(t) + \weightednormattime{u}{\frac{C}{H}}{t}^2 
= \left( F , u_h \right).
\]
Cauchy-Schwarz, Young's inequality, and norm equivalence give
\[
\frac{d}{dt} E_1(t)
+ \frac{C_*}{2} \weightednormattime{u_h}{\frac{1}{H}}{t}^2
\leq
\frac{1}{2C_*} \weightednormattime{F}{H}{t}^2 
\]
The result follows by dropping the positive term from the left-hand side and integrating.
\end{proof}

However, linear energy accumulation is not observed for actual tidal
motion, so we expect a stronger result to hold.  Turning to the second
order equation~(\ref{eq:secondorderdiscrete}), we 
begin with vanishing forcing and damping terms, putting $v_h = u_{h,t}$
to find
\begin{equation}
\left( \frac{1}{H}u_{h,tt} , u_{h,t} \right) 
+ \frac{1}{\epsilon} \left( \frac{f}{H} u_{h,t}^\perp , u_{h,t} \right) 
+ \frac{\beta}{\epsilon^2} \left( \nabla \cdot u_h , \nabla \cdot u_{h,t} \right)
= 0,
\end{equation}
which simplifies to
\begin{equation}
\frac{1}{2} \frac{d}{dt} \weightednorm{ u_{h,t}}{\frac{1}{H}}^2
+ \frac{\beta}{2\epsilon^2} \frac{d}{dt} \norm{\nabla \cdot u_h}^2  = 0,
\end{equation}
so that the quantity
\begin{equation}
E(t) = \frac{1}{2} \weightednorm{ u_{h,t}}{\frac{1}{H}}^2
+ \frac{\beta}{2\epsilon^2} \norm{\nabla \cdot u_h}^2
\label{eq:Edef}
\end{equation}
is conserved exactly for all time.

If $C$ is nonzero, we have that
\begin{equation}
\frac{1}{2} \frac{d}{dt} \weightednorm{ u_{h,t}}{\frac{1}{H}}^2
+ \frac{\beta}{2\epsilon^2} \frac{d}{dt} \norm{\nabla \cdot u_h}^2  
+ \weightednorm{u_{h,t}}{\frac{C}{H}}^2 = 0,
\end{equation}
which implies that $E(t)$ is nonincreasing, although with no
particular decay rate. 

Now, we develop more refined technique based on the Helmholtz decomposition
that gives a much stronger damping result.  We can write
$u_h = u_h^D + u_h^S$ in the $\frac{1}{H}$-weighted decomposition.
We let $0 < \alpha$ be a scalar to be determined later and let
the test function $v$ in~(\ref{eq:secondorderdiscrete}) be
$v_h = u_{h,t} + \alpha u^D_h$.  This gives
\begin{equation}
\begin{split}
\left( \frac{1}{H}u_{h,tt} ,  u_{h,t} + \alpha u^D_h \right) 
+ \frac{1}{\epsilon} \left( \frac{f}{H} u_{h,t}^\perp ,  u_{h,t} + \alpha u^D_h \right) & \\
+ \frac{\beta}{\epsilon^2} \left( \nabla \cdot u_h , \nabla \cdot
  \left(  u_{h,t} + \alpha u^D_h \right) \right)
+ \left( \frac{C}{H} u_{h,t} ,  u_{h,t} + \alpha u^D_h \right)  & = 0,
\end{split}
\end{equation}
and we rewrite the left-hand side so that
\begin{equation}
\begin{split}
\frac{1}{2} \frac{d}{dt} \weightednorm{ u_{h,t} }{\frac{1}{H}}^2
+ \alpha \left( \frac{1}{H} u_{h,tt} , u_h^D \right)
+ \frac{\alpha}{\epsilon} \left( \frac{f}{H} u_{h,t}^\perp , u_h^D \right) & \\
+ \frac{\beta}{2 \epsilon^2} \frac{d}{dt} \norm{\nabla \cdot u_h^D}^2
+ \frac{\alpha \beta}{\epsilon^2} \norm{ \nabla \cdot u_h^D }^2
+ \weightednorm{ u_{h,t} }{\frac{C}{H}}^2 
+ \alpha \left( \frac{C}{H}  u_{h,t} , u_{h}^D \right) & = 0.
\end{split}
\end{equation}
We use the fact that
\[
\frac{d}{dt} \left( \frac{1}{H} u_{h,t} , u_h^D \right)
= \left( \frac{1}{H} u_{h,tt} , u_h^D \right)
+ \left( \frac{1}{H} u_{h,t} , u_{h,t}^d \right)
\]
and also that $u_h^S$ is $\frac{1}{H}$-orthogonal to $u_h^D$
to rewrite the left-hand side as
\begin{equation}
\begin{split}
\frac{d}{dt}
\left[
\frac{1}{2} \weightednorm{u_{h,t}}{\frac{1}{H}}^2
+ \alpha \left( \frac{1}{H} u_{h,t} , u_h^D \right)
+ \frac{\beta}{2 \epsilon^2} \norm{ \nabla \cdot u_h^D }^2
\right] & \\
+ \frac{\alpha}{\epsilon} \left( \frac{f}{H} u_{h,t}^\perp , u_h^D \right)
+ \frac{\alpha\beta}{\epsilon^2} \norm{ \nabla \cdot u_h^D }^2 & \\
+ \weightednorm{ u_{h,t} }{\frac{C}{H}}^2 
- \alpha \weightednorm{ u_{h,t}^D }{\frac{1}{H}}^2 
+ \alpha \left( \frac{C}{H} u_{h,t} , u_h^D \right) & = 0.
\end{split}
\end{equation}
This has the form of an ordinary differential equation
\begin{equation}
A^\prime(t) + B(t) = 0,
\label{eq:ode}
\end{equation}
where
\begin{equation}
A(t) = \frac{1}{2} \weightednorm{u_{h,t}}{\frac{1}{H}}^2
+ \alpha \left( \frac{1}{H} u_{h,t} , u_h^D \right)
+ \frac{\beta}{2 \epsilon^2} \norm{ \nabla \cdot u_h^D }^2
\end{equation}
and
\begin{equation}
\begin{split}
B(t) = &
\frac{\alpha}{\epsilon} \left( \frac{f}{H} u_{h,t}^\perp , u_h^D \right)
+ \frac{\alpha\beta}{\epsilon^2} \norm{ \nabla \cdot u_h^D }^2 \\
& + \weightednorm{ u_{h,t} }{\frac{C}{H}}^2 
- \alpha \weightednorm{ u_{h,t}^D }{\frac{1}{H}}^2 
+ \alpha \left( \frac{C}{H} u_{h,t} , u_h^D \right).
\end{split}
\end{equation}
By showing that for suitably chosen $\alpha$, both $A(t)$ and $B(t)$ are comparable to $E(t)$ defined in~(\ref{eq:Edef}), we can obtain exponential damping of the energy.

\begin{lemma}
Suppose that 
\begin{equation}
\alpha \leq \alpha_1 \equiv \frac{\sqrt{\beta H_*}}{2 C_p \epsilon}.
\label{eq:alpha1}
\end{equation}
Then
\begin{equation}
\frac{1}{2} E(t) \leq A(t) \leq \frac{3}{2} E(t).
\label{eq:AequivE}
\end{equation}
\end{lemma}
\begin{proof}
We bound the term $\left( \frac{1}{H} u_{h,t} , u_h^D \right)$, with
Cauchy-Schwarz, Poincare-Friedrichs~(\ref{eq:pf}), and weighted
Young's inequality with $\delta = \frac{\epsilon}{\sqrt{\beta}}$: 
\begin{equation}
\begin{split}
\left( \frac{1}{H} u_{h,t}^D , u_h^D \right) &
\leq \frac{C_P}{2\sqrt{H_*}} \left[
\frac{\epsilon}{\sqrt{\beta}} \weightednorm{u_{h,t}}{\frac{1}{H}}^2
+ \frac{\sqrt\beta}{\epsilon} \norm{\nabla \cdot u_h^D}^2 \right] \\
& = 
\frac{C_P\epsilon}{\sqrt{H_*\beta}} \left[
\frac{1}{2} \weightednorm{u_{h,t}}{\frac{1}{H}}^2
+ \frac{\beta}{2\epsilon^2} \norm{\nabla \cdot u_h^D}^2 \right] \\
& = \frac{C_P\epsilon}{\sqrt{H_*\beta}} E(t).
\end{split}
\end{equation}
So, then, we have
\begin{equation}
\begin{split}
A(t) & \leq \left( 1 + \frac{\alpha C_P \epsilon}{\sqrt{\beta H_*}} \right)
E(t), \\
A(t) & \geq \left( 1 - \frac{\alpha C_P \epsilon}{\sqrt{\beta H_*}} \right)
E(t),
\end{split}
\end{equation}
and the result follows thanks to the assumption~(\ref{eq:alpha1}).
\end{proof}

Showing that $B(t)$ is bounded above by a constant times $E(t)$ is
straightforward, but not needed for our damping results.
\begin{lemma}
Suppose that 
\begin{equation}
0 < \alpha \leq \alpha_2 \equiv \frac{2 C_*}{1 + \chi},
\label{eq:alpha2}
\end{equation}
where
\begin{equation}
\chi =  \left( 2 +  \frac{C_P^2 \left( 1 + \epsilon C^*
      \right)^2}{ \beta H_*} \right).
\label{eq:chidef}
\end{equation}
Then
\begin{equation}
B(t) \geq \alpha E(t).
\label{eq:Bbelow}
\end{equation}
\end{lemma}
\begin{proof}
We use Cauchy Schwarz, the bounds $0 < C_* \leq C \leq C^*$ and $|f| \leq 1$, and
Young's inequality with weight $\delta > 0$ to write
\begin{equation}
\begin{split}
B(t) \geq &
\left( C_* - \alpha \right)
\weightednorm{u_{h,t}}{\frac{1}{H}}^2 
+ \frac{\alpha \beta}{\epsilon^2} \norm{\nabla \cdot u_h^D}^2 \\
&
- \frac{\alpha C_P}{\epsilon \sqrt{H_*}} \left( C^* \epsilon + 1 \right)
\weightednorm{u_{h,t}}{\frac{1}{H}}
  \norm{\nabla \cdot u_h^D} \\
 \geq &
\left[ 2 C_* - \alpha \left( 2 + \frac{C_P \left( 1 + \epsilon C^*
      \right)}{\epsilon \sqrt{H_*} \delta} \right) \right]
\frac{1}{2} \weightednorm{u_{h,t}}{\frac{1}{H}}^2 \\
&
+
\alpha
\left[ 2 - \frac{\epsilon C_P \left( 1 + \epsilon C^* \right)}{\beta
    \sqrt{H_*}} \delta \right] \frac{\beta}{2\epsilon^2} \norm{\nabla
  \cdot u_h^D}^2.
\end{split}
\end{equation}
Next, it remains to select $\delta$ and $\alpha$ to make the
coefficients of each norm positive and also balance the terms.  
First, we pick
\[
\delta = \frac{\beta \sqrt{H_*}}{\epsilon C_P \left( 1 + \epsilon C^* \right)},
\]
and calculating that
\[
\frac{C_P \left( 1 + \epsilon C^*
      \right)}{\epsilon \sqrt{H_*} \delta}
= \frac{C_P^2 \left( 1 + \epsilon C^* \right)^2}{ \beta H_*},
\]
we have that
\begin{equation}
\begin{split}
B(t) \geq &
\left[ 2 C_* - \alpha \left( 2 +  \frac{C_P^2 \left( 1 + \epsilon C^*
      \right)^2}{ \beta H_*} \right) \right] 
\frac{1}{2} \weightednorm{u_{h,t}}{\frac{1}{H}}^2
+ \alpha \frac{\beta}{2\epsilon^2} \norm{ \nabla \cdot u_h^D}^2 \\
& = \left( 2 C_* - \alpha \chi \right) 
    \frac{1}{2} \weightednorm{u_{h,t}}{\frac{1}{H}}^2
    + \frac{\alpha\beta}{2\epsilon^2} \norm{\nabla \cdot u_h^D}^2.
\end{split}
\label{eq:Blower2}
\end{equation}
We let $\alpha_2$ be the solution to
\[
2 c_* - \alpha_2 \chi = \alpha_2,
\]
so that
\begin{equation}
\alpha_2 \equiv \frac{2 C_*}{1 + \chi}.
\label{eq:alpha2choice}
\end{equation}
If we pick $\alpha = \alpha_2$, then we have the lower bound for 
$B(t)$ is exactly $\alpha E(t)$.   However, we are also constrained to
pick $\alpha \leq \min\{\alpha_1,\alpha_2\}$ in order
to guarantee that the lower bounds for $A(t)$ is positive as well.
If we have $\alpha \leq \alpha_2$, then
\[
2 C_* - \alpha \chi \geq 2 C_* - \alpha_2 \chi
= \alpha_2 \geq \alpha,
\]
and so we also have
\begin{equation}
B(t) \geq \alpha E(t).
\end{equation}
\end{proof}

We combine these two lemmas to give our exponential damping
result.
\begin{theorem}
\label{th:exp}
Let $\alpha_1$ and $\alpha_2$ be defined by~(\ref{eq:alpha1})
and~(\ref{eq:alpha2}), respectively.  Then, for any
$0 < \alpha \leq \min\{ \alpha_1,\alpha_2 \}$, and any $t > 0$, we have
\begin{equation}
E(t) \leq 3 E(0) e^{-\frac{2\alpha}{3} t}.
\end{equation}
\end{theorem}
\begin{proof}
In light of~(\ref{eq:ode}),~(\ref{eq:Bbelow}), and the lower bound
in~(\ref{eq:AequivE}), we have that
\begin{equation}
A^\prime(t)  + \frac{2\alpha}{3} A(t) \leq 0,
\end{equation}
so that
\begin{equation}
A(t) \leq A(0) e^{-\frac{2\alpha}{3} t}.
\end{equation}
Using the upper and lower bounds of $A$ in~(\ref{eq:AequivE}) gives
the desired estimate.
\end{proof}

This result shows that the damping term drives an unforced system to
one with a steady, solenoidal velocity field, in which the Coriolis
force balances the pressure gradient term, \emph{i.e.} in a state of
geostrophic balance.  Using the second equation
in~(\ref{eq:discrete_mixed}), we also know that the linearized height
disturbance is steady in time in this case.  These facts together lead
to an elliptic equation for the steady state
\begin{equation}
\label{eq:elliptic}
\begin{split}
\left( \frac{C}{H} u_h , v_h \right) + \frac{1}{\epsilon} \left(
\frac{f}{H} u_h^\perp , v_h \right) - \frac{\beta}{\epsilon^2} \left(
\eta_h , \nabla \cdot v_h \right) & = 0 \\
\left( \nabla \cdot u_h, w_h \right) & = 0
\end{split}
\end{equation}
It is easy to see that this problem is coercive on the divergence-free
subspaces and thus is well-posed.  Hence, with zero
forcing, both $u_h$ and $\eta_h$ equal zero is the only solution.  The 
zero-energy steady state then cannot have a nonzero solenoidal part.
Moreover, the exponentially decay of $\| u_t \|$ toward zero forces
$u$ to reach its steady state quickly, driving both $u^D$ and $u^S$
toward zero at an exponential rate.  Finally, since 
$\eta_t = -\nabla \cdot u$ almost everywhere, the exponential damping of 
$\| \nabla \cdot u \|$ also forces $\eta$ toward its zero steady state at
the same rate.


Now, we turn to the case where the forcing term is nonzero, adapting this
damping result to give long-time stability.
The same techniques as before now lead to
\begin{equation}
A^\prime(t) + B(t) = \left( \widetilde{F} , u_{h,t} + \alpha u_h^D \right).
\label{eq:ode2}
\end{equation}

\begin{theorem}
For any $0 < \alpha \leq \min\{ \alpha_1,\alpha_2\}$ and
\label{thm:dampedstable}
\begin{equation}
K_\alpha \equiv \frac{1}{2} \left[1 +  \frac{\alpha^2 C_P^2\epsilon^2}{\beta H_*^2} \right],
\end{equation} 
we have the bound
\begin{equation}
E(t) \leq 3 e^{-\frac{\alpha}{3}t} E(0) + \frac{K_\alpha}{\alpha} \int_0^t e^{\frac{\alpha}{3} \left( s - t \right)} \weightednorm{\widetilde{F}}{H}^2 ds.
\end{equation}
\end{theorem}
\begin{proof}
We bound the right-hand side of~(\ref{eq:ode2}) by
\begin{equation}
\begin{split}
 \left( \widetilde{F}  , u_{h,t} + \alpha u_h^D \right) &
\leq \weightednorm{ \widetilde{F}}{H} \weightednorm{ u_{h,t} }{\frac{1}{H}}
+ \alpha C_P \weightednorm{ \widetilde{F} }{H} \weightednorm{ \nabla \cdot u_h^D }{\frac{1}{H}} \\
& \leq 
\left[ \frac{H^*}{2 \delta_1} +  \frac{\alpha C_P}{2 \delta_2} 
\right] \weightednorm{\widetilde{F}}{H}^2
+ \frac{\delta_1}{2} \weightednorm{u_{h,t}}{\frac{1}{H}}
+ \frac{\alpha C_P \delta_2}{2 H_*}\norm{ \nabla \cdot u_h^D }^2
\end{split}
\end{equation}
We put $\delta_2 =
\frac{\beta\delta_1 H_*}{\alpha C_P \epsilon^2}$ to find
\begin{equation}
 \left( \widetilde{F}  , u_{h,t} + \alpha u_h^D \right)
\leq
\frac{1}{\delta_1} K_\alpha
\weightednorm{\widetilde{F}}{H}^2
+ \delta_1 E(t).
\end{equation}
This turns~(\ref{eq:ode2}) into the differential inequality
\begin{equation}
A^\prime(t) + B(t) \leq \frac{K_\alpha}{\delta_1}
\weightednorm{\widetilde{F}}{H}^2
+ \delta_1 E(t).
\end{equation} 
Using ~(\ref{eq:Bbelow}), we obtain 
\begin{equation}
A^\prime(t) + \alpha E(t) \leq  \frac{K_\alpha}{\delta_1}
\weightednorm{\widetilde{F}}{H}^2
+ \delta_1 E(t).
\end{equation}
At this point, we specify $\delta_1 = \frac{\alpha}{2}$ so that,
with~(\ref{eq:AequivE}) we have
\begin{equation}
A^\prime(t) + \frac{\alpha}{3} A(t) 
\leq  \frac{K_\alpha}{\alpha} 
\weightednorm{\widetilde{F}}{H}^2.
\end{equation}
This leads to the bound on $A(t)$
\begin{equation}
A(t) \leq e^{-\frac{\alpha}{3} t} A(0)
+ \frac{K_\alpha}{\alpha} \int_0^t e^{\frac{\alpha}{3}\left(s-t\right)} \weightednorm{\widetilde{F}}{H}^2 ds.
\end{equation}
Using~(\ref{eq:AequivE}) again gives the desired result.
\end{proof}

These stability results have important implications for tidal computations.
Theorem~\ref{thm:dampedstable} shows long-time stability of the
system.  Our stability result  also shows that the semidiscrete method
captures the three-way geotryptic balance between Coriolis, pressure
gradients, and forcing.  Moreover, we also can demonstrate that
``spin-up'', the process by which in practice tide models are started
from an arbitrary initial condition and run until they approach their
long-term behavior, is justified for this method.  To see this, the
difference between any two solutions with equal forcing but differing initial 
conditions will satisfy the same~\eqref{eq:secondorderdiscrete} with
nonzero initial conditions and zero forcing.  Consequently, the
difference must approach zero exponentially fast.  This means that we
can define a global attracting solution in the standard way (that is,
take $\eta(x,t;t^*)$, $u(x,t;t^*)$ for $0 > t^*$ and $t > t^*$ as
the solution starting from zero initial conditions at $t^*$ and define
the global attracting solution as the limit as $t^* \rightarrow
-\infty$), to which the solution for any condition becomes
exponentially close in finite time.  The error estimates we
demonstrate in the next section then can be used to show that the
semidiscrete finite element 
solution for given initial conditions approximates this global
attracting solution arbitrarily well by picking $t$ large enough that
the difference between the exact solution with those initial
conditions and the global attracting
solution is small and then letting $h$ be small enough that the
finite element solution approximates that exact solution well.




