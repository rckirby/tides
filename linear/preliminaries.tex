\section{Mathematical preliminaries}

\label{se:prelim}

For the velocity space $V_h$, we will work with standard $\hdiv$ mixed
finite element spaces on triangular elements, such as Raviart-Thomas
(RT), Brezzi-Douglas-Marini (BDM), and Brezzi-Douglas-Fortin-Marini
(BDFM)~\cite{RavTho77a,brezzi1985two,brezzi1991mixed}.  We label the
lowest-order Raviart-Thomas space with index $k=1$, following the
ordering used in the finite element exterior
calculus~\cite{arnold2006finite}.  Similarly, the lowest-order
Brezzi-Douglas-Fortin-Marini and Brezzi-Douglas-Marini spaces
correspond to $k=1$ as well.  We will always take $W_h$ to consist of
piecewise polynomials of degree $k-1$, not constrained to be
continuous between cells. In the case of domains with boundaries, we
require the strong boundary condition $u\cdot n = 0$ on all
boundaries. 

In the main part of this paper we shall present results assuming that
the domain is a subset of $\mathbb{R}^2$, \emph{i.e.} flat
geometry. In the Appendix, we describe how to extend these results to
the case of embedded surfaces in $\mathbb{R}^3$.

Throughout, we shall let $\norm{\cdot}$ denote the standard $L^2$ norm.  We
will frequently work with weighted $L^2$ norms as well.  For a
positive-valued weight function $\kappa$, we define the weighted 
$L^2$ norm 
\begin{equation}
\weightednorm{g}{\kappa}^2
= \int_\Omega \kappa \left| g \right|^2 dx.
\end{equation}
If there exist positive constants $\kappa_*$ and $\kappa^*$ such that
$0 < \kappa_* \leq \kappa \leq \kappa^* < \infty$ 
almost everywhere, then the weighted norm is equivalent to
the standard $L^2$ norm by
\begin{equation}
\sqrt{\kappa_*} \norm{g} 
\leq \weightednorm{g}{\kappa} 
\leq \sqrt{\kappa^*} \norm{g}.
\end{equation}

A Cauchy-Schwarz inequality 
\begin{equation}
(\kappa g_1 , g_2) \leq 
\weightednorm{g_1}{\kappa}
\weightednorm{g_2}{\kappa}
\end{equation}
holds for the weighted inner product, and we can also incorporate
weights into Cauchy-Schwarz for the standard $L^2$ inner product by
\begin{equation}
(g_1,g_2) = 
(\sqrt{\kappa} g_1 , \frac{1}{\sqrt{\kappa}} g_2)
\leq \weightednorm{g_1}{\kappa} \weightednorm{g_2}{\frac{1}{\kappa}}.
\end{equation}

We refer the reader to references such as~\cite{brezzi1991mixed} for full
details about the particular definitions and properties of these
spaces, but here recall several facts essential for our analysis.  For
all velocity spaces $V_h$ we consider, the divergence maps $V_h$ onto $W_h$.
Also, the spaces of interest all have a projection, $\Pi : \hdiv
\rightarrow V_h$ that commutes with the $L^2$ projection $\pi$ into
$W_h$: 
\begin{equation}
\left( \nabla \cdot \Pi u , w_h \right)
= \left( \pi \nabla \cdot u , w_h \right)
\end{equation}
for all $w_h \in W_h$ and any $u \in \hdiv$.  
We have the error estimate
\begin{equation}
\norm{u - \Pi u} \leq C_{\Pi} h^{k+\sigma} \weightedseminorm{u}{k}
\label{eq:PiL2}
\end{equation}
when $u \in H^{k+1}$.  Here, $\sigma = 1$ for the BDM spaces but
$\sigma =0$ for the RT or BDFM spaces. The projection also has an
error estimate for the divergence
\begin{equation}
\norm{ \nabla \cdot \left( u - \Pi u \right) } \leq C_\Pi h^{k} 
\weightedseminorm{\nabla \cdot u}{k}
\label{eq:PiDiv}
\end{equation}
for all the spaces of interest, whilst the pressure projection has the
error estimate 
\begin{equation}
\norm{ \eta - \pi \eta } \leq C_{\pi} h^k \weightedseminorm{\eta}{k}.
\label{eq:piL2}
\end{equation}
Here, $C_\Pi$ and $C_\pi$ are positive constants independent of $u$,
$\eta$, and $h$, although not necessarily of the shapes of the
elements in the mesh.

We will utilize a Helmholtz decomposition of $\hdiv$ under a weighted inner product.  For a very general treatment of such decompositions,
we refer the reader to~\cite{arnold2010finite}.  For each $u \in V$, there exist
unique vectors $u^D$ and $u^S$ such that $u = u^D + u^S$, $\nabla
\cdot u^S = 0$, and also $\left( \frac{1}{H} u^D , u^S \right) = 0$.
That is, $\hdiv$ is decomposed into the direct sum of solenoidal
vectors, which we denote by
\begin{equation}
\mathcal{N} \left( \nabla \cdot \right)
= \left\{ u \in V : \nabla \cdot u = 0 \right\},
\end{equation}
 and its orthogonal complement under the $\left( \frac{1}{H}
  \cdot , \cdot \right)$ inner product, which we denote by
\begin{equation}
\mathcal{N} \left( \nabla \cdot \right)^\perp
= \left\{ u \in V : \left( \frac{1}{H} u , v \right) = 0, \ \forall v
  \in \mathcal{N} \left( \nabla \cdot \right) \right\}.
\end{equation}
Functions in $\mathcal{N} \left( \nabla \cdot \right)^\perp$
satisfy a generalized Poincar\'e-Friedrichs inequality, that there
exists some $C_P$ such that 
\begin{equation}
\weightednorm{u^D}{\frac{1}{H}}
\leq C_P \weightednorm{\nabla \cdot u^D}{\frac{1}{H}}.
\label{eq:pf}
\end{equation}
We may also use norm equivalence to write this as
\begin{equation}
\weightednorm{u^D}{\frac{1}{H}}
\leq \frac{C_P}{\sqrt{H_*}} \norm{\nabla \cdot u^D}.
\end{equation}
Because our mixed spaces $V_h$ are contained in $H(\mathrm{div})$, the same decompositions can be applied, and the Poincar\'e-Friedrichs inequality holds with a constant no larger than $C_p$.

