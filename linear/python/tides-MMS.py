"""
MMS test for tides.
"""

from firedrake import *
import numpy as np

op2.init(log_level="WARNING")
#parameters['assembly_cache']['enabled'] = False

#Some physical parameters
Eps = 0.1
Beta = Eps
C = 1000.0
Theta = 0.5
Dt = 0.01
Omega = 2.0
T = 0.3

def L_u(u,eta,Hinv):
    """
    Return a form that defines L_u(u,eta,H) for the equation:
    <v,u_t> + <v,L_u(u,eta;H)> = 0.
    u is velocity, eta is free surface elevation,
    H is topography field.
    """
    
    return (
        inner(v,perp(u))*Hinv/Eps
        - Beta/Eps/Eps*div(v)*eta
        + C*Hinv*inner(v,u)
        )*dx

def L_eta(u,eta):
    """
    Return a form that defines L_eta(u,eta) for the equation:
    <phi,eta_t> + <phi,L_eta(u,eta)> = 0.
    """
    
    return phi*div(u)*dx

# Define global normal
global_normal = Expression(("x[0]/pow(x[0]*x[0]+x[1]*x[1]+x[2]*x[2],0.5)",
                            "x[1]/pow(x[0]*x[0]+x[1]*x[1]+x[2]*x[2],0.5)",
                            "x[2]/pow(x[0]*x[0]+x[1]*x[1]+x[2]*x[2],0.5)"))

#Exact solution
#we take a solution that is purely divergent, u = cos(omega*t)*grad phi
#therefore, eta_t = -cos(omega*t)*Laplace phi
#so eta = -sin(omega*t)/omega*Laplace phi
laplacephi = Expression("x[0]*x[1]*x[2]")
phi = Expression("-x[0]*x[1]*x[2]/12")
gradphi = Expression(("-(x[1]*x[2]-3*x[0]*x[0]*x[1]*x[2]/(x[0]*x[0]+x[1]*x[1]+x[2]*x[2]))/12",
                      "-(x[0]*x[2]-3*x[0]*x[1]*x[1]*x[2]/(x[0]*x[0]+x[1]*x[1]+x[2]*x[2]))/12",
                      "-(x[0]*x[1]-3*x[0]*x[1]*x[2]*x[2]/(x[0]*x[0]+x[1]*x[1]+x[2]*x[2]))/12"))


nmeshes = 5
plotting = False

eta_L2s = []

n_list = np.arange(nmeshes,dtype='int')
for n in n_list:
    dt = 1.0e-5
    mesh = UnitIcosahedralSphereMesh(n,reorder=False)
    mesh.init_cell_orientations(global_normal)
    
# Define approximation spaces and basis functions
    order = 1
    V = FunctionSpace(mesh, "RT", order)
    Q = FunctionSpace(mesh, "DG", order-1)
    Vn = VectorFunctionSpace(mesh,"CG",1)
    VH = FunctionSpace(mesh,"CG",1)
    
    Normal = Function(Vn).interpolate(global_normal)

# Setting up perp operator
    perp = lambda u: cross(Normal, u)
    
#Equations
#Timestepping is implicit midpoint applied to whole thing

    oldu = project(gradphi,V)
    newu = Function(V)
    oldeta = Function(Q)
    neweta = Function(Q)
    
    u = TrialFunction(V)
    v = TestFunction(V)
    
    ubar = Theta*u + (1-Theta)*oldu
    eta = oldeta - dt*div(ubar)
    etabar = Theta*eta + (1-Theta)*oldeta

    #Hinv = Function(VH).interpolate(Expression(("1.0/(1.0 + 0.1*exp(-x[0]*x[0]))")))
    Hinv = 1.0

    Au = inner(v,(u-oldu))*Hinv*dx + dt*L_u(ubar,etabar,Hinv)

    au = lhs(Au)
    Lu = rhs(Au)

    eta = TrialFunction(Q)
    phi = TestFunction(Q)

    ubar = Theta*newu + (1-Theta)*oldu
    Aeta = phi*(eta-oldeta)*dx + dt*L_eta(ubar,etabar)

    aeta = lhs(Aeta)
    Leta = rhs(Aeta)

    t = 0.

    Vexact = FunctionSpace(mesh,"CG",3)
    Vexactu = VectorFunctionSpace(mesh,"CG",3)

    Gradphi = project(gradphi, Vexactu)
    Laplacephi = project(laplacephi, Vexact)

    uexact_out = Function(Vexactu)
    etaexact_out = Function(Vexact)

    ufile = File("u.pvd")
    etafile = File("eta.pvd")
    uexactfile = File("uexact.pvd")
    etaexactfile = File("etaexact.pvd")

    th = Constant(0.0)

    uexact = cos(Omega*th)*Gradphi
    etaexact = -sin(Omega*th)/Omega*Laplacephi
    uexact_t = Omega*sin(Omega*th)*Gradphi
    etaexact_t = -cos(Omega*th)*Laplacephi
    
    Lu0 = Lu + dt*(inner(v,uexact_t)*Hinv*dx 
                   + L_u(uexact,etaexact,Hinv))

    eta_L2 = 0.

    uproblem = LinearVariationalProblem(au,Lu0,newu)
    usolver = LinearVariationalSolver(uproblem,
                                      solver_parameters={"ksp_type": "preonly",
                                                         "pc_type": "lu"})
    etaproblem = LinearVariationalProblem(aeta,Leta,neweta)
    etasolver = LinearVariationalSolver(etaproblem,
                                        solver_parameters={"ksp_type": 
                                                           "preonly",
                                                           "pc_type": "lu"})
    
    tcyc = 100*dt
    cyct = 0.
    while (t < T):
        cyct += dt
        if(cyct>tcyc-0.5*dt):
            print n,t,dt,T
            cyct -= tcyc
        th.assign(t + Theta*dt)

        usolver.solve()
        etasolver.solve()
        t += dt

        #th.assign(t)
        etabar = 0.5*(oldeta+neweta)
        eta_L2 += dt*assemble((etabar-etaexact)*(etabar-etaexact)*dx)

        oldu.assign(newu)
        oldeta.assign(neweta)

        if(plotting):
            ufile << oldu
            etafile << oldeta
            
            ue = TrialFunction(Vexactu)
            ve = TestFunction(Vexactu)
            solve(inner(ve,ue)*dx == inner(ve,uexact)*dx, uexact_out)
            etae = TrialFunction(Vexact)
            phie = TestFunction(Vexact)
            solve(phie*etae*dx == phie*etaexact*dx, etaexact_out)
            
            uexactfile << uexact_out
            etaexactfile << etaexact_out

    th.assign(T)

    eta_L2 = eta_L2**0.5
    print eta_L2
    eta_L2s.append(eta_L2)

#import pylab
h = 2.0**(-np.array(n_list))
h.tofile('MMSh.dat',sep=' ')
np.array(eta_L2s).tofile('MMS_eta_order1.dat',sep=' ')
#pylab.loglog(h,eta_L2s,'.')
#pylab.loglog(h,h**2)
#pylab.show()
