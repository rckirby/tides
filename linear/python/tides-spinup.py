"""
Energy conservation test for tides
"""

from firedrake import *
from numpy import array

#Some physical parameters
Eps = 0.1
Beta = Eps
C = 0.01
Theta = 0.5
Dt = 0.01
Omega = 2.09
T = 100.0
dt = 0.01

def L_u(u,eta,Hinv):
    """
    Return a form that defines L_u(u,eta,H) for the equation:
    <v,u_t> + <v,L_u(u,eta;H)> = 0.
    u is velocity, eta is free surface elevation,
    H is topography field.
    """
    
    return (
        inner(v,perp(u))*Hinv/Eps
        - Beta/Eps/Eps*div(v)*eta
        + C*Hinv*inner(v,u)
        )*dx

def L_eta(u,eta):
    """
    Return a form that defines L_eta(u,eta) for the equation:
    <phi,eta_t> + <phi,L_eta(u,eta)> = 0.
    """
    
    return phi*div(u)*dx

# Define global normal
global_normal = Expression(("x[0]/pow(x[0]*x[0]+x[1]*x[1]+x[2]*x[2],0.5)",
                            "x[1]/pow(x[0]*x[0]+x[1]*x[1]+x[2]*x[2],0.5)",
                            "x[2]/pow(x[0]*x[0]+x[1]*x[1]+x[2]*x[2],0.5)"))

mesh = UnitIcosahedralSphereMesh(3)
mesh.init_cell_orientations(global_normal)

# Define approximation spaces and basis functions
order = 1
V = FunctionSpace(mesh, "RT", order)
Q = FunctionSpace(mesh, "DG", order-1)
Vn = VectorFunctionSpace(mesh,"CG",1)
VH = FunctionSpace(mesh,"CG",1)

Normal = Function(Vn).interpolate(global_normal)

# Setting up perp operator
perp = lambda u: cross(Normal, u)

#Equations
#Timestepping is implicit midpoint applied to whole thing

oldu = Function(V)
newu = Function(V)
oldeta = Function(Q)#project(Expression(("x[0]*x[1]*x[2]")),Q)
neweta = Function(Q)

u = TrialFunction(V)
v = TestFunction(V)

inner(v,perp(u))*dx

eta = oldeta - dt*(Theta*div(u) + (1-Theta)*div(oldu))
etabar = Theta*eta + (1-Theta)*oldeta
ubar = Theta*u + (1-Theta)*oldu

Hinv = Function(VH).interpolate(Expression(("1.0/(1.0 + 0.1*exp(-x[0]*x[0]))")))

Au = inner(v,(u-oldu))*Hinv*dx + dt*L_u(ubar,etabar,Hinv)

au = lhs(Au)
Lu = rhs(Au)

eta = TrialFunction(Q)
phi = TestFunction(Q)

ubar = Theta*newu + (1-Theta)*oldu
Aeta = phi*(eta-oldeta)*dx + dt*L_eta(ubar,etabar)

aeta = lhs(Aeta)
Leta = rhs(Aeta)

t = 0.

Vexact = FunctionSpace(mesh,"CG",3)

Energy = 0.5*(
    inner(oldu,oldu)*Hinv + Beta*oldeta*oldeta/Eps**2
    )*dx

energy = []
time = []

ForcingPotential = Function(VH).interpolate(
    Expression(("x[0]*x[1]*x[2]")))

while (t < T):
    Lu0 = Lu + dt*sin(Omega*(t+Theta*dt))*div(v)*ForcingPotential*dx

    solve(au == Lu0, newu, solver_parameters={"ksp_type": "preonly",
                                             "pc_type": "lu"})
    
    solve(aeta == Leta, neweta, solver_parameters={"ksp_type": "preonly",
                                             "pc_type": "lu"})

    t += dt
    time.append(t)
    
    oldu.assign(newu)
    oldeta.assign(neweta)

    energy.append(assemble(Energy))

