"""
MMS test for tides.
"""

from firedrake import *
import numpy as np

op2.init(log_level="WARNING")
#parameters['assembly_cache']['enabled'] = False

#Some physical parameters
Eps = 0.1
Beta = Eps
C = 10.0
Theta = 0.5
Dt = 0.01
Omega = 2.0
T = 30.0

def L_u(u,eta,Hinv):
    """
    Return a form that defines L_u(u,eta,H) for the equation:
    <v,u_t> + <v,L_u(u,eta;H)> = 0.
    u is velocity, eta is free surface elevation,
    H is topography field.
    """
    
    return (
        inner(v,perp(u))*Hinv/Eps
        - Beta/Eps/Eps*div(v)*eta
        + C*Hinv*inner(v,u)
        )*dx

def L_eta(u,eta):
    """
    Return a form that defines L_eta(u,eta) for the equation:
    <phi,eta_t> + <phi,L_eta(u,eta)> = 0.
    """
    
    return phi*div(u)*dx

# Define global normal
global_normal = Expression(("x[0]/pow(x[0]*x[0]+x[1]*x[1]+x[2]*x[2],0.5)",
                            "x[1]/pow(x[0]*x[0]+x[1]*x[1]+x[2]*x[2],0.5)",
                            "x[2]/pow(x[0]*x[0]+x[1]*x[1]+x[2]*x[2],0.5)"))

#Forcing
etaprime = Expression("x[0]*x[1]*x[2]")

dt = 1.0e-3
mesh = UnitIcosahedralSphereMesh(4,reorder=False)
mesh.init_cell_orientations(global_normal)

# Define approximation spaces and basis functions
order = 1
V = FunctionSpace(mesh, "RT", order)
Q = FunctionSpace(mesh, "DG", order-1)
Vn = VectorFunctionSpace(mesh,"CG",1)
VH = FunctionSpace(mesh,"CG",1)
    
Normal = Function(Vn).interpolate(global_normal)

# Setting up perp operator
perp = lambda u: cross(Normal, u)

#Equations
#Timestepping is implicit midpoint applied to whole thing

oldu1 = Function(V)
newu1 = Function(V)
oldeta1 = Function(Q)
neweta1 = Function(Q)

oldu2 = Function(V)
newu2 = Function(V)
oldeta2 = Function(Q)
neweta2 = Function(Q)

nu = oldu1.dat.data[:].size
neta = oldeta1.dat.data[:].size
oldu1.dat.data[:] = np.random.randn(nu)
oldu2.dat.data[:] = np.random.randn(nu)
oldeta1.dat.data[:] = np.random.randn(neta)
oldeta2.dat.data[:] = np.random.randn(neta)

One = Function(Q).interpolate(Expression(('1')))

eta1mean = assemble(oldeta1*dx)/assemble(One*dx)
eta2mean = assemble(oldeta2*dx)/assemble(One*dx)
oldeta1 -= eta1mean
oldeta2 -= eta2mean

u = TrialFunction(V)
v = TestFunction(V)

ubar1 = Theta*u + (1-Theta)*oldu1
eta1 = oldeta1 - dt*div(ubar1)
etabar1 = Theta*eta1 + (1-Theta)*oldeta1

ubar2 = Theta*u + (1-Theta)*oldu2
eta2 = oldeta2 - dt*div(ubar2)
etabar2 = Theta*eta2 + (1-Theta)*oldeta2

#Hinv = Function(VH).interpolate(Expression(("1.0/(1.0 + 0.1*exp(-x[0]*x[0]))")))
Hinv = 1.0

Au1 = inner(v,(u-oldu1))*Hinv*dx + dt*L_u(ubar1,etabar1,Hinv)
Au2 = inner(v,(u-oldu2))*Hinv*dx + dt*L_u(ubar2,etabar2,Hinv)

au = lhs(Au1)
Lu1 = rhs(Au1)
Lu2 = rhs(Au2)

eta = TrialFunction(Q)
phi = TestFunction(Q)

ubar1 = Theta*newu1 + (1-Theta)*oldu1
Aeta1 = phi*(eta-oldeta1)*dx + dt*L_eta(ubar1,etabar1)
ubar2 = Theta*newu2 + (1-Theta)*oldu2
Aeta2 = phi*(eta-oldeta2)*dx + dt*L_eta(ubar2,etabar2)

aeta = lhs(Aeta1)
Leta1 = rhs(Aeta1)
Leta2 = rhs(Aeta2)

t = 0.

etaprime = project(etaprime, Q)

th = Constant(0.0)

Lu01 = Lu1 - dt*Beta/Eps/Eps*(sin(th)*div(v)*etaprime)*dx
Lu02 = Lu2 - dt*Beta/Eps/Eps*(sin(th)*div(v)*etaprime)*dx

uproblem1 = LinearVariationalProblem(au,Lu01,newu1)
usolver1 = LinearVariationalSolver(uproblem1,
                                  solver_parameters={"ksp_type": "preonly",
                                                     "pc_type": "lu"})
uproblem2 = LinearVariationalProblem(au,Lu02,newu2)
usolver2 = LinearVariationalSolver(uproblem2,
                                  solver_parameters={"ksp_type": "preonly",
                                                     "pc_type": "lu"})

etaproblem1 = LinearVariationalProblem(aeta,Leta1,neweta1)
etasolver1 = LinearVariationalSolver(etaproblem1,
                                    solver_parameters={"ksp_type": 
                                                       "preonly",
                                                       "pc_type": "lu"})
etaproblem2 = LinearVariationalProblem(aeta,Leta2,neweta2)
etasolver2 = LinearVariationalSolver(etaproblem2,
                                    solver_parameters={"ksp_type": 
                                                       "preonly",
                                                       "pc_type": "lu"})

tcyc = 100*dt
cyct = 0.

L2us = []
L2etas = []

while (t < T):
    #cyct += dt
    #if(cyct>tcyc-0.5*dt):
    #    print t,dt,T
    #    cyct -= tcyc

    th.assign(t + Theta*dt)
        
    usolver1.solve()
    usolver2.solve()
    etasolver1.solve()
    etasolver2.solve()
    t += dt
    
    oldu1.assign(newu1)
    oldeta1.assign(neweta1)
    oldu2.assign(newu2)
    oldeta2.assign(neweta2)

    L2u = assemble(inner(oldu1-oldu2,oldu1-oldu2)*dx)
    L2eta = assemble((oldeta1-oldeta2)*(oldeta1-oldeta2)*dx)

    print L2u,L2eta, t

    L2us.append(L2u)
    L2etas.append(L2eta)

L2us = np.array(L2us)
L2etas = np.array(L2etas)

L2us.tofile('L2us.dat',sep=' ')
L2etas.tofile('L2etas.dat',sep=' ')


