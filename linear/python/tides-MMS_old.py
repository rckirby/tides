"""
MMS test for tidal equations.
"""

from firedrake import *


#Some physical parameters
Eps = 0.1
Beta = Eps
C = 0.01
Theta = 0.5
Dt = 0.01
H = 1.0
Omega = 0.568473658364
T = 1.0

def L_u(u,eta):
    """
    Return a form that defines L_u(u,eta) for the equation:
    <v,u_t> + <v,L_u(u,eta)> = 0.
    """
    
    return (
        inner(v,perp(u))/Eps - Beta/Eps/Eps*div(v)*eta
        + C*inner(v,u)
        )*dx

def L_eta(u,eta):
    """
    Return a form that defines L_eta(u,eta) for the equation:
    <phi,eta_t> + <phi,L_eta(u,eta)> = 0.
    """
    
    return phi*div(u)*dx

# Define global normal
global_normal = Expression(("x[0]", "x[1]", "x[2]"))

# Define meshes
meshes = ["sphere_ico3"]
#meshes = ["sphere_ico3", "sphere_ico4", "sphere_ico5", "sphere_ico6"]

#Exact solution
#we take a solution that is purely divergent, u = cos(omega*t)*grad phi
#therefore, eta_t = -cos(omega*t)*Laplace phi
#so eta = -sin(omega*t)/omega*Laplace phi
laplacephi = Expression("x[0]*x[1]*x[2]")
phi = Expression("-x[0]*x[1]*x[2]/12")
gradphi = Expression(("-(x[1]*x[2]-3*x[0]*x[0]*x[1]*x[2]/(x[0]*x[0]+x[1]*x[1]+x[2]*x[2]))/12",
                      "-(x[0]*x[2]-3*x[0]*x[1]*x[1]*x[2]/(x[0]*x[0]+x[1]*x[1]+x[2]*x[2]))/12",
                      "-(x[0]*x[1]-3*x[0]*x[1]*x[2]*x[2]/(x[0]*x[0]+x[1]*x[1]+x[2]*x[2]))/12"))

# Iterate over meshes
for (i, meshid) in enumerate(meshes):

    # Define global normal
    global_normal = Expression(("x[0]/pow(x[0]*x[0]+x[1]*x[1]+x[2]*x[2],0.5)",
                                "x[1]/pow(x[0]*x[0]+x[1]*x[1]+x[2]*x[2],0.5)",
                                "x[2]/pow(x[0]*x[0]+x[1]*x[1]+x[2]*x[2],0.5)"))
    
    mesh = UnitIcosahedralSphereMesh(3)
    mesh.init_cell_orientations(global_normal)
    

    meshname = "meshes/%s.xml" % meshid
    mesh = Mesh(meshname)
    mesh.init_cell_orientations(global_normal)

    # Define approximation spaces and basis functions
    order = 1
    V = FunctionSpace(mesh, "RT", order)
    Q = FunctionSpace(mesh, "DG", order-1)

    # Setting up perp operator
    outward_normals = Expression(outward_normals_code)
    outward_normals.mesh = mesh
    perp = lambda u: cross(outward_normals, u)
      
    #Equations
    #Timestepping is implicit midpoint applied to whole thing

    oldu = project(gradphi, V)
    newu = Function(V)
    oldeta = Function(Q)
    neweta = Function(Q)
    
    u = TrialFunction(V)
    v = TestFunction(V)

    eta = oldeta - dt*(Theta*div(u) + (1-Theta)*div(oldu))
    etabar = Theta*eta + (1-Theta)*oldeta
    ubar = Theta*u + (1-Theta)*oldu

    Au = inner(v,u-oldu)/H*dx + dt*L_u(ubar,etabar)
    
    au = lhs(Au)
    Lu = rhs(Au)

    eta = TrialFunction(Q)
    phi = TestFunction(Q)
    
    ubar = Theta*newu + (1-Theta)*oldu
    Aeta = phi*(eta-oldeta)*dx + dt*L_eta(ubar,etabar)*dx
    
    aeta = lhs(Aeta)
    Leta = rhs(Aeta)

    # Store solutions to .xml
    file_eta = File("eta.xml")
    file_u = File("u.xml")
    file_eta << oldeta
    file_u << oldu

    t = 0.

    Vexact = FunctionSpace(mesh,"CG",3)

    while (t < Tend):
        th = t + Theta*dt
        
        Eu = project(cos(Omega*th)*gradphi, Vexact)
        Eeta = project(-sin(Omega*th)/Omega*laplacephi, Vexact)
        Eu_t = project(Omega*sin(Omega*th)*gradphi, Vexact)
        Eeta_t = project(-cos(Omega*th)*laplacephi, Vexact)

        Lu0 = Lu - (inner(v,Eu_t)*dx + dt*L_u(Eu, Eeta))        
        solve(au == Lu0, newu)
        
        Leta0 = Leta - (phi*Eeta_t*dx + dt*L_eta(Eu, Eeta))
        solve(au == Lu0, neweta)

        oldu.assign(newu)
        oldeta.assign(neweta)

        file_eta << oldeta
        file_u << oldu
