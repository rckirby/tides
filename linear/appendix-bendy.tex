\section{Extension to the sphere and other embedded manifolds}
\label{ap:bendy}
Global tidal simulations are performed in spherical geometry, so it is
necessary to consider mixed finite element discretization using meshes
of isoparametric elements that approximate the sphere. This
constitutes a variational crime since the domain $M_h$ supporting the
mesh is only the same as the manifold $M$ in the limit $h\to
0$. Recently, the topic of mixed finite elements on embedded manifolds
was comprehensively analyzed by \cite{holst2012geometric}, following
previous work on nodal finite elements. Here, we sketch out how to use
their approach to extend the results of this paper to embedded
manifolds.

In the case of curved domains such as the surface of the sphere,
$\hdiv$ elements are implemented \emph{via} Piola transforms from a
reference triangle. This means that (a) the velocity fields are always
tangential to the mesh element, and (b) normal fluxes $u\cdot n$ take
the same value on each side of element boundaries, as required to
obtain a divergence that is bounded in $L^2$ (an approach to practical
implementation of these finite element spaces on manifolds is provided
by \cite{RoHaCoMc2013}). Similarly, the discontinuous $L^2$ spaces are
implemented using a transformation from the reference triangle
that includes scaling by the determinant of the Jacobian $J_e$; this
ensures that the surface divergence maps from $V_h$ onto $W_h$.

In this case $V_h\not\subset V$, $W_h \not \subset
W$. \cite{holst2012geometric} dealt with this problem by constructing
operators $\iota_{V_h}:V_h \to V$ and $\iota_{W_h}:W_h\to W$ such that
\[
\Pi \circ \iota_{V_h}= \Id_{V_h}, \quad \pi \circ \iota_{W_h} = \Id_{W_h},
\]
where $\Pi$ and $\pi$ are projections from $V$ to $V_h$ and $W$ to
$W_h$ respectively; these two operators commute with $\nabla\cdot$
defined on $M_h$. In particular,
\[
\left(\pi\eta,w_h\right) = \left(\eta,\iota_{W_h}w_h
\right), \quad \forall w_h \in W_h, \, \eta \in W.
\]
The estimates (\ref{eq:PiL2}-\ref{eq:piL2}) then
hold with $\iota_{V_h}\circ \Pi$ and $\iota_{W_h}\circ \pi$ replacing
$\Pi$ and $\pi$ respectively, provided that the polynomial expansion
of the element geometries in $M_h$ have at the same approximation
order as $V_h$ and $W_h$. There is also still a discrete
Poincar\'e-Friedrichs inequality for $V_h$. This means that all of our
stability results \ref{se:energy} hold in the manifold case, and it
remains to deal with the error estimates. This is done by introducing
further variables $u_h'\in V_h$, $\eta'_h\in W_h$ satisfying
\begin{equation}
\begin{split}
\left( \frac{1}{H} \iota_{V_h}u_{h,t}' , \iota_{V_h} v_h \right) 
+ \frac{1}{\epsilon} \left( \frac{f}{H}  (\iota_{V_h}u_h')^\perp , \iota_{V_h} v_h \right) \qquad \qquad & \\
 - \frac{\beta}{\epsilon^2} \left( \iota_{W_h}\eta'_h ,
\iota_{W_h}\nabla \cdot v_h \right) + \left( \frac{C}{H} \iota_{V_h}u'_h , 
\iota_{V_h}v_h \right) & =
\left( F , \iota_{V_h}v_h \right)
, \\
\left( \eta'_{h,t} , w_h \right) + \left( 
\nabla \cdot u_h'  ,w_h \right)& = 0.
\end{split}
\label{eq:discrete_mixed_J}
\end{equation}
This equation is of the form \eqref{eq:discrete_mixed} but with
a modified inner product on $V_h$. Therefore, all of our stability estimates
also hold for this modified equation. 

We split the error in $u$ and $\eta$ by writing
\begin{equation}
\begin{split}
u - \iota_{V_h}u_h &= -\chi + \iota_{V_h}\theta'_h + \iota_{V_h}\theta_h,\\
\eta - \iota_{W_h}\eta_h &= -\rho + \iota_{W_h}\zeta'_h + \iota_{W_h}\zeta_h, \\
\end{split}
\end{equation}
where
\begin{equation}
\begin{split}
\chi &\equiv \iota_{V_h}\Pi u - u, \\
\rho & \equiv \iota_{W_h}\pi \eta - \eta, \\
\theta'_h & \equiv \Pi u -  u_h',  \\
\zeta'_h & \equiv \pi \eta_ - \eta_h'.\\
\theta_h & \equiv u_h' - u_h,  \\
\zeta_h & \equiv \eta_h' - \eta_h.
\end{split}
\end{equation}
We can bound $\theta'_h$ and $\zeta'_h$ by applying
Proposition~\ref{prop:firstorderstability} adapted to Equation
\eqref{eq:discrete_mixed_J}, \emph{i.e.} by substituting
$v=\iota_{V_h}v_h$ into \eqref{eq:mixed} and rearranging so that it
takes the form of \eqref{eq:discrete_mixed_J} with a forcing defined
in terms of $u$, then subtracting
\eqref{eq:discrete_mixed_J}. Similarly, $\theta_h$ and $\zeta_h$ may
be bounded by rearranging Equation \eqref{eq:discrete_mixed_J} into
the form of \eqref{eq:mixed}, then subtracting \eqref{eq:mixed}. Terms
appear that are proportional to $\|\Id-J\|$ where 
\[
J_{V_h} = \iota_{V_h}^*\iota_{V_h}, \quad
J_{W_h} = \iota_{W_h}^*\iota_{W_h},
\]
and $\|\Id-J\|$ is the maximum of the operator norms of
$\Id_{V_h}-J_{V_h}$ and $\Id_{W_h}-J_{W_h}$. \cite{holst2012geometric}
showed that $\|\Id-J\|$ converges to zero as $h\to 0$ with rate
determined by the order of polynomial approximation in the
isoparametric mapping. Hence we obtain a manifold version of Theorem
\ref{th:L2 bounds}, with $u_h$ and $\eta_h$ substituted by
$\iota_{V_h}u_h$ and $\iota_{W_h}\eta_h$ respectively. Similar techniques
lead to a manifold version of Theorem \ref{th:exponential error}.
