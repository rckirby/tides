\section{Introduction}

Finite element methods are attractive for modelling the world's oceans
since implemention with triangular cells provides a
means to accurately represent coastlines and topography
~\cite{We_etal2010}. In the last decade or
so, there has been much discussion about the best choice of mixed
finite element pairs to use as the horizontal discretization for
atmosphere and ocean models. In particular, much attention has been
paid to the properties of numerical dispersion relations obtained when
discretizing the rotating shallow water equations
\cite{Da2010,CoHa2011,CoLaReLe2010,RoRoPo2007,Ro2005,RoBe2009,RoRo2008,Ro2012}.
In this paper we take a different angle, and study the behavior of
discretizations of forced-dissipative rotating shallow-water
equations, which are used for predicting global barotropic tides. The
main point of interest here is whether the discrete solutions approach
the correct long-time solution in response to quasi-periodic forcing.
In particular, we study the behavior of the linearized energy. Since
this energy only controls the divergent part of the solution, as we
shall see later, it is important to choose finite element spaces
where there is a natural discrete Helmholtz decomposition, and where
the Coriolis term projects the divergent and divergence-free
components of vector fields correctly onto each other. Hence, we
choose to concentrate on the mimetic, or compatible, finite element
spaces (\emph{i.e.} those which arise naturally from the finite
element exterior calculus \cite{arnold2006finite}) which were proposed
for numerical weather prediction in \cite{CoSh2012}. In that paper, it
was shown that the discrete equations have an exactly steady
geostrophic state (a solution in which the Coriolis term balances the
pressure gradient) corresponding to each of the divergence-free
velocity fields in the finite element space; this approach was
extended to develop finite element methods for the nonlinear rotating
shallow-water equations on the sphere that can conserve energy,
enstrophy and potential vorticity
\cite{CoTh2014,McCo2014,RoHaCoMc2013}. Here, we shall make use of the
discrete Helmholtz decomposition in order to show that mixed finite
element discretizations of the forced-dissipative linear rotating
shallow-water equations have the correct long-time energy
behavior. Since we are studying linear equations, these energy
estimates then provide finite time error bounds.

Predicting past and present ocean tides is important because they have
a strong impact on sediment transport and coastal flooding, and hence
are of interest to geologists. Recently, tides have also received a
lot of attention from global oceanographers since breaking internal
tides provide a mechanism for vertical mixing of temperature and
salinity that might sustain the global ocean circulation
\cite{GaKu2007,MuWu1998}. A useful tool for predicting tides are the
rotating shallow water equations, which provide a model of the
barotropic (\emph{i.e.}, depth-averaged) dynamics of the ocean. When
modelling global barotropic tides away from coastlines, the nonlinear
advection terms are very weak compared to the drag force, and a
standard approach is to solve the linear rotating shallow-water
equations with a parameterised drag term to model the effects of
bottom friction, as described in \cite{La45}. This approach can be
used on a global scale to set the boundary conditions for a more
complex regional scale model, as was done in \cite{Hi_etal2011}, for
example. Various additional dissipative terms have been proposed to
account for other dissipative mechanisms in the barotropic tide, due
to baroclinic tides, for example \cite{JaSt2001}. 

As mentioned above, finite element methods provide useful
discretizations for tidal models since they can be used on
unstructured grids which can seamless couple global tide structure
with local coastal dynamics. A discontinuous Galerkin approach was
developed in \cite{salehipour2013higher}, whilst continuous finite
element approaches have been used in many studies \cite[for example]{FoHeWaBa1993,kawahara1978periodic,Le_etal2002}. The lowest order Raviart-Thomas element for
velocity combined with $P_0$ for height was proposed for coastal tidal
modeling in \cite{walters2005coastal}; this pair fits into the framework 
that we discuss in this paper.

In this paper we will restrict attention to the linear bottom drag
model as originally proposed in \cite{La45}. We are aware that the
quadratic law is more realistic, but the linear law is more amenable
to analysis and we believe that the correct energy behavior of
numerical methods in this linear setting already rules out many
methods which are unable to correctly represent the long-time solution
which is in geotryptic balance (the extension to geostrophic balance
of the three way balance between Coriolis, the pressure gradient and
the dissipative term). In the presence of quasiperiodic time-varying
tidal forcing, the equations have a time-varying attracting solution
that all solutions converge to as $t\to \infty$. In view of this, we
prove the following results which are useful to tidal modellers (at
least, for the linear law):
\begin{enumerate}
\item For the mixed finite element methods that we consider, the
  spatial semidiscretization also has an attracting solution 
  in the presence of time-varying forcing.
\item This attracting solution converges to time-varying attracting
  solution of the unapproximated equations.
\end{enumerate}

Global problems require tidal simulation on manifolds rather than
planar domains.  For simplicity, our description and analysis will
follow the latter case.  However, our numerical results include
the former case.  Recently, Holst and Stern~\cite{holst2012geometric}
have demonstrated that finite element analysis on discretized
manifolds can be handled as a variational crime.  We summarize these
findings and include an appendix at the end demonstrating how to apply
their techniques to our own case.  This suggests that the extension to
manifolds presents technicalities rather than difficulties to the
analysis we provide here.


Although our present work focuses squarely on the shallow water
equations, we believe that many of our results will apply to other
hyperbolic systems with damping.  For example, the model we consider
is just the damped acoustic wave equation plus the Coriolis term.
Our techniques should extend to other settings where
function spaces have discrete Helmholtz decompositions, most notably
damped electromagnetics or elastodynamics.

Also, we point out that our main aim here is the theoretical analysis
of the damped system.  This is the first such analysis of which we are
aware demonstrating the strong damping and hence optimal long-time
error bounds.  We do not assert whether similar results hold for other
discretizations, just that they are unkown.  While an extended
experimental and/or theoretical study of these properties for a wide
range of discretizations could be a fruitful project for the tide
modelling community, it is beyond the scope of the present work.  We
do point out that the lowest-order Raviart-Thomas element has been
previously proposed for tidal modeling~\cite{walters2005coastal}, and
our framework covers this case as well as the extension to
higher-order methods and other mixed spaces.

The rest of this paper is organised as follows. In Section
\ref{se:model} we describe the finite element modelling framework
which we will analyse. In Section \ref{se:prelim} we provide
some mathematical preliminaries. In Section \ref{se:energy} we derive
energy stability estimates for the finite element tidal equations. In 
Section \ref{se:error} we use these energy estimates to obtain error
bounds for our numerical solution.  Appendix~\ref{ap:bendy} includes
the discussion of embedded manifolds.
