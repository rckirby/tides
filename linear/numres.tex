\section{Numerical results}

In this section we present some numerical experiments that illustrate
the estimates derived in the previous sections. In all cases the
equations are discretized in time using the implicit midpoint rule.
The domain is the unit sphere, centred on the origin, which is
approximated using triangular elements arranged in an icosahedral mesh
structure (see Appendix \ref{ap:bendy} for extensions of the results
of this paper to embedded surfaces such as the sphere). All numerical
results are obtained using the open source finite element library,
Firedrake (\url{http://www.firedrake.org}).

First, we verify the energy behavior in the absence of dissipation,
\emph{i.e.} $C=0$. The variables were initialized with ${u}=0$ and
$\eta=xyz$, and the equations were solved with parameters
$\epsilon=\beta=0.1$, $f=1$, $H=1 + 0.1\exp(-x^2)$, and $\Delta t=0.01$.
The energy is conserved by the continuous-time spatial
semi-discretization, and is quadratic. Since the implicit midpoint
rule time-discretization preserves all quadratic invariants (see \cite{LeRe2004}, for example), we expect exact energy conservation in
this case; this was indeed observed as shown in Figure
\ref{fig:energy}. Upon introducing a positive dissipation constant
$C=0.1$, we observe both that the energy is monotonically decreasing
(as implied by Proposition \ref{prop:monotonic}), and is scaling
exponentially in time (as implied by Theorem \ref{th:exp}). These
results are also illustrated in Figure \ref{fig:energy}.

\begin{figure}
\centerline{\includegraphics[width=6cm]{images/energy-conservation}
\includegraphics[width=6cm]{images/log-energy}}
\caption{\label{fig:energy}Plots of the evolution of energy with time
  in the cases $C=0$ and $C=0.01$. {\bfseries Left}: Energy-time plots
  for $C=0$ and $C=0.01$, over the time interval $0<t<1$. For $C=0$ we
  observe exact energy conservation as expected. For $C=0.01$ the
  energy is monotonically decreasing as expected. {\bfseries Right}:
  Energy-time plot for $C=0.01$ on a logarithmic scale over the time
  interval $0<t<50$. Then energy is decaying exponentially in time, as
  expected.}
\end{figure}

Second, we verify the convergence results proved in Section
\ref{se:error}. This was done by constructing a reference solution
using the method of manufactured solutions, \emph{i.e.} by choosing
the solution
\[
u = \cos(\Omega t)\left(
-\frac{1}{12}(yz(1 - 3x^2),
-\frac{1}{12}(xz(1 - 3y^2),
-\frac{1}{12}(xy(1 - 3z^2)
\right)
, \quad \eta = -\sin(\Omega t)\frac{xyz}{12},
\]
where we have expressed the velocity in three dimensional coordinates
even though it is constrained to remain tangential to the sphere. Here
$\eta$ and $u$ are chosen to solve the continuity equation for $\eta$
exactly, and $F$ is then chosen so that the $u$ equation is satisfied.
We used the parameters $\epsilon=\beta=0.1$, $f=H=1$, $C=1000$,
$\Omega=2$, and chose $\Delta t=10^{-5}$ in order to isolate the error
due to spatial discretization only. We ran the solutions until $t=0.3$
and computed the time-averaged $L^2$ error for $\eta$. Plots are shown in
Figure \ref{fig:MMS}; they confirm the expected first order
convergence rate for $V=$RT0, $Q=$DG0, and the expected second order
convergence rate for $V=$RT1, $Q=$DG1.

\begin{figure}
\centerline{\includegraphics[width=6cm]{images/l2errors_RT1}
\includegraphics[width=6cm]{images/l2errors_RT2}}
\caption{\label{fig:MMS}Convergence plots obtained from the method of
  manufactured solutions, showing the time-integrated $L^2$ error in
  $\eta$ against the typical element edge length $h$. {\bfseries Left:}
  Plot for RT0-DG0, the error is proportional to $h$ as expected. {\bfseries Right:} Plot for RT1-DG1, the error is proportional to $h^2$ as expected.}
\end{figure}

Finally, we illustrate that this type of discretization excludes the
possibility of spurious attracting solutions. In the case of the linear
forced-dissipative tidal equations with time-dependent forcing, the
continuous equations have the property that the solutions lose memory
of the initial conditions exponentially quickly with timescale
determined from $C$ and the other parameters (and bounded by $\alpha$
in Theorem \ref{th:exp}).  As discussed among our stability results, any two solutions with 
different initial conditions should converge to the same solution as
$t\to \infty$. We illustrate this by randomly generating initial conditions for two
solutions $(u_1,\eta_1)$ and $(u_2,\eta_2)$ with the same time-periodic forcing,
\[
(F,v) = \frac{\beta}{\epsilon^2} \sin(t) (xyz,\nabla\cdot v), 
\quad \forall v \in V,
\]
 and measuring the
difference between them as $t\to \infty$. In performing this test,
care must be taken to ensure that $\eta_1$ and $\eta_2$ both have zero
mean as implied by the perturbative derivation of the linear equations
(since the dissipation cannot influence the mean component). In this
experiment, we used the parameters $\epsilon =\beta = 0.1$, $C =
10.0$, $\Delta t=0.01$ and we used an icosahedral mesh of the sphere
at the fourth level of refinement. We indeed observed that the two
solutions converge to each other exponentially quickly in the $L^2$ norm,
as illustrated in Figure \ref{fig:sync}.

\begin{figure}
\centerline{\includegraphics[width=10cm]{images/l2-sync}}
\caption{\label{fig:sync}Plot of the $L^2$ difference between two pairs
  of solutions $(u_1,\eta_1)$ and $(u_2,\eta_2)$ with different
  randomly generated initial conditions but the same forcing, as a
  function of time. As expected, the difference converges to zero
  (eventually with exponential rate) as $t\to \infty$, demonstrating
  the absence of spurious solutions.}
\end{figure}
