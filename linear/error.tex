\section{Error estimates}
\label{se:error}
Optimal \emph{a priori} error estimates follow by applying our
stability estimates to a discrete equation for the difference between
the numerical solution and a projection of the true solution.
We define 
\begin{equation}
\begin{split}
\chi &\equiv \Pi u - u, \\
\rho & \equiv \pi \eta - \eta, \\
\theta_h & \equiv \Pi u - u_h,  \\
\zeta_h & \equiv \pi \eta - \eta_h.
\end{split}
\end{equation}

The projections $\Pi u$ and $\pi \eta$ satisfy the first-order system
\begin{equation}
\begin{split}
\left( \frac{1}{H} \Pi u_{t} , v_h \right) 
+ \frac{1}{\epsilon} \left( \frac{f}{H} \Pi u^\perp , v_h \right) 
- \frac{\beta}{\epsilon^2} \left( \pi \eta ,
\nabla \cdot v_h \right) + \left( \frac{C}{H} \Pi u , v_h \right) & =
\left( F + \frac{f}{\epsilon H} \chi + \frac{1}{H}\chi_t +\frac{C}{H} \chi , v_h \right)
, \\
\left( \pi \eta_{t} , w_h \right) + \left( \nabla \cdot \Pi u  , w_h \right)& = 0.
\end{split}
\end{equation}
Subtracting the discrete equation~(\ref{eq:discrete_mixed}) from this gives
\begin{equation}
\label{eq:firstordererror}
\begin{split}
\left( \frac{1}{H} \theta_{h,t} , v_h \right) 
+ \frac{1}{\epsilon} \left( \frac{f}{H} \theta_h^\perp , v_h \right) 
- \frac{\beta}{\epsilon^2} \left( \zeta_h ,
\nabla \cdot v_h \right) + \left( \frac{C}{H} \theta_h , v_h \right) & =
\left( \frac{f}{\epsilon H} \chi +  \frac{1}{H} \chi_t + \frac{C}{H} \chi , w_h \right)
, \\
\left( \zeta_{h,t} , w_h \right) + \left( \nabla \cdot \theta_h  , w_h \right)& = 0.
\end{split}
\end{equation}

By choosing the initial conditions for the discrete problem as
$u_h(\cdot,0) = \Pi u_0$ and
$\eta_h(\cdot,0) = \pi \eta_0$, the initial conditions for these error equations are
\begin{equation}
\begin{split}
\theta_h(\cdot,0) & = 0, \\
\eta_h(\cdot,0) & = 0.
\end{split}
\end{equation}

We start with $L^2$ estimates for the height and momentum variables,
based on the stability result for the first order system. 
\begin{proposition}
\label{prop:L2discerr}
For any $t > 0$, provided that $u,u_t \in L^2([0,t],H^{k+\sigma}(\Omega))$, 
\begin{equation}
\begin{split}
& \frac{1}{2} \weightednormattime{\theta_h}{\frac{1}{H}}{t}^2
+ \frac{\beta}{2\epsilon^2} \normattime{\zeta_h}{t}^2 \\
\leq
&
\frac{C_\pi^2 h^{2\left( k + \sigma \right)}}{C_* H_*}
\int_0^t 
\frac{1}{\epsilon}
\weightedseminormattime{u}{k+\sigma}{s}^2
+ \weightedseminormattime{u_t}{k+\sigma}{s}^2
+ C^* \weightedseminormattime{u}{k+\sigma}{s}^2 ds.
\end{split}
\end{equation}
\end{proposition}
\begin{proof}
We apply Proposition~\ref{prop:firstorderstability} to~(\ref{eq:firstordererror}) to find
\begin{equation}
\frac{1}{2} \weightednormattime{\theta_h}{\frac{1}{H}}{t}^2
+ \frac{\beta}{2\epsilon^2} \normattime{\zeta_h}{t}^2
\leq
\frac{1}{2C_*} \int_0^t \weightednormattime{\frac{f}{\epsilon H} \chi +  \frac{1}{H} \chi_t +
  \frac{C}{H} \chi }{H}{s}^2 ds.
\end{equation}

Note that for any $g$,
\[
\weightednorm{\frac{1}{H} g}{H}^2
= \int_\Omega H \left( \frac{1}{H} \left| g \right| \right)^2 dx
= \int_\Omega \frac{1}{H} \left| g \right|^2 dx
= \weightednorm{g}{\frac{1}{H}}^2.
\]
Using this, that $(a+b)^2 \leq 2 \left( a^2 + b^2 \right)$, and norm 
equivalence bounds
the right-hand side above by
\[
\begin{split}
& \frac{1}{C_*H_*} \int_0^t 
\normattime{\frac{f}{\epsilon} \chi}{s}^2 +
\normattime{\chi_t}{s}^2 +
\normattime{C \chi}{s}^2 ds \\
\leq
& \frac{1}{C_*H_*} \int_0^t 
\frac{1}{\epsilon} \normattime{\chi}{s}^2 +
\normattime{\chi_t}{s}^2 +
C^* \normattime{\chi}{s}^2 ds
\end{split}
\]
and the approximation estimate~(\ref{eq:PiL2}) finishes the proof.
\end{proof}

Since
\[
\frac{1}{2} \weightednorm{\left( u - u_h
  \right)}{\frac{1}{H}}^2
+
\frac{\beta}{2 \epsilon^2} \norm{\eta - \eta_h}^2
\leq 
\weightednorm{\rho}{\frac{1}{H}}^2 + 
\weightednorm{\zeta_h}{\frac{1}{H}}^2
+ \frac{\beta}{2 \epsilon^2} \norm{\chi}^2
+ \frac{\beta}{2 \epsilon^2} \norm{\theta}^2,
\]
we combine this result with the approximation estimates to obtain
\begin{theorem}
\label{th:L2 bounds}
If the above hypotheses hold, and also $u \in
L^\infty([0,t];H^{k+\sigma}(\Omega))$ and $\eta \in
L^\infty([0,t];H^k(\Omega))$, we have the error estimate 
\begin{equation}
\begin{split}
\frac{1}{2} \weightednormattime{\left( u - u_h
  \right)}{\frac{1}{H}}{t}^2
+
\frac{\beta}{2 \epsilon^2} \normattime{\left( \eta - \eta_h
  \right)}{t}^2
& \leq \frac{C_\Pi^2 h^{2\left( k + \sigma\right)}}{H_*}
\weightedseminormattime{ u }{ k + \sigma }{ t }^2
+
\frac{C_\pi^2 \beta h^{2k}}{\epsilon^2}
\weightedseminormattime{ \eta }{k}{t}^2 \\
& + 
\frac{2 C_\pi^2 h^{2\left( k + \sigma \right)}}{C_* H_*}
\int_0^t \weightedseminormattime{u_t}{k+\sigma}{s}^2
+ C^* \weightedseminormattime{u}{k+\sigma}{s}^2 ds.
\end{split}
\end{equation}
\end{theorem}

Note that our bound on the error equations in Proposition~\ref{prop:L2discerr}
depends only on the approximation properties of the velocity space,
while the full error in the finite element
solution depends on the approximation properties of both spaces.
Consequently, the velocity approximation using BDM elements is
suboptimal.  Using RT or BDFM elements, both fields are approximated
to optimal order. 

Now, we use our estimates based on the second-order system to obtain
error estimates for the time derivative and divergence of the momentum.
The projection $\Pi u$ satisfies the perturbed equation
\begin{equation}
\begin{split}
& \left( \frac{1}{H} \Pi u_{tt} , v_h \right) 
+ \frac{1}{\epsilon} \left( \frac{f}{H} \Pi u_{t}^\perp , v_h \right) 
+ \frac{\beta}{\epsilon^2} \left( \nabla \cdot \Pi u , \nabla \cdot v_h \right)
+ \left( \frac{C}{H} \Pi u_{t} , v_h \right) \\
= &
\left( \frac{1}{H} \chi_{tt} , v_h \right)
+ \frac{1}{\epsilon} \left( \frac{1}{H} \chi^\perp_t , v_h \right)
+ \left( \frac{C}{H} \chi_t , v_h \right)
+ \left( \widetilde{F}  , v_h \right).
\end{split}
\label{eq:secondorderprojeq}
\end{equation}
As in the first-order case, we have 
$\theta_h \equiv \Pi u - u_h$, and
subtracting~(\ref{eq:secondorderdiscrete}) from
(\ref{eq:secondorderprojeq}) gives
\begin{equation}
\begin{split}
& \left( \frac{1}{H} \theta_{h,tt} , v_h \right) 
+ \frac{1}{\epsilon} \left( \frac{f}{H} \theta_{h,t}^\perp , v_h \right) 
+ \frac{\beta}{\epsilon^2} \left( \nabla \cdot \theta_{h} , \nabla \cdot v_h \right)
+ \left( \frac{C}{H} \theta_{h,t} , v_h \right) \\
= &
\left( \frac{1}{H} \chi_{tt} , v_h \right)
+ \frac{1}{\epsilon} \left( \frac{f}{H} \chi^\perp_t , v_h \right)
+ \left( \frac{C}{H} \chi_t , v_h \right).
\end{split}
\label{eq:thetaeq}
\end{equation}
Theorem~\ref{thm:dampedstable} and approximation
estimates for $\chi$ give this result.
\begin{proposition}
Let $\alpha = \alpha_* = \min\{ \alpha_1 , \alpha_2\}$ and suppose
that $u_t, u_{tt} \in L^1(0,T;H_{k+1})$.  Then
\begin{equation}
\frac{1}{2} \weightednorm{\theta_{h,t}}{\frac{1}{H}}^2
+ \frac{\beta}{2\epsilon^2} \norm{\nabla \cdot \theta_h}^2
\leq \frac{K_{\alpha_*}C_\Pi^2 h^{2\left(k+\sigma\right)}}{\alpha_* H_*} 
\int_0^t e^{\frac{\alpha_*}{3}\left( s - t \right)} 
\left(
\weightedseminorm{u_{tt}}{k+1}^2
+ \left( \frac{1}{\epsilon} + C^* \right)
\weightedseminorm{u_{t}}{k+1}^2
\right).
\end{equation}
\end{proposition}
\begin{proof}
Applying the stability estimate to~(\ref{eq:thetaeq}), noting that
$\theta_h =0$ at $t=0$ gives
\begin{equation}
\frac{1}{2} \weightednorm{\theta_{h,t}}{\frac{1}{H}}^2
+ \frac{\beta}{2\epsilon^2} \norm{\nabla \cdot \theta_h}^2
\leq \frac{K_{\alpha_*}}{\alpha_*}
\int_0^t e^{-\frac{\alpha_*}{3}\left( s - t \right)} 
\left(
\weightednorm{\xi_{tt}}{\frac{1}{H}}^2
+ \left( \frac{1}{\epsilon} + C^* \right)
\weightednorm{\xi_{t}}{\frac{1}{H}}^2
\right),
\end{equation}
and applying the norm equivalence and approximation estimate~(\ref{eq:piL2}) gives the result.
\end{proof}

It is straightforward to get from here to a bound on the error
\begin{equation}
\varepsilon^2 \equiv
\frac{1}{2} \weightednormattime{\left( u_t - u_{h,t}
  \right)}{\frac{1}{H}}{t}^2
+
\frac{\beta}{2 \epsilon^2} \normattime{\nabla \cdot \left( u - u_h
  \right)}{t}^2.
\end{equation}

\begin{theorem}
\label{th:exponential error}
If the above assumptions hold, and also $u_t,u_{tt} \in L^\infty([0,t];H^{k+1}(\Omega))$, then
\begin{equation}
\begin{split}
\varepsilon^2 
& \leq \frac{C_\Pi^2 h^{2\left( k + \sigma\right)}}{H_*}
\weightedseminormattime{ u_t }{ k + \sigma }{ t }^2
+
\frac{C_\pi^2 \beta h^{2k}}{\epsilon^2}
\weightedseminormattime{ u }{k+1}{t}^2 \\
& +
\frac{2K_{\alpha_*}C_\Pi^2 h^{2\left(k+\sigma\right)}}{\alpha_* H_*} 
\int_0^t e^{-\frac{\alpha_*}{3}\left( s - t \right)} 
\left(
\weightedseminorm{u_{tt}}{k+1}^2
+ \left( \frac{1}{\epsilon} + C^* \right)
\weightedseminorm{u_{t}}{k+1}^2
\right).
\end{split}
\end{equation}
\end{theorem}


