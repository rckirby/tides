%%%%%%%%%%%%%%%%%%%%%%% file template.tex %%%%%%%%%%%%%%%%%%%%%%%%%
%
% This is a general template file for the LaTeX package SVJour3
% for Springer journals.          Springer Heidelberg 2010/09/16
%
% Copy it to a new file with a new name and use it as the basis
% for your article. Delete % signs as needed.
%
% This template includes a few options for different layouts and
% content for various journals. Please consult a previous issue of
% your journal as needed.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% First comes an example EPS file -- just ignore it and
% proceed on the \documentclass line
% your LaTeX will extract the file if required
\begin{filecontents*}{example.eps}
%!PS-Adobe-3.0 EPSF-3.0\section{Introduction}

Finite element methods are attractive for modelling the world's oceans
since implemention with triangular cells provides a
means to accurately represent coastlines and topography
~\cite{We_etal2010}. In the last decade or
so, there has been much discussion about the best choice of mixed
finite element pairs to use as the horizontal discretization for
atmosphere and ocean models. In particular, much attention has been
paid to the properties of numerical dispersion relations obtained when
discretizing the rotating shallow water equations
\cite{Da2010,CoHa2011,CoLaReLe2010,RoRoPo2007,Ro2005,RoBe2009,RoRo2008,Ro2012}.
In this paper we take a different angle, and study the behavior of
discretizations of forced-dissipative rotating shallow-water
equations, which are used for predicting global barotropic tides. The
main point of interest here is whether the discrete solutions approach
the correct long-time solution in response to quasi-periodic forcing.
In particular, we study the behavior of the linearized energy. Since
this energy only controls the divergent part of the solution, as we
shall see later, it is important to choose finite element spaces
where there is a natural discrete Helmholtz decomposition, and where
the Coriolis term projects the divergent and divergence-free
components of vector fields correctly onto each other. Hence, we
choose to concentrate on the mimetic, or compatible, finite element
spaces (\emph{i.e.} those which arise naturally from the finite
element exterior calculus \cite{arnold2006finite}) which were proposed
for numerical weather prediction in \cite{CoSh2012}. In that paper, it
was shown that the discrete equations have an exactly steady
geostrophic state (a solution in which the Coriolis term balances the
pressure gradient) corresponding to each of the divergence-free
velocity fields in the finite element space; this approach was
extended to develop finite element methods for the nonlinear rotating
shallow-water equations on the sphere that can conserve energy,
enstrophy and potential vorticity
\cite{RoHaCoMc2013,CoTh2014,McCo2014}. Here, we shall make use of the
discrete Helmholtz decomposition in order to show that mixed finite
element discretizations of the forced-dissipative linear rotating
shallow-water equations have the correct long-time energy
behavior. Since we are studying linear equations, these energy
estimates then provide finite time error bounds.

Predicting past and present ocean tides is important because they have
a strong impact on sediment transport and coastal flooding, and hence
are of interest to geologists. Recently, tides have also received a
lot of attention from global oceanographers since breaking internal
tides provide a mechanism for vertical mixing of temperature and
salinity that might sustain the global ocean circulation
\cite{MuWu1998,GaKu2007}. A useful tool for predicting tides are the
rotating shallow water equations, which provide a model of the
barotropic (\emph{i.e.}, depth-averaged) dynamics of the ocean. When
modelling global barotropic tides away from coastlines, the nonlinear
advection terms are very weak compared to the drag force, and a
standard approach is to solve the linear rotating shallow-water
equations with a parameterised drag term to model the effects of
bottom friction, as described in \cite{La45}. This approach can be
used on a global scale to set the boundary conditions for a more
complex regional scale model, as was done in \cite{Hi_etal2011}, for
example. Various additional dissipative terms have been proposed to
account for other dissipative mechanisms in the barotropic tide, due
to baroclinic tides, for example \cite{JaSt2001}. 

As mentioned above, finite element methods provide useful
discretizations for tidal models since they can be used on
unstructured grids which can seamless couple global tide structure
with local coastal dynamics. A discontinuous Galerkin approach was
developed in \cite{salehipour2013higher}, whilst continuous finite
element approaches have been used in many studies \cite[for example]{kawahara1978periodic,Le_etal2002,FoHeWaBa1993}. The lowest order Raviart-Thomas element for
velocity combined with $P_0$ for height was proposed for coastal tidal
modeling in \cite{walters2005coastal}; this pair fits into the framework 
that we discuss in this paper.

In this paper we will restrict attention to the linear bottom drag
model as originally proposed in \cite{La45}. We are aware that the
quadratic law is more realistic, but the linear law is more amenable
to analysis and we believe that the correct energy behavior of
numerical methods in this linear setting already rules out many
methods which are unable to correctly represent the long-time solution
which is in geotryptic balance (the extension to geostrophic balance
of the three way balance between Coriolis, the pressure gradient and
the dissipative term). In the presence of quasiperiodic time-varying
tidal forcing, the equations have a time-varying attracting solution
that all solutions converge to as $t\to \infty$. In view of this, we
prove the following results which are useful to tidal modellers (at
least, for the linear law):
\begin{enumerate}
\item For the mixed finite element methods that we consider, the
  spatial semidiscretization also has an attracting solution 
  in the presence of time-varying forcing.
\item This attracting solution converges to time-varying attracting
  solution of the unapproximated equations.
\end{enumerate}

Global problems require tidal simulation on manifolds rather than
planar domains.  For simplicity, our description and analysis will
follow the latter case.  However, our numerical results include
the former case.  Recently, Holst and Stern~\cite{holst2012geometric}
have demonstrated that finite element analysis on discretized
manifolds can be handled as a variational crime.  We summarize these
findings and include an appendix at the end demonstrating how to apply
their techniques to our own case.  This suggests that the extension to
manifolds presents technicalities rather than difficulties to the
analysis we provide here.

The rest of this paper is organised as follows. In Section
\ref{se:model} we describe the finite element modelling framework
which we will analyse. In Section \ref{se:prelim} we provide
some mathematical preliminaries. In Section \ref{se:energy} we derive
energy stability estimates for the finite element tidal equations. In 
Section \ref{se:error} we use these energy estimates to obtain error
bounds for our numerical solution.  Appendix~\ref{ap:bendy} includes
the discussion of embedded manifolds.

%%BoundingBox: 19 19 221 221
%%CreationDate: Mon Sep 29 1997
%%Creator: programmed by hand (JK)
%%EndComments
gsave
newpath
  20 20 moveto
  20 220 lineto
  220 220 lineto
  220 20 lineto
closepath
2 setlinewidth
gsave
  .4 setgray fill
grestore
stroke
grestore
\end{filecontents*}
%
\RequirePackage{fix-cm}
%
\documentclass{svjour3}                     % onecolumn (standard format)
%\documentclass[smallcondensed]{svjour3}     % onecolumn (ditto)
%\documentclass[smallextended]{svjour3}       % onecolumn (second format)
%\documentclass[twocolumn]{svjour3}          % twocolumn
%
\smartqed  % flush right qed marks, e.g. at end of proof
%
\usepackage{graphicx}
\usepackage{amsmath,amssymb}
\usepackage{url}
%
% \usepackage{mathptmx}      % use Times fonts if available on your TeX system
%
% insert here the call for the packages your document requires
%\usepackage{latexsym}
% etc.
%
% please place your own definitions here and don't use \def but
% \newcommand{}{}
\newcommand{\hdiv}{H(\mathrm{div})}
\newcommand{\norm}[1]{\left\| #1 \right\|}
\newcommand{\seminorm}[1]{\left| #1 \right|}
\newcommand{\weightednorm}[2]{\norm{#1}_{#2}}
\newcommand{\weightedseminorm}[2]{\seminorm{#1}_{#2}}
\newcommand{\weightednormattime}[3]{\weightednorm{#1\left( \cdot , #3
    \right)}{#2}}
\newcommand{\normattime}[2]{\norm{#1\left( \cdot , #2 \right)}}
\newcommand{\weightedseminormattime}[3]{\weightedseminorm{#1\left( \cdot , #3 \right)}{#2}}
\DeclareMathOperator{\Ro}{Ro}
\DeclareMathOperator{\Id}{Id}
%
% Insert the name of "your journal" with
\journalname{Numerische Mathematik}
%
\begin{document}

\title{Mixed finite elements for global tide models\thanks{
CJC acknowledges support from NERC grant NE/I016007/1 and RCK
from NSF grant CCF-1325480.}
}
%\subtitle{Do you have a subtitle?\\ If so, write it here}

%\titlerunning{Short form of title}        % if too long for running head

\author{Colin J. Cotter         \and
        Robert C. Kirby %etc.
}

%\authorrunning{Short form of author list} % if too long for running head

\institute{
Colin J. Cotter \at
Imperial College London, South Kensington Campus;
London SW7 2AZ. \\
\email{colin.cotter@imperial.ac.uk}
\and
Robert C. Kirby \at
Baylor University, Department of Mathematics;
One Bear Place \#97328;
Waco, TX 76798-7328 \\
Tel.: +1-254-710-4846 \\
Fax: +1-254-710-3569 \\
\email{Robert\_Kirby@baylor.edu}
}

\date{Received: date / Accepted: date}
% The correct dates will be entered by the editor


\maketitle

\begin{abstract}
We study mixed finite element methods for the linearized rotating
shallow water equations with linear drag and forcing terms.
By means of a strong energy estimate for an equivalent second-order
formulation for the linearized momentum, we prove long-time stability
of the system without energy accumulation --  the geotryptic state.
\emph{A priori} error estimates for the linearized momentum and free
surface elevation are given in $L^2$ as well as for the time
derivative and divergence of the linearized momentum.  Numerical
results confirm the theoretical results regarding both energy damping
and convergence rates.

\keywords{Finite element method \and global tidal models \and H(div) elements \and energy estimates}
% \PACS{PACS code1 \and PACS code2 \and more}
\subclass{65M12 \and 65M60 \and 35Q86}
\end{abstract}

\input{intro.tex}
\input{modelnondim.tex}
\input{preliminaries.tex}
\input{stability.tex}
\input{error.tex}
\input{numres.tex}
\input{conc.tex}

\appendix
\input{appendix-bendy.tex}



%\begin{acknowledgements}
%If you'd like to thank anyone, place your comments here
%and remove the percent signs.
%\end{acknowledgements}

% BibTeX users please use one of
%\bibliographystyle{spbasic}      % basic style, author-year citations
\bibliographystyle{spmpsci}      % mathematics and physical sciences
%\bibliographystyle{spphys}       % APS-like style for physics
\bibliography{bib}   % name your BibTeX data base


\end{document}
% end of file template.tex

