from numpy import *
import pylab as py

h = array([1.0, 0.5, 0.25, 0.125, 0.0625])
eta_l2errors = array([0.0118976875263, 0.00444469244147, 0.00138311689884, 
                      0.000305983277111, 8.18073339987e-05])

py.loglog(h,0.1*h**2,'-')
py.loglog(h,eta_l2errors,'.')
py.axis([0.05,1.0,1.0e-5,0.1])
py.xlabel('$h$')
py.ylabel('Errors')
py.legend(('$\propto h^2$','$\eta$ L2  error'),loc=4)
py.title('$\eta$ L2 error for RT1-DG1')
py.show()
