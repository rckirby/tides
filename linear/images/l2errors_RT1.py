from numpy import *
import pylab as py

h = array([1.0, 0.5, 0.25, 0.125, 0.0625])
eta_l2errors = array([0.00901528317492, 0.0120679148752,
                      0.00675189962207, 0.00342159892971, 0.00171171722968])

py.loglog(h,0.1*h,'-')
py.loglog(h,eta_l2errors,'.')
py.axis([0.05,1.0,1.0e-3,0.1])
py.xlabel('$h$')
py.ylabel('Errors')
py.legend(('$\propto h$','$\eta$ L2  error'),loc=4)
py.title('$\eta$ L2 error for RT0-DG0')
py.show()
