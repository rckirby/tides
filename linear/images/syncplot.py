from numpy import *
import pylab as py

L2u = fromfile('../python/L2us.dat',sep=' ')
L2eta = fromfile('../python/L2etas.dat',sep=' ')
time = 0.01*arange(0,L2u.size)

py.loglog(time,L2u,'-')
py.loglog(time,L2eta,'--')
py.legend(('L2 norm of $u_1-u_2$','L2 norm of $\eta_1-\eta_2$'),loc=0)
py.xlabel('Time')
py.ylabel('Difference in L2')
py.title('Synchronization of solutions')
py.show()
