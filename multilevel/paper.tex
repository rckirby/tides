\documentclass[11pt]{article}
\oddsidemargin 0.0in
\evensidemargin 0.0in
\topmargin -0.5in
\textwidth 6.5in
\textheight 9.0in
\usepackage{amssymb,url,amsmath,graphicx,amsthm,stmaryrd}
\usepackage{tikz}


\def\MM#1{\boldsymbol{#1}}
\newcommand{\pp}[2]{\tfrac{\partial #1}{\partial #2}}
\newcommand{\ppp}[2]{\tfrac{\partial^2 #1}{\partial #2^2}}
\newcommand{\dede}[2]{\tfrac{\delta #1}{\delta #2}}
\newcommand{\dd}[2]{\tfrac{\diff#1}{\diff#2}}
\newcommand{\dt}[1]{\diff\!#1}
\def\MM#1{\boldsymbol{#1}}
\DeclareMathOperator{\diff}{d}
\DeclareMathOperator{\DIV}{DIV}
\DeclareMathOperator{\D}{D}
\bibliographystyle{plain}
\newcommand{\vecx}[1]{\MM{#1}}
\newtheorem{definition}{Definition}
\newcommand{\code}[1]{{\ttfamily #1}} 

\newcommand{\hdiv}{H(\mathrm{div})}
\newcommand{\hodiv}{H_0(\mathrm{div})}

\newcommand{\bfe}{\mathbf{e}}
\newcommand{\bfv}{\mathbf{v}}

\newtheorem{theorem}{Theorem}[section]
\newtheorem{proposition}{Proposition}[section]
\newtheorem{lemma}{Lemma}[section]
\newtheorem{remark}{Remark}[section]
\newtheorem{example}{Example}[section]


\title{Mixed finite element methods for a linearized multilayer shallow water model}

\date{\today}
\author{Colin J.~Cotter \and P.~Jameson Graber \and Robert C.~Kirby \and Alan Mullenix}

\begin{document}
\maketitle
\section{Introduction}
Describe the tide modeling problem and need for linearized multilayer problems

\section{The physical model and its discretization}

We consider a series of layers of fluid, with the top layer having
thickness $D_1$, the next layer $D_2$, and so on until $D_N$ for the
bottom layer (oceanographers count downwards) with bottom boundary
$z=b(x,y)$. The density in each layer $i$ is $\rho_i$, and the
horizontal velocity $\MM{u}_i$.  We have constants $g$ (acceleration
due to gravity) and $f$ (Coriolis parameter).

We assume that the pressure is hydrostatic, meaning that the pressure
in each layer $i$ satisfies
\begin{equation}
  \pp{p}{z}|_i = -\rho_i g,
\end{equation}
so $p|_i = -\rho_i gz + c_i$ in each layer.

Using $p=0$ at the top surface, we have
\begin{equation}
  p|_1 = \rho_1 g\left(\sum_{j=1}^N D_i + b - z\right).
\end{equation}
Evaluating this at the bottom of the top layer gives
\begin{equation}
p|_1(z=\sum_{j=2}^N+b)=\rho_1 g D_1.
\end{equation}
Then,
\begin{equation}
  p|_2 = \rho_2\left(\sum_{j=2}^N D_i + b - z + \rho_1 g D_1\right).
\end{equation}
By induction or pattern-matching, we have
\begin{align}
  p|_i &= \rho_i\left(\sum_{j=i}^N D_i + b - z + \sum_{i=1}^{j-1}\frac{\rho_j}
  {\rho_i} g D_j\right),\\
  &= \rho_i\left(\sum_{j=1}^N D_i + b - z + \sum_{i=1}^{j-1}\frac{\rho_j-\rho_i}{\rho_i}
  g D_j\right),
\end{align}

Under the assumption that the motion is columnar (i.e. horizontal
velocity is independent of $z$), the horizontal component of
the momentum equation becomes (after dividing by $\rho_i)$
\begin{equation}
  \pp{\MM{u}_i}{t} +
  \MM{u_i}\cdot\nabla \MM{u}_i + f\MM{u}_i^\perp
  = -g\nabla \left(\sum_{j=1}^N D_j + \sum_{j=1}^{i-1} \frac{\rho_j-\rho_i}{\rho_i}
  D_j + b\right) - \frac{C(|\MM{u}_N|)}{D_i}\delta_{iN}\MM{u}_N + \frac{F(t)}{D_i},
\end{equation}
where we added a parameterisation for bottom drag, with $C(s)$ the
damping function, $\delta_{iN}$ is equal to 1 if $i=N$ and zero
otherwise, and $F(t)$ is the barotropic tidal forcing. The rationale
for the scaling with $D_i$ is that the drag is due to turbulence
assumed to occur in the bottom layer only. This turbulent flow exerts
an effective damping force proportional to the velocity in the bottom
layer, so the depth averaged momentum source is $\int_b^{b+D_N} F(u)
\diff z$, and then we divide by $D_i$ to get the equation for
$\MM{u}$.

The steady solutions are $\MM{u}_i=0$ $i=1,\ldots, N$, and
$D_i=\bar{D}_i=$constant for $i<N$, and $D_N - b = \bar{D}_N-b=$constant.  To
linearise, we write $D_i = \bar{D}_i+D'_i$, where $\bar{D}_i$ is the
thickness of the layer when the system is at rest.  We assume
that $D'_i$ and $\MM{u}_i$ are small, and retain only the linear terms
in the advection terms as well as replacing $D_i$ by $\bar{D}_i$ in the
forcing terms.

\begin{subequations}
\label{eq:mlpde}
\begin{align}
  \pp{\MM{u}_i}{t} + f\MM{u}_i^\perp
  = -g\nabla \left(\sum_{j=1}^N D_j' + \sum_{j=1}^{i-1} \frac{\rho_j-\rho_i}{\rho_i}
  D_j'\right) - \frac{C(|\MM{u}_N|)}{\bar{D}_i}\delta_{iN}\MM{u}_N + \frac{F(t)}{\bar{D}_i}, \label{eq:mlpdeu} \\
  \pp{D_i'}{t} + \nabla\cdot \left(\bar{D}_i\MM{u}_i\right) = 0 \label{eq:mlpdeD}.
\end{align}
\end{subequations}
Sometimes a simplified model is used under the rigid lid assumption,
in which we assume that $\sum_{j=1}^ND_j+b$
is constant. This is relevant because typically $(\rho_j-\rho_i)/\rho_i$ is
small, and so there are very fast ``barotropic'' waves where $\MM{u}_i$
is independent of $i$, and much slower ``baroclinic'' waves where the
free surface is more-or-less flat. It is the baroclinic tides that become
interesting since that is where tidally-generated energy is thought to be
dissipated as turbulence away from the bottom boundary.

In order to put~\eqref{eq:mlpde} into a form more amenable for mixed finite elements and analysis in $H(\mathrm{div})$, we perform a few additional manipulations.  First, we make the substitution $\widetilde{\MM{u}}_i = D_i \MM{u}_i$  Then, we multiply each~\eqref{eq:mlpdeu} by $\rho_i$ and define $\mu_i \equiv D_i^{-1} \rho_i$.  Then, we let 
\(
\widetilde{F}_i (t) = \mu_i F(t)
\)
and
\(
\widetilde{C}(s) = C(D_i^{-1} s)
\)
and after dropping primes on $D^\prime_i$ and tildes on $\widetilde{\MM{u}}_i$ and $\widetilde{F}_i$, we obtain

\begin{subequations}
\label{eq:mlpde2}
\begin{align}
  \mu_i \pp{\MM{u}_i}{t} + \mu_i f \MM{u}_i^\perp
  = -g\nabla \left(\sum_{j=1}^N A_{ij} D_j \right) - \mu_i C(|\MM{u}_N|)\delta_{iN}\MM{u}_N + F(t), \label{eq:mlpde2u} \\
  \pp{D_i}{t} + \nabla\cdot \MM{u}_i = 0 \label{eq:mlpde2D},
\end{align}
\end{subequations}
where
\begin{equation}
\label{eq:Adef}
A_{ij} = \rho_{\min\{i, j\}}.
\end{equation}


In this paper, we focus on the case of linear damping,
where $C$ is constant with respect to $|\MM{u}_n|$ but possibly spatially
varying.  When we consider damping, we will assume that $C$ is
nonnegative and bounded
\begin{equation}
  \label{eq:Cbounds}
  0 < C_* \leq C(\MM{x}) \leq C^* < \infty
\end{equation}
for all $\MM{x} \in \Omega$.  In~\cite{cotter2018mixed}, we have analyzed the case of
nonlinear damping for a single layer, and hope to extend this to the
multilayer case in the future.

Now, we let $D$ be the vector of all $N$ components $D_i$ and similar for $\MM{u}$.
Let $M$ be the diagonal matrix with $M_{ii} = \mu_i$ and $B$ the
matrix of all zeros except $B_{NN} = C$, we can rewrite the
system in vector notation as

\begin{align}
  \label{eq:mlvec}
  M \pp{\MM{u}}{t} + f M \MM{u}^\perp + g \nabla \left( A D
  \right)
  + B\MM{u} = 0, \\
  \pp{D}{t} + \nabla \cdot \MM{u} = F,
\end{align}
where we have the gradient and divergence working componentwise on vectors.  Note that here, $AD$ means the standard matrix-vector multiplication and that divergence and gradient applied to vectors of functions are defined componentwise.

Critical to our analysis, as in~\cite{CoKi,cotter2018mixed}, we work with an equivalent second order equation to obtain energy estimates.  We let
$\MM{\phi}$ satisfy $\pp{\MM{\phi}}{t} = \MM{u}$ and $\nabla \cdot \MM{\phi}
= D$.  With this identification, $\MM{\phi}$ satisfies
\begin{equation}
  \label{eq:primitive}
  M \ppp{\MM{\phi}}{t} + f M\pp{\MM{\phi}^\perp}{t} + B \pp{\MM{\phi}}{t}
  - g A \nabla \left(\nabla \cdot \MM{\phi}\right) = F.
\end{equation}

TODO: what are initial and boundary conditions for this equation?


At this point, we introduce the standard space $L^2(\Omega)$
consisting of square-integrable functions and $L^2_0(\Omega)$ its
subspace of zero-mean functions.  Let $\hdiv$
consist of vector-valued $L^2$ functions with divergences in
$L^2$ and $\hodiv$ the subspace with vanishing normal trace on the
boundary.  For any space $V$ (such as $L^2$ or $\hdiv$) we let $V^N$
denote the Cartesian product of $N$ copies of $V$.  

We use the standard notation of $(f,g)$  to denote the $L^2$ and $(L^2)^N$ inner products by
\begin{equation}
  (f, g) = \int_\Omega f(\MM{x}) g(\MM{x}) d\MM{x}
\end{equation}

and if $\MM{f}$ and $\MM{g}$ are vector-valued functions, $(\MM{f}, \MM{g})$ similarly integrates their dot product over $\Omega$.
For any function $w:\Omega \rightarrow \mathbb{R}$, we define the notation.
\begin{equation}
  (f, g)_w = \int_\Omega w(\MM{x}) f(\MM{x}) g(\MM{x}) d\MM{x},
\end{equation}
with the same modification for vector-valued functions.
If $w$ satisfies $0 \leq w_* \leq w(\MM{x}) \leq w_* < \infty$ for almost all $\MM{x} \in \Omega$, then these define equivalent weighted $L^2$ inner products.  

We can also equip the Cartesian product $V^N$ of an inner-product space with an $L^2$ inner product by
\begin{equation}
  (\MM{u}, \MM{v}) = \sum_{i=1}^N (\MM{u}_i, \MM{v}_i),
\end{equation}
and this works whether the component functions are themselves scalar- or vector-valued.

For any matrix-valued function $A: \Omega \rightarrow \mathbb{R}^{N\times N}$, we can define a weighted inner product on $V^N$ by
\begin{equation}
  (\MM{u}, \MM{v})_A = \sum_{i,j=1}^N (\MM{u}_i, \MM{v}_j)_{A_{ij}},
\end{equation}
assuming that $A$ is symmetric and uniformly positive-definite in $\Omega$.  Similarly, we define the norm
\begin{equation}
\| \MM{u} \|_{A} = \sqrt{(\MM{u}, \MM{u})_A}
\end{equation}


Let $W_h \subset L^2$ be the space of possibly discontinuous piecewise
polynomials of some degree $k\geq 0$ and $V_h \subset \hdiv$ 
the Raviart-Thomas space~\cite{RavTho77a}.  Our analysis applies without modification
to the Brezzi-Douglas-Fortin-Marini~\cite{brezzi1991mixed} spaces and, with minor
modifications in the approximation properties, the
Brezzi-Douglas-Marini~\cite{brezzi1985two} space.  Also, let $W_h^N \subset
(L^2)^N$ and $V_h^N \subset (\hdiv)^N$ consist of $N$ copies of the
finite element spaces.  

With this notation established, we now define the weak form of the system~\eqref{eq:mlvec}.  We seek $\MM{u}_h:[0,T] \rightarrow V_h^N$ and $D_h:[0,T] \rightarrow W_h^N$ such that

\begin{align}
  \label{eq:mlvecweak}
  \left(\pp{\MM{u}_H}{t}, \MM{v}_h\right)_M +
  \left( f \MM{u}^\perp_h, \MM{v}_h \right)_M
  - g \left(  D_h , \nabla \cdot \MM{v}_h \right)_A
  + \left( B\MM{u}_h , \MM{v}_h \right) = \left(F, \MM{v}_h \right), \\
  \left( \pp{D_h}{t}, \MM{w}_h \right)
  + \left( \nabla \cdot \MM{u}_h, \MM{w}_h \right) = 0,
\end{align}
for all $\MM{v}_h \in V_h^N$ and $\MM{w}_h \in W_h^N$.

We also give the weak form of the primitive equation~\eqref{eq:primitive}.  We seek $\MM{\phi}_h : [0,T] \rightarrow V_h^N$ such that
\begin{equation}
  \label{eq:primitiveweak}
  \left( \ppp{\MM{\phi}_h}{t}, \MM{v}_h \right)_M
  + \left( f \pp{\MM{\phi_h}^\perp}{t}, \MM{v}_h \right)_M
  + \left( B \pp{\MM{\phi}_h}{t}, \MM{v}_h \right)
  + g \left( \nabla \cdot \MM{\phi}_h, \nabla \cdot \MM{v}_h \right)_A = \left( F , \MM{v}_h \right).
\end{equation}
  
Because of the commuting diagram property of mixed finite element spaces, this discretization of the primitive equation is in fact a primitive for~\eqref{eq:mlvecweak} by the identifications $\pp{\MM{\phi}_h}{t} = \MM{u}_h$ and $\nabla \cdot \MM{\phi}_h = D_h$.  This identification allows energy estimates and other results established for~\eqref{eq:primitiveweak} to transfer to the first order form~\eqref{eq:mlvecweak} and vice versa.

\section{Properties of the coupling matrix}
\label{sec:A}
In this section, we identify several properties of the coupling matrix $A$
defined in~\eqref{eq:Adef} that will play a critical role in our analysis.

\begin{proposition}
  \label{prop:Asym}
  $A$ is symmetric.
\end{proposition}

\begin{proposition}
  \label{prop:Apd}
  If the densities are positive and strictly increasing (that is, if $0 < \rho_1 < \rho_2 < \dots < \rho_N$), then $A$ is positive-definite.
\end{proposition}
\begin{proof}
  Clearly, the result holds if $N=1$.  For $N > 1$, consider taking one step of Gaussian elimination on $A$, by which
  \begin{equation}
    \begin{split}
    \begin{bmatrix}
      \rho_1 & \rho_1 & \rho_1 & \dots & \rho_1 \\
      \rho_1 & \rho_2 & \rho_2 & \dots & \rho_2 \\
      & \vdots & & \ddots & \\
      \rho_1 & \rho_2 & \rho_3 & \dots & \rho_N
    \end{bmatrix}
    & \rightarrow
    \begin{bmatrix}
      \rho_1 & \rho_1 & \rho_1 & \dots & \rho_1 \\
      0 & \rho_2 - \rho_1 & \rho_2- \rho_1 & \dots & \rho_2-\rho_1 \\
      & \vdots & & \ddots & \\
      0 & \rho_2-\rho_1 & \rho_3-\rho_1 & \dots & \rho_N-\rho_1
    \end{bmatrix} \\
    & \equiv
    \begin{bmatrix}
      \rho_1 & \rho_1 & \rho_1 & \dots & \rho_1 \\
      0 & \rho_{1,2} & \rho_{1,2} & \dots & \rho_{1,2} \\
      & \vdots & & \ddots & \\
      0 & \rho_{1,2} & \rho_{1,3} & \dots & \rho_{1,N}
    \end{bmatrix}
    \end{split}
  \end{equation}
  Under our assumptions on $\rho_i$, the sequence $\{\rho_{1,i+1} \}_{i=1}^{N-1}$ is positive and strictly increasing.  Hence, by induction, Gaussian elimination without continues with all positive pivots, which is equivalent to positive-definiteness~\cite{strang1993introduction}.
\end{proof}

These properties allow us to use $A$ to define a suitable energy for the system.  In practice, the densities satisfy our hypotheses, although the difference between successive $\rho_i$ tends to be quite small.  This allows us to characterize the spectrum of $A$ in such a way as to explain the separation of solutions into fast and slow modes.

\begin{proposition}
  Let $\delta_\rho \equiv \rho_N - \rho_1$.  
  $A$ has one eigenvalue $\lambda$ such that
  \begin{equation}
    |\lambda - \rho_1 N| \leq (N-1) \delta_\rho,
  \end{equation}
  and $N-1$ eigenvalues between 0 and $(N-1)\delta_\rho$.
\end{proposition}
\begin{proof}
  Using the notation in the proof of Proposition~\ref{prop:Apd}, 
  we write $A$  as
  \begin{equation}
    \begin{split}
    A & = \begin{bmatrix}
      \rho_1 & \rho_1 & \dots & \rho_1 \\
      \rho_1 & \rho_1 & \dots & \rho_1 \\
      \vdots & & \ddots & \\
      \rho_1 & \rho_1 & \dots & \rho_1
    \end{bmatrix}
    +
    \begin{bmatrix}
      0 & 0 & \dots & 0 \\
      0 & \rho_{1,2} & \dots & \rho_{1,2} \\
      \vdots & & \ddots & \\
      0 & \rho_{1,2} & \dots & \rho_{1,N}
    \end{bmatrix} \\
    & \equiv \rho_1 1_N + \delta R,
    \end{split}
  \end{equation}
  where $1_N$ is the $N \times N$ with 1 in each entry.  Equivalently, we have
  \begin{equation}
    \rho_1 1_N = A - \delta R.
  \end{equation}
  The matrix $\rho_1 1_N$ has rank 1, and its eigenvalues are $N
  \rho_1$ with multiplicity 1 and $0$ with multiplicty $N-1$.
  Moreover, it is symmetric and so has an orthogonal matrix $V$ of
  eigenvectors.  

  From the Bauer-Fike theorem~\cite{bauer1960norms}, for each
  eigenvalue $\mu$ of $\rho_1 1_N$, there exists an eigenvalue $\mu$
  of $A$ such that
  \begin{equation}
    |\lambda - \mu| \leq \kappa_2(V) \| \delta R \|_2,
  \end{equation}
  where $\| \cdot \|_2$ denotes the matrix 2-norm and $\kappa_2$ the
  2-norm condition number.  Since $V$ is orthogonal, it has unit
  condition number, and we must just bound the 2-norm of $\delta R$.

  $\delta R$ is symmetric and semi-definite, so its 2-norm coincides
  with its spectral radius.  An upper bound on the spectral radius
  of $\delta R$ of
  \begin{equation}
    \sum_{i=2}^N \rho_{1,i} 
    \leq (N-1) (\rho_N - \rho_1) = (N-1) \delta_\rho
  \end{equation}
  is readily obtained from the Gerschgorin Circle Theorem.  From Propositions~\ref{prop:Asym} and~\ref{prop:Apd}, $A$ must have real and positive eigenvalues.  Combining this with the Gerschgorin estimate puts $N-1$ eigenvalues in the interval $(0, (N-1) \delta_\rho)$.
\end{proof}

Consequently, if the difference between the maximal and minimal densities is small compared to the minimal one, we have a clear separation into a single fast mode and a conglomerate of very slow modes.

\section{Energy estimates}
We select $\MM{v} = \MM{\phi}_r$ in~\eqref{eq:primitiveweak} to find that
\begin{equation}
\label{eq:basicenergyrelation}
    \tfrac{d}{dt} \tfrac{1}{2} \| \tfrac{\partial \MM{\phi}}{\partial t} \|^2 + \tfrac{d}{\mu_n} \| \tfrac{\partial \MM{\phi}_n}{\partial t} \|^2 + \tfrac{d}{dt} \tfrac{1}{2} \| \nabla \cdot \MM{\phi} \|_A^2 = 0.
\end{equation}
We define the energy
\begin{equation}
\overline{E}(t) = \frac{1}{2} \| \pp{\phi}{t} \|^2 + \frac{1}{2} \| \nabla \cdot \phi \|_A^2.
\end{equation}
Equation~\eqref{eq:basicenergyrelation} proves that $\dd{\overline{E}}{t} \leq 0$, but it does not imply any particular rate.  However, we are able to prove the following exponential decay result:

TODO: separate bar energy from non-bar?
\begin{theorem}
\label{thm:energydecay}
Assumptions on $T$?  Look for that...
There exists a constant $\overline{C}$ such that
\begin{equation}
    \int_0^T \overline{E}(t) \ dt \leq \overline{C}\overline{E}(0).
\end{equation}
 and 
 \begin{equation}
    E(t) \leq \frac{T}{\overline{C}}e^{-\frac{\ln(\overline{C}/T)}{T} t} \equiv Ke^{-\omega t}.
\end{equation}
\end{theorem}
Proving this result will require considerable effort.  To do this, we will make a scaling substitution and an orthogonal coordinate change in order to tridiagonalize $A$, much as in the Lanczos procedure~\cite{lanczos1950iteration} or in the reduction of a generic Hermitian matrix to tridiagonal form before QR iteration~\cite{trefethen1997numerical}.  For current purposes, this is a theoretical construction that simplifies handling the pressure term while keeping the damping term manageable.  If we attempt to diagonalize the pressure term with, say, an eigendecomposition of $A$, the damping term then becomes dense.  

The following assures that the Lanczos construction in fact succeeds in $\mathbb{R}^n$ when the initial vector is the canonical basis vector $\bfe^n$:
\begin{proposition}
Let $\bfe^n$ be the $n$th canonical basis vector in $\mathbb{R}^n$. For any $n \times n$ matrix $B$, if $\bfe^n$ is not orthogonal to any eigenvector of $B$, then $B$'s eigenvalues are distinct. 
\end{proposition}
\begin{proof}
Suppose otherwise, that $B\bfv_1 = \lambda \bfv_1$, $B \bfv_2 = \lambda \bfv_2$ for $\bfv_1, \bfv_2$ independent. Observe that
$$ B((\bfv_2,\bfe^n)\bfv_1-(\bfv_1,\bfe^n)\bfv_2) = \lambda((\bfv_2,\bfe^n)\bfv_1-(\bfv_1,\bfe^n)v_2) \ \ \ \text{ and } \ \ \ ((\bfv_2,\bfe^n)\bfv_1-(\bfv_1,\bfe^n)\bfv_2),\bfe^n) = 0,$$
contradicting the assumption that $\bfe^n$ is not orthogonal to any eigenvectors of $B$.
\end{proof}

\begin{proposition}
\label{prop:Krylovbasis}
Let $B$ be any symmetric $n \times n$ matrix. If $(\bfe^n,\bfv) \neq 0$ for any eigenvector $\bfv$ of $B$, then the set $\left\{ \bfe^n, B\bfe^n, B^2\bfe^n, \dots, B^{n-1} \bfe^n \right\}$ forms a basis for $\mathbb{R}^n$.
\end{proposition}
\begin{proof}
Let $\{ \bfv_i \}_{i=1}^n$ be an eigenbasis.  Suppose $\sum_{i=1}^n c_i B^{i-1}\bfe^n = 0$, then
$$ 0 = \sum_{i=1}^n c_i \left( \sum_{j=1}^n (B^{i-1}\bfe^n,\bfv_j)\bfv_j \right) = \sum_{j=1}^n \left( \sum_{i=1}^n c_i(B^{i-1}\bfe^n,\bfv_j) \right)v_j = \sum_{j=1}^n \left( \sum_{i=1}^n c_i \lambda_i^{i-1} \right) (\bfe^n,\bfv_j) \bfv_j.$$
Since $(\bfe^n,\bfv_j) \neq 0$ for any $j$, then $\sum_{i=1}^n c_i \lambda_i^{i-1}$ must be zero.  Suppose the $c_i \neq 0$ for some $i$. Then the Vandermonde matrix $V$ given by $V_{ij} = \lambda_i^{j-1}$ is singular. The determinant of the Vandermonde matrix is well known as
$$ \text{det}(V) = \prod_{i,j = 1}^n (\lambda_i - \lambda_j),$$
but since the eigenvalues of $B$ are distinct by Proposition 1, it follows that the $c_i$ are all zero and a basis is obtained. 
\end{proof}

Now, we let $\widetilde{\MM{\psi}} = M^{1/2} \MM{\phi}$ in~\eqref{} and multiply through by $M^{-1/2}$.  For now, we let $A \leftarrow M^{-1/2} A M^{-1/2}$ (which is still symmetric and positive definite) and $B \leftarrow M^{-1/2} B M^{-1/2}$ and drop the tildes on $\MM{\psi}_h$ to obtain
\begin{equation}
  \label{eq:primitiveweakrescaled}
  \left( \ppp{\MM{\psi}_h}{t}, \MM{v}_h \right)
  + \left( f \pp{\MM{\psi}_h^\perp}{t}, \MM{v}_h \right)
  + \left( B \pp{\MM{\psi}_h}{t}, \MM{v}_h \right)
  + g \left( \nabla \cdot \MM{\psi}_h, \nabla \cdot \MM{v}_h \right)_A = \left( F , \MM{v}_h \right).
\end{equation}

\begin{proposition}
The set $\left\{ A^{i} \bfe^n \right\}_{i=0}^{n-1}$ forms a basis for $\mathbb{R}^n$.
\end{proposition}
\begin{proof}
In light of Proposition~\ref{prop:Krylovbasis}, we just need to show that $\bfe^n$ is not orthogonal to any eigenvector of $A$.  Let $A \bfv = \lambda \bfv$.  Since $A$ is SPD, $\lambda$ is positive (in particular, nonzero) real number.  If $(\bfe^n)^T \bfv = 0$, then $\bfv_n = 0$ and so $(A \bfv)_n = \lambda \bfv_n = 0$ as well.  This gives
$$ 0 = (A\bfv)_n = \sum_{i=1}^n A_{ni} \bfv_i = \sum_{i=1}^{n-1} A_{ni} \bfv_i = \sum_{i=1}^{n-1} \frac{\sqrt{\mu_{n-1}}}{\sqrt{\mu_{n}}}A_{(n-1)i} \bfv_i = \frac{\sqrt{\mu_{n-1}}}{\sqrt{\mu_n}}(A\bfv)_{n-1} = \lambda \bfv_{n-1}.$$
Since $\lambda \neq 0$, $\bfv_{n-1} = 0$ as well, and inductively, $\bfv = 0$.  Hence, by contradition, $\bfv$ cannot be orthogonal to $\bfe^n$.
\end{proof}

These results show that the Lanczos algorithm creates an orthogonal matrix $V$ with $\bfe^n$ in the first column such that $V^T A V$ is tridiagonal as well as SPD.


%\bibliographystyle{plain}
\bibliography{bib}

\end{document}
