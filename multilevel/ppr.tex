\documentclass[11pt]{article}
\oddsidemargin 0.0in
\evensidemargin 0.0in
\topmargin -0.5in
\textwidth 6.5in
\textheight 9.0in
\usepackage{amssymb,url,amsmath,graphicx,amsthm,stmaryrd}
\usepackage{tikz}


\def\MM#1{\boldsymbol{#1}}
\newcommand{\pp}[2]{\tfrac{\partial #1}{\partial #2}}
\newcommand{\ppp}[2]{\tfrac{\partial^2 #1}{\partial #2^2}}
\newcommand{\dede}[2]{\tfrac{\delta #1}{\delta #2}}
\newcommand{\dd}[2]{\tfrac{\diff#1}{\diff#2}}
\newcommand{\dt}[1]{\diff\!#1}
\def\MM#1{\boldsymbol{#1}}
\DeclareMathOperator{\diff}{d}
\DeclareMathOperator{\DIV}{DIV}
\DeclareMathOperator{\D}{D}
\bibliographystyle{plain}
\newcommand{\vecx}[1]{\MM{#1}}
\newtheorem{definition}{Definition}
\newcommand{\code}[1]{{\ttfamily #1}} 

\newcommand{\hdiv}{H(\mathrm{div})}
\newcommand{\hodiv}{H_0(\mathrm{div})}

\newcommand{\bfe}{\mathbf{e}}
\newcommand{\bfv}{\mathbf{v}}
\newcommand{\bfu}{\mathbf{u}}
\newcommand{\bfx}{\mathbf{x}}

\newtheorem{theorem}{Theorem}[section]
\newtheorem{proposition}{Proposition}[section]
\newtheorem{lemma}{Lemma}[section]
\newtheorem{remark}{Remark}[section]
\newtheorem{example}{Example}[section]


\title{Mixed finite element methods for a linearized multilayer shallow water model}

\date{\today}
\author{Colin J.~Cotter \and P.~Jameson Graber \and Robert C.~Kirby \and Alan Mullenix}

\begin{document}
\maketitle
\section{Introduction}
Describe the tide modeling problem and need for linearized multilayer problems

\section{The physical model}

We consider a series of layers of fluid, with the top layer having
thickness $D_1$, the next layer $D_2$, and so on until $D_N$ for the
bottom layer (oceanographers count downwards) with bottom boundary
$z=b(x,y)$. The density in each layer $i$ is $\rho_i$, and the
horizontal velocity $\MM{u}_i$.  We have constants $g$ (acceleration
due to gravity) and $f$ (Coriolis parameter).

We assume that the pressure is hydrostatic, meaning that the pressure
in each layer $i$ satisfies
\begin{equation}
  \pp{p}{z}|_i = -\rho_i g,
\end{equation}
so $p|_i = -\rho_i gz + c_i$ in each layer.

Using $p=0$ at the top surface, we have
\begin{equation}
  p|_1 = \rho_1 g\left(\sum_{j=1}^N D_i + b - z\right).
\end{equation}
Evaluating this at the bottom of the top layer gives
\begin{equation}
p|_1(z=\sum_{j=2}^N+b)=\rho_1 g D_1.
\end{equation}
Then,
\begin{equation}
  p|_2 = \rho_2\left(\sum_{j=2}^N D_i + b - z + \rho_1 g D_1\right).
\end{equation}
By induction or pattern-matching, we have
\begin{align}
  p|_i &= \rho_i\left(\sum_{j=i}^N D_i + b - z + \sum_{i=1}^{j-1}\frac{\rho_j}
  {\rho_i} g D_j\right),\\
  &= \rho_i\left(\sum_{j=1}^N D_i + b - z + \sum_{i=1}^{j-1}\frac{\rho_j-\rho_i}{\rho_i}
  g D_j\right),
\end{align}

Under the assumption that the motion is columnar (i.e. horizontal
velocity is independent of $z$), the horizontal component of
the momentum equation becomes (after dividing by $\rho_i)$
\begin{equation}
  \pp{\MM{u}_i}{t} +
  \MM{u_i}\cdot\nabla \MM{u}_i + f\MM{u}_i^\perp
  = -g\nabla \left(\sum_{j=1}^N D_j + \sum_{j=1}^{i-1} \frac{\rho_j-\rho_i}{\rho_i}
  D_j + b\right) - \frac{C(|\MM{u}_N|)}{D_i}\delta_{iN}\MM{u}_N + \frac{F(t)}{D_i},
\end{equation}
where we added a parameterisation for bottom drag, with $C(s)$ the
damping function, $\delta_{iN}$ is equal to 1 if $i=N$ and zero
otherwise, and $F(t)$ is the barotropic tidal forcing. The rationale
for the scaling with $D_i$ is that the drag is due to turbulence
assumed to occur in the bottom layer only. This turbulent flow exerts
an effective damping force proportional to the velocity in the bottom
layer, so the depth averaged momentum source is $\int_b^{b+D_N} F(u)
\diff z$, and then we divide by $D_i$ to get the equation for
$\MM{u}$.

The steady solutions are $\MM{u}_i=0$ $i=1,\ldots, N$, and
$D_i=\bar{D}_i=$constant for $i<N$, and $D_N - b = \bar{D}_N-b=$constant.  To
linearise, we write $D_i = \bar{D}_i+D'_i$, where $\bar{D}_i$ is the
thickness of the layer when the system is at rest.  We assume
that $D'_i$ and $\MM{u}_i$ are small, and retain only the linear terms
in the advection terms as well as replacing $D_i$ by $\bar{D}_i$ in the
forcing terms.

\begin{subequations}
\label{eq:mlpde}
\begin{align}
  \pp{\MM{u}_i}{t} + f\MM{u}_i^\perp
  = -g\nabla \left(\sum_{j=1}^N D_j' + \sum_{j=1}^{i-1} \frac{\rho_j-\rho_i}{\rho_i}
  D_j'\right) - \frac{C(|\MM{u}_N|)}{\bar{D}_i}\delta_{iN}\MM{u}_N + \frac{F(t)}{\bar{D}_i}, \label{eq:mlpdeu} \\
  \pp{D_i'}{t} + \nabla\cdot \left(\bar{D}_i\MM{u}_i\right) = 0 \label{eq:mlpdeD}.
\end{align}
\end{subequations}
Sometimes a simplified model is used under the rigid lid assumption,
in which we assume that $\sum_{j=1}^ND_j+b$
is constant. This is relevant because typically $(\rho_j-\rho_i)/\rho_i$ is
small, and so there are very fast ``barotropic'' waves where $\MM{u}_i$
is independent of $i$, and much slower ``baroclinic'' waves where the
free surface is more-or-less flat. It is the baroclinic tides that become
interesting since that is where tidally-generated energy is thought to be
dissipated as turbulence away from the bottom boundary.

In order to put~\eqref{eq:mlpde} into a form more amenable for mixed finite elements and analysis in $H(\mathrm{div})$, we perform a few additional manipulations.  First, we make the substitution $\widetilde{\MM{u}}_i = D_i \MM{u}_i$  Then, we multiply each~\eqref{eq:mlpdeu} by $\rho_i$ and define $\mu_i \equiv D_i^{-1} \rho_i$.  Then, we let 
\(
\widetilde{F}_i (t) = \mu_i F(t)
\)
and
\(
\widetilde{C}(s) = C(D_i^{-1} s)
\)
and after dropping primes on $D^\prime_i$ and tildes on $\widetilde{\MM{u}}_i$ and $\widetilde{F}_i$, we obtain

\begin{subequations}
\label{eq:mlpde2}
\begin{align}
  \mu_i \pp{\MM{u}_i}{t} + \mu_i f \MM{u}_i^\perp
  = -g\nabla \left(\sum_{j=1}^N A_{ij} D_j \right) - \mu_i C(|\MM{u}_N|)\delta_{iN}\MM{u}_N + F(t), \label{eq:mlpde2u} \\
  \pp{D_i}{t} + \nabla\cdot \MM{u}_i = 0 \label{eq:mlpde2D},
\end{align}
\end{subequations}
where
\begin{equation}
\label{eq:Adef}
A_{ij} = \rho_{\min\{i, j\}}.
\end{equation}


In this paper, we focus on the case of linear damping,
where $C$ is constant with respect to $|\MM{u}_n|$ but possibly spatially
varying.  When we consider damping, we will assume that $C$ is
nonnegative and bounded
\begin{equation}
  \label{eq:Cbounds}
  0 < C_* \leq C(\MM{x}) \leq C^* < \infty
\end{equation}
for all $\MM{x} \in \Omega$.  In~\cite{cotter2018mixed}, we have analyzed the case of
nonlinear damping for a single layer, and hope to extend this to the
multilayer case in the future.

Now, we let $D$ be the vector of all $N$ components $D_i$ and similar for $\MM{u}$.
Let $M$ be the diagonal matrix with $M_{ii} = \mu_i$ and $P_n$ the
matrix of all zeros except $(P_n)_{nn} = C$.  Also, redefining $A \equiv g A$, we can rewrite the
system in vector notation as

\begin{align}
  \label{eq:mlvec}
  M \pp{\MM{u}}{t} + f M \MM{u}^\perp + \nabla \left( A D
  \right)
  + P_n\MM{u} = 0, \\
  \pp{D}{t} + \nabla \cdot \MM{u} = F,
\end{align}
where we have the gradient and divergence working componentwise on vectors.  Note that here, $AD$ means the standard matrix-vector multiplication and that divergence and gradient applied to vectors of functions are defined componentwise.

In our previous work~\cite{CoKi,cotter2018mixed}, we analyzed a second-order primitive of this first-order system, and we are also able to do this here.  We can identify $\MM{u}$ with $\MM{\phi}_t$ and $D$ with $-\nabla \cdot \MM{\phi}$ to arrive at the equation
\begin{equation}
  \label{eq:primitive}
  M \ppp{\MM{\phi}}{t} + f M \pp{\MM{\phi}}{t}^\perp + P_n \pp{\MM{\phi}}{t} - \nabla \left( A \nabla \cdot \MM{\phi} \right) = \MM{F}
\end{equation}

Throughout, we will let $L^2(\Omega)$ be the standard space of square-integrable functions over $\Omega$, $(L^2(\Omega))^2$ the space of functions mapping $\Omega$ into $\mathbb{R}^2$, each component of which is in $L^2$.  $\hdiv$ will consist of the subspace of $(L^2(\Omega))^2$ with (weak) divergences also being in $L^2(\Omega)$.  When it is unambiguous, we may drop the domain of dependence $\Omega$.  We also let $\mathbf{V} = (\hdiv)^n$ denote the $n$-way Cartesian product of $\hdiv$.  

We will let $\{ \bfe_n \}_{i=1}^n$ denote the canonical basis in $\mathbb{R}^n$.


\section{Some linear algebra results}
\label{sec:A}
In this parenthetical section, we identify several properties of the coupling matrix $A$ defined in~\eqref{eq:Adef} that will play a critical role in our analysis, and also establish some facts that will enable us to perform our tridiagonalization argument.

\begin{proposition}
  \label{prop:Asym}
  $A$ is symmetric.
\end{proposition}

\begin{proposition}
  \label{prop:Apd}
  If the densities are positive and strictly increasing (that is, if $0 < \rho_1 < \rho_2 < \dots < \rho_N$), then $A$ is positive-definite.
\end{proposition}
\begin{proof}
  Clearly, the result holds if $N=1$.  For $N > 1$, consider taking one step of Gaussian elimination on $A$, by which
  \begin{equation}
    \begin{split}
    \begin{bmatrix}
      \rho_1 & \rho_1 & \rho_1 & \dots & \rho_1 \\
      \rho_1 & \rho_2 & \rho_2 & \dots & \rho_2 \\
      & \vdots & & \ddots & \\
      \rho_1 & \rho_2 & \rho_3 & \dots & \rho_N
    \end{bmatrix}
    & \rightarrow
    \begin{bmatrix}
      \rho_1 & \rho_1 & \rho_1 & \dots & \rho_1 \\
      0 & \rho_2 - \rho_1 & \rho_2- \rho_1 & \dots & \rho_2-\rho_1 \\
      & \vdots & & \ddots & \\
      0 & \rho_2-\rho_1 & \rho_3-\rho_1 & \dots & \rho_N-\rho_1
    \end{bmatrix} \\
    & \equiv
    \begin{bmatrix}
      \rho_1 & \rho_1 & \rho_1 & \dots & \rho_1 \\
      0 & \rho_{1,2} & \rho_{1,2} & \dots & \rho_{1,2} \\
      & \vdots & & \ddots & \\
      0 & \rho_{1,2} & \rho_{1,3} & \dots & \rho_{1,N}
    \end{bmatrix}
    \end{split}
  \end{equation}
  Under our assumptions on $\rho_i$, the sequence $\{\rho_{1,i+1} \}_{i=1}^{N-1}$ is positive and strictly increasing.  Hence, by induction, Gaussian elimination without continues with all positive pivots, which is equivalent to positive-definiteness~\cite{strang1993introduction}.
\end{proof}

These properties allow us to use $A$ to define a suitable energy for the system.  In practice, the densities satisfy our hypotheses, although the difference between successive $\rho_i$ tends to be quite small.  This allows us to characterize the spectrum of $A$ in such a way as to explain the separation of solutions into fast and slow modes.

\begin{proposition}
  Let $\delta_\rho \equiv \rho_N - \rho_1$.  
  $A$ has one eigenvalue $\lambda$ such that
  \begin{equation}
    |\lambda - \rho_1 N| \leq (N-1) \delta_\rho,
  \end{equation}
  and $N-1$ eigenvalues between 0 and $(N-1)\delta_\rho$.
\end{proposition}
\begin{proof}
  Using the notation in the proof of Proposition~\ref{prop:Apd}, 
  we write $A$  as
  \begin{equation}
    \begin{split}
    A & = \begin{bmatrix}
      \rho_1 & \rho_1 & \dots & \rho_1 \\
      \rho_1 & \rho_1 & \dots & \rho_1 \\
      \vdots & & \ddots & \\
      \rho_1 & \rho_1 & \dots & \rho_1
    \end{bmatrix}
    +
    \begin{bmatrix}
      0 & 0 & \dots & 0 \\
      0 & \rho_{1,2} & \dots & \rho_{1,2} \\
      \vdots & & \ddots & \\
      0 & \rho_{1,2} & \dots & \rho_{1,N}
    \end{bmatrix} \\
    & \equiv \rho_1 1_N + \delta R,
    \end{split}
  \end{equation}
  where $1_N$ is the $N \times N$ with 1 in each entry.  Equivalently, we have
  \begin{equation}
    \rho_1 1_N = A - \delta R.
  \end{equation}
  The matrix $\rho_1 1_N$ has rank 1, and its eigenvalues are $N
  \rho_1$ with multiplicity 1 and $0$ with multiplicty $N-1$.
  Moreover, it is symmetric and so has an orthogonal matrix $V$ of
  eigenvectors.  

  From the Bauer-Fike theorem~\cite{bauer1960norms}, for each
  eigenvalue $\mu$ of $\rho_1 1_N$, there exists an eigenvalue $\mu$
  of $A$ such that
  \begin{equation}
    |\lambda - \mu| \leq \kappa_2(V) \| \delta R \|_2,
  \end{equation}
  where $\| \cdot \|_2$ denotes the matrix 2-norm and $\kappa_2$ the
  2-norm condition number.  Since $V$ is orthogonal, it has unit
  condition number, and we must just bound the 2-norm of $\delta R$.

  $\delta R$ is symmetric and semi-definite, so its 2-norm coincides
  with its spectral radius.  An upper bound on the spectral radius
  of $\delta R$ of
  \begin{equation}
    \sum_{i=2}^N \rho_{1,i} 
    \leq (N-1) (\rho_N - \rho_1) = (N-1) \delta_\rho
  \end{equation}
  is readily obtained from the Gerschgorin Circle Theorem.  From Propositions~\ref{prop:Asym} and~\ref{prop:Apd}, $A$ must have real and positive eigenvalues.  Combining this with the Gerschgorin estimate puts $N-1$ eigenvalues in the interval $(0, (N-1) \delta_\rho)$.
\end{proof}

Consequently, if the difference between the maximal and minimal densities is small compared to the minimal one, we have a clear separation into a single fast mode and a conglomerate of very slow modes.

\begin{proposition}
  \label{prop:orthogonality}
  Let $B \in \mathbb{R}^{n \times n}$.  If there exists some $\bfx \in \mathbb{R}^n$ that is not orthogonal to any eigenvector of $B$, then the eigenvalues of $B$ are distinct.
\end{proposition}
\begin{proof}
  Suppose instead that $B \bfv_1 = \lambda \bfv_1$ and $B \bfv_2 = \lambda \bfv_2$ for some linearly independent $\bfv_1$ and $\bfv_2$.  Then (note that $\lambda$ and the eigenvectors could be complex-valued so we use $\cdot^*$ instead of $\cdot^T$ to denote inner products).  We have that
  \[
  B \left( \left( \bfv_2^* \bfx \right) \bfv_1 -
  \left( \bfv_1^* \bfx \right) \bfv_2 \right)
  = \lambda \left(  \left( \bfv_2^* \bfx \right) \bfv_1 -
  \left( \bfv_1^* \bfx \right) \bfv_2 \right),
  \]
  so $\left( \bfv_2^* \bfx \right) \bfv_1 -
  \left( \bfv_1^* \bfx \right) \bfv_2$ is an eigenvector of $B$, 
  but it is orthogonal to $\bfx$, for:
  \[
  \left( \left( \bfv_2^* \bfx \right) \bfv_1 -
  \left( \bfv_1^* \bfx \right) \bfv_2 \right)^* \bfx
  = \left( \bfv_2^* \bfx \right) \left(\bfv_1^* \bfx \right)
  - \left( \bfv_1^* \bfx \right) \left( \bfv_2^* \bfx \right) = 0.
  \]
\end{proof}


\begin{proposition}
  \label{prop:symbasis}
  Let $B \in \mathbb{R}^{n \times n}$ be a symmetric matrix and $\bfx \in \mathbb{R}^n$ a vector that is not orthogonal to any eigenvector of $B$.  Then the set
  \[
  \left\{ \bfx , B \bfx , \cdots B^{n-1} \bfx \right\}
  \]
  is a basis for $\mathbb{R}^n$
\end{proposition}
\begin{proof}
  The set contains $n$ vectors -- it is sufficient to show it is linearly independent.  
  $B$ is symmetric, let $\{ \bfv_i \}_{i=1}^n \subset \mathbb{R}^n$ be an orthonormal basis of eigenvectors.  Suppose that we have
  \[
  \sum_{i=1}^n c_i B^{i-1} \bfx = 0
  \]
  for some real numbers $c_1, \dots, c_n$.  Then, for each $0 \leq i \leq n-1$, we can write $B^i \bfx$ in terms of the eigenbasis by
  \[
  B^i \bfx = \sum_{j=1}^n \left(\left( B^i \bfx\right)^T \bfv_j \right) \bfv_j
  =  \sum_{j=1}^n \bfx^T (B^i \bfv_j) \bfv_j
  = \sum_{j=1}^n \left(\lambda_j^i \bfx^T \bfv_j\right) \bfv_j,
  \]
  and so
  \[
  0 = \sum_{i=1}^n c_i B^{i-1} \bfx =
  \sum_{i=1}^n c_i  \sum_{j=1}^n \left(\lambda_j^{i-1} \bfx^T \bfv_j\right) \bfv_j
  = \sum_{j=1}^n \left( \bfx^T \bfv_j \right) \bfv_j \sum_{i=1}^n c_i \lambda_j^{i-1}.
  \]
  Since none of the $\bfv_j$ nor the $\bfx^T \bfv_j$ are zero, we must have
  that
  \[ \sum_{i=1}^n c_i \lambda_j^{i-1} = 0
  \]
  for each $1 \leq j \leq n$.  However, the previous propostion guarantees that the $\lambda_j$ are unique, and it is well-known that the Vandermonde matrix
  $V_{ij} = \lambda_i^{j-1}$ is nonsingular for distinct points.  Consequently, the only set of $c_i$ that make the sum vanish for each $j$ are all zeros.
\end{proof}

This last result gives a condition under which the Lanczos algorithm~\cite{} for symmetric and positive-definite matrix $B$ starting from some initial vector $\bfx$ succeeds in producing an orthonormal basis for $\mathbb{R}^n$ (at least, in exact arithmetic).  In particular, it shows that there exists an orthogonal matrix $V$ with first column $\bfx$ such that $T = V^T B V$ is tridiagonal.  By similarity, $T$ must also be positive-definite. 

\section{Reformulating the system}
Now, we want to make a change of variables such that the layers are coupled only through a tridiagonal rather than dense matrix.  This will enable the technique we develop for deriving energy estimates, and also presents a possible more attractive discrete system.


The matrix $M$ is diagonal and positive-definite at each point $\bfx \in \Omega$, so it has a unique positive square root $M^{\frac{1}{2}}$.  We let
\begin{equation}
  \tilde{\MM{\psi}} = M^{\frac{1}{2}} \MM{\phi},
\end{equation}
and then premultiply~\eqref{eq:primitive} by $M^{-\frac{1}{2}}$ to find
\begin{equation}
  \label{eq:primitivepsitilde}
  \ppp{\tilde{\MM{\psi}}}{t}
  + f M \pp{\tilde{\MM{\psi}}}{t}^\perp
   + P_n \pp{\tilde{\MM{\psi}}}{t} - M^{-\frac{1}{2}} \nabla \left( A \nabla \cdot M^{-\frac{1}{2}} \tilde{\MM{\psi}} \right) = \tilde{\MM{F}}
\end{equation}

We can write this equation in variational form by taking its inner product with $\MM{v} \in \mathbf{V}$, integrating over $\Omega$ and then by parts:
\begin{equation}
  \label{eq:weak}
  \left(\ppp{\tilde{\MM{\psi}}}{t}, \MM{v} \right)
  + \left(f M \pp{\tilde{\MM{\psi}}}{t}^\perp, \MM{v} \right)
  + \left(P_n \pp{\tilde{\MM{\psi}}}{t}, \MM{v} \right)
  + \left(A \left( \nabla \cdot M^{-\frac{1}{2}} \tilde{\MM{\psi}} ,
  \nabla \cdot M^{-\frac{1}{2}} \MM{\psi} \right) \right) =
  \left( \tilde{\MM{F}}, \MM{v} \right).
\end{equation}

Noting that only the $n,n$ entry of $M$ varies spatially, We apply the product rule to find
\begin{equation} \label{eq:prodrule}
\nabla \cdot M^{-1/2}\MM{v} 
= M^{-1/2}\nabla \cdot \MM{v} + \tilde \mu\cdot M^{-1/2}P_n\MM{v},
\end{equation}
where $\tilde \mu = \mu_n^{1/2}\nabla (\mu_n^{-1/2}) = -\frac{1}{2}\nabla (\ln \mu_n)$.  We apply this twice in~\eqref{eq:weak} and put $\tilde{A} = g M^{-\frac{1}{2}} A M^{-\frac{1}{2}}$ to find
\begin{equation}
  \label{eq:newweak}
  \begin{split}
  \left(\ppp{\tilde{\MM{\psi}}}{t}, \MM{v} \right)
  + \left(f \pp{\tilde{\MM{\psi}}}{t}^\perp, \MM{v} \right)
  + \left(P_n \pp{\tilde{\MM{\psi}}}{t}, \MM{v} \right)  
  + \left(\tilde{A} \nabla \cdot \tilde{\MM{\psi}} , \nabla \cdot \MM{v} \right)  & \\
  + \left( \tilde{\mu} \tilde{A} \nabla \cdot \tilde{\MM{\psi}},  P_n \MM{v} \right)
  + \left( \tilde{\mu} \tilde{A} P_n \tilde{\MM{\psi}} ,
  \nabla \cdot \MM{v} \right)
  + \left( \tilde{\mu}^2 \tilde{A} P_n \tilde{\MM{\psi}} , P_n \MM{v} \right)
  & = \left( \tilde{\MM{F}}, \MM{v} \right).
  \end{split}
\end{equation}
Note that $\tilde{A}$, unlike $A$, varies spatially, and we right $\tilde{A} = \tilde{A}(\bfx)$ as needed.  It is SPD at each point and in fact has a positive uniform lower bound on its eigenvalues.

We can use this fact and the linear algebra results developed in Section~\ref{sec:A} to establish that the Lanczos algorithm, starting with $\bfe_n$ produces an orthogonal basis tridiagonalizing $\tilde{A}$.  This is the key step in finishing our reformulation of the PDE system.

\begin{lemma} \label{lem:Abasis}
   For any $\bfx \in \Omega$ and $\tilde{A} = \tilde{A}(\bfx) \in \mathbb{R}^{n \times n}$, the set $ \left\{ e_n, \tilde Ae_n, \tilde A^2 e_n, \dots, \tilde A^{n-1} e_n \right\}$
forms a basis for $\mathbb{R}^n$. 
\end{lemma}
\begin{proof}
  By Propositions \ref{prop:orthogonality} and \ref{prop:symbasis}, we need only show that $\bfe_n^T \bfv \neq 0$ for all eigenvectors $\bfv$ of $\tilde A$. By way of contradiction, suppose for some nonzero $\bfv$, we have $\tilde A \bfv = \lambda \bfv$ and $\bfe_n^T \bfv = 0$. Then $\bfv_n = 0$ and we have
  $(\tilde A\bfv)_n = \lambda \bfv_n = 0$ so that
\[
0 = (\tilde A\bfv)_n = \sum_{i=1}^n \tilde A_{ni} \bfv_i = \sum_{i=1}^{n-1} \tilde A_{ni} \bfv_i = \sum_{i=1}^{n-1} \frac{\sqrt{\mu_{n-1}}}{\sqrt{\mu_{n}}}\tilde A_{(n-1)i} \bfv_i = \frac{\sqrt{\mu_{n-1}}}{\sqrt{\mu_n}}(\tilde A\bfv)_{n-1} = \lambda \bfv_{n-1}.
\]
Since $\tilde A$ is an SPD matrix, $\lambda \neq 0$ so that $\bfv_{n-1} = 0$. Iterate this process to find $\bfv = 0$, a contradiction.
\end{proof}

We now apply the Lanczos algorithm.  We define $\bfv_1 = \bfe_n$ and
\[
\tilde{\bfv}_2 = \tilde{A} \bfv_1 - \left(\left(\tilde{A}\bfv_1\right)^T\bfv_1\right) \bfv_1
\]
and $\bfv_2 = \tilde{\bfv}_2 / \| \tilde{\bfv}_2 \|$.  Proceeding with the standard three-term recurrence for each $3 \leq i \leq n$, we put
\begin{equation}
\begin{split}
  \tilde{\bfv}_i & = \tilde{A} \bfv_{i-1} - \left( \left( \tilde{A} \bfv_{i-1}\right)^T \bfv_{i-1} \right) \bfv_{i-1}
  - \left( \left( \tilde{A} \bfv_{i-1}\right)^T \bfv_{i-2} \right) \bfv_{i-2},
    \\
 \bfv_i &= \frac{\tilde{\bfv}_i}{\| \tilde{\bfv}_i \|}.
\end{split}
\end{equation}
Since the set $\{ \bfe_n , \tilde{A} \bfe_n, \dots, \tilde{A}^{n-1} \bfe_n \}$ is linearly independent, this process is guaranteed to complete with the set $\{ \bfv_i \}_{i=1}^n$ forming an orthonormal basis for $\mathbb{R}^n$.

Somewhat suprisingly, we have:
\begin{proposition}
  \label{prop:spaceindep}
  The orthonormal basis $\{ \bfv_i \}_{i=1}^n$ defined above is independent of the $\bfx \in \Omega$ at which $\tilde{A}$ is evaluated.
\end{proposition}
\begin{proof}
  We note that $\bfv_1 = \bfe_n$ is independent of $\bfx$.  Also, note that $\bfv_2 \perp \bfe_n$ and for $i \leq n-1$, we have
  \[
  \tilde{\bfv}_2^T \bfe_i =
  g \left( M^{-\frac{1}{2}} A M^{-\frac{1}{2}} \bfe_n \right)^T \bfe_i
  = \left( g \mu_n^{-\frac{1}{2}} \mu_i^{-\frac{1}{2}} \bfe_n \right)^T \left( A \bfe_i \right) = g  \mu_n^{-\frac{1}{2}} \mu_i^{-\frac{1}{2}} A_{ni}.
  \]
  So, $\tilde{\bfv}_2$ is proportionaol to the vector
  \[
  \begin{bmatrix}
    \mu_1^{-\frac{1}{2}} A_{n1} &  \mu_2^{-\frac{1}{2}} A_{n2} & \dots
     \mu_{n-1}^{-\frac{1}{2}} A_{n(n-1)} & 0
  \end{bmatrix}^T,
  \]
  so that after normalizing, $\bfv_2$ is also independent of $\bfx$.

  We now proceed inductively, supposing that for some $3 \leq j \leq n-1$, $\{\bfv_i\}_{i=1}^{j-1}$ are independent of $\bfx$.  Extend this set with to an orthonormal basis for $\mathbb{R}^n$ by adding orthonormal $\{\bfu\}_{i=j}^n$, also independent of $\bfx$.  We know that $\tilde{\bfv}_j \perp \bfv_i$ for $1 \leq i \leq j-1$.  For $i \geq j$, $\bfu_i$ is orthogonal to all of $\{\bfv_k\}_{k=1}^{j-1}$, so
  \[
  \tilde{\bfv}_{j}^T \bfu_i
  = g \left( M^{-\frac{1}{2}} A M^{-\frac{1}{2}} \bfe_n \right)^T \bfv_i
  = g \left( A M^{-\frac{1}{2}} \bfv_{j-1} \right)^T\left(M^{-\frac{1}{2}} \bfv_i \right).
  \]
  Now, both $\bfv_{j-1}$ and $\bfv_i$ are orthogonal to $\bfe_n$.  The only entry of $M$ depending on $\bfx$ is $\mu_n$, so neither $M^{-\frac{1}{2}} \bfv_{j-1}$ nor
  $ M^{-\frac{1}{2}} \bfv_j$ can depend on $\bfx$.  Since $A$ is  constant, this implies that $\tilde{\bfv}_j$ and hence $\bfv_j$ are are also independent of $\bfx$.  
\end{proof}

Now, we define
\begin{equation}
  V = \begin{bmatrix}
    \bfv_1 & \bfv_2 & \dots & \bfv_n
  \end{bmatrix}
\end{equation}
to be the orthogonal matrix containing the orthonormal vectors produced above, and the three-term recurrence gives rise to the factorization
\begin{equation}
  \tilde{A}(\bfx) = V^T T(\bfx) V,
\end{equation}
where $V$ is orthogonal and $T(\bfx)$ is, for each $\bfx$, symmetric and tridiagonal.  By similarity, $T(\bfx)$ is also positive-definite with the same eigenvalues as $\tilde{A}(\bfx)$.  
We refer to the entries of $T$ (implicitly dependent on $\bfx$) as
\begin{equation}
  \begin{split}
    \alpha_i & = T_{i, i} = (\tilde{A}\bfv_i)^T \bfv_i, \\
    \beta_i & = T_{i, i+1} = (\tilde{A}\bfv_i)^T \bfv_{i+1},
  \end{split}
\end{equation}
and we can determine a considerable amount of information about these particular entries.

\begin{proposition}
  \label{lem:entriesofT}
  Let      
  \begin{equation}
    b(M,A) = \sqrt{\mu_1^{-1}A_{n1}^2 + \cdots + \mu_{n-1}^{-1}A_{n(n-1)}^2}.
  \end{equation}

  For $2 \leq i \leq n$, the values $\alpha_i, \beta_i$  are positive constants.
  Moreover, $\alpha_1,\beta_1$ are positive, continuously differentiable functions satisfying the estimate
        \begin{equation}
        \label{eq:beta1estimate}
        \frac{A_{nn}}{\mu_{n,1}} \leq \alpha_1 \leq \frac{A_{nn}}{\mu_{n,0}},
        \ \ 
        \frac{b(M,A)}{\sqrt{\mu_{n,1}}} \leq \beta_1 \leq \frac{b(M,A)}{\sqrt{\mu_{n,0}}},
        \end{equation}
\end{proposition}
\begin{proof}
  Note that $\alpha_i = (AM^{-1/2}\bfv_i)^T (M^{-1/2}\bfv_i)$.
  By Proposition \ref{prop:spaceindep}, the vectors $\bfv_i$ do not depend on $\bfx$. Moreover for $i \geq 2$ they are orthogonal to $\bfv_1 = \bfe_n$.
  Therefore, for $i \geq 2$, $M^{-1/2}\bfv_i$ is also independent of $\bfx$, hence so is $\alpha_i$.
  On the other hand, $\alpha_1 = \mu_n^{-1}(A\bfv_1)^T \bfv_1 = \mu_n^{-1}A_{nn}$, which is a positive, continuously differentiable function.
        
  By inspecting the Lanczos algorithm, we have $\beta_i = (\tilde A \bfv_i)^T \bfv_{i+1} = \tilde \bfv_{i+1}^T \bfv_{i+1} = \|\tilde \bfv_{i+1}\|$, and from the proof of Proposition \ref{prop:spaceindep} this is independent of $\bfx$ if $i \geq 2$.   For $i = 1$ we have a precise formula given by 
  \begin{equation}
    \beta_1 = \|\tilde \bfv_2 \| = \mu_n^{-1/2}\sqrt{\mu_1^{-1}A_{n1}^2 + \cdots + \mu_{n-1}^{-1}A_{n(n-1)}^2}.
  \end{equation}
\end{proof}

Now, we let $\MM{\psi} = V^T \tilde{\MM{\psi}}$ in~\eqref{eq:newweak} and also replace $\MM{v} \mapsto V \MM{v}$ (this mapping is an isomorphism on $\mathbf{V}$) to arrive at a tridiagonalized form of our system:
\begin{equation}
  \label{eq:finalweak}
  \begin{split}
  \left(\ppp{\MM{\psi}}{t}, \MM{v} \right)
  + \left(f \pp{\MM{\psi}}{t}^\perp, \MM{v} \right)
  + \left(P_n \pp{\MM{\psi}}{t}, \MM{v} \right)  
  + \left(T \nabla \cdot \MM{\psi} , \nabla \cdot \MM{v} \right)  & \\
  + \left( \tilde{\mu} T \nabla \cdot \MM{\psi},  P_n \MM{v} \right)
  + \left( \tilde{\mu} T P_n \MM{\psi},
  \nabla \cdot \MM{v} \right)
  + \left( \tilde{\mu}^2 T P_n \MM{\psi} , P_n \MM{v} \right)
  & = \left( \tilde{\MM{F}}, \MM{v} \right).
  \end{split}
\end{equation}


\section{Energy estimates}
\label{sec:energy}
For now, we begin with an energy estimate for the case of zero forcing.  We can select $\MM{v} = \pp{\MM{\psi}}{t}$ in~\eqref{eq:finalweak} to find:
\begin{equation}
  \frac{1}{2} \frac{d}{dt} \| \pp{\MM{\psi}}{t} \|^2
  + \frac{
\end{equation}



We next by writing out~\eqref{eq:finalweak} in terms of coordinate functions.  Denoting components of $\MM{\psi}$ as $\psi_1, \psi_2, \dots, \psi_n$, we write
\begin{equation}
  \begin{split}
  \left(\ppp{\psi_i}{t} + f \pp{\psi_i}{t}^\perp + \tfrac{d}{\mu_n} \delta_{i,1} \pp{\psi_i}{t}, v \right)
  + \left( \beta_{i-1} \nabla \cdot \psi_{i-1} + \alpha_i \nabla \cdot \psi_i + \beta_i \nabla \cdot \psi_{i+1} , \nabla \cdot v \right) & \\
  + \delta_{i,1} \left( \alpha_1 \nabla \cdot \psi_1 + \beta \nabla \cdot \psi_2 + \alpha_1 \tilde{\mu} \psi_1 , \tilde{\mu} v \right)
  +\left( \left(\delta_{i,1} \alpha_1 + \delta_{i,2} \beta_1\right) \tilde{\mu} \psi_1, \nabla \cdot v \right) & = 0
  \end{split}
\end{equation}


\section{Finite element error estimates}
\label{sec:error}



\section{Conclusion}

%\bibliographystyle{plain}
\bibliography{bib}

\end{document}
