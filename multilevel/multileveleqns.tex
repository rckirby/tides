\documentclass{article}
\usepackage{amsmath,amssymb,stmaryrd}
\def\MM#1{\boldsymbol{#1}}
\newcommand{\pp}[2]{\frac{\partial #1}{\partial #2}} 
\newcommand{\dede}[2]{\frac{\delta #1}{\delta #2}}
\newcommand{\dd}[2]{\frac{\diff#1}{\diff#2}}
\newcommand{\dt}[1]{\diff\!#1}
\def\MM#1{\boldsymbol{#1}}
\DeclareMathOperator{\diff}{d}
\DeclareMathOperator{\DIV}{DIV}
\DeclareMathOperator{\D}{D}
\bibliographystyle{plain}
\newcommand{\vecx}[1]{\MM{#1}}
\newtheorem{definition}{Definition}
\newcommand{\code}[1]{{\ttfamily #1}} 
\begin{document}
\title{Multilevel linear SWE}
\author{Colin Cotter}
\maketitle

We consider a series of layers of fluid, with the top layer having
thickness $D_1$, the next layer $D_2$, and so on until $D_N$ for the
bottom layer (oceanographers count downwards) with bottom boundary
$z=b(x,y)$. The density in each layer $i$ is $\rho_i$, and the
horizontal velocity $\MM{u}_i$.  We have constants $g$ (acceleration
due to gravity) and $f$ (Coriolis parameter).

We assume that the pressure is hydrostatic, meaning that the pressure
in each layer $i$ satisfies
\begin{equation}
  \pp{p}{z}|_i = -\rho_i g,
\end{equation}
so $p|_i = -\rho_i gz + c_i$ in each layer.

Using $p=0$ at the top surface, we have
\begin{equation}
  p|_1 = \rho_1 g\left(\sum_{j=1}^N D_i + b - z\right).
\end{equation}
Evaluating this at the bottom of the top layer gives
\begin{equation}
p|_1(z=\sum_{j=2}^N+b)=\rho_1 g D_1.
\end{equation}
Then,
\begin{equation}
  p|_2 = \rho_2\left(\sum_{j=2}^N D_i + b - z + \rho_1 g D_1\right).
\end{equation}
By induction or pattern-matching, we have
\begin{align}
  p|_i &= \rho_i\left(\sum_{j=i}^N D_i + b - z + \sum_{i=1}^{j-1}\frac{\rho_j}
  {\rho_i} g D_j\right),\\
  &= \rho_i\left(\sum_{j=1}^N D_i + b - z + \sum_{i=1}^{j-1}\frac{\rho_j-\rho_i}{\rho_i}
  g D_j\right),
\end{align}

Under the assumption that the motion is columnar (i.e. horizontal
velocity is independent of $z$), the horizontal component of
the momentum equation becomes (after dividing by $\rho_i)$
\begin{equation}
  \pp{\MM{u}_i}{t} +
  \MM{u_i}\cdot\nabla \MM{u}_i + f\MM{u}_i^\perp
  = -g\nabla \left(\sum_{j=1}^N D_j + \sum_{j=1}^{i-1} \frac{\rho_j-\rho_i}{\rho_i}
  D_j + b\right) - \frac{C}{D_i}(|\MM{u}_N|)\delta_{iN}\MM{u}_N + \frac{F(t)}{D_i},
\end{equation}
where we added a parameterisation for bottom drag, with $C(s)$ the
damping function, $\delta_{iN}$ is equal to 1 if $i=N$ and zero
otherwise, and $F(t)$ is the barotropic tidal forcing. The rationale
for the scaling with $D_i$ is that the drag is due to turbulence
assumed to occur in the bottom layer only. This turbulent flow exerts
an effective damping force proportional to the velocity in the bottom
layer, so the depth averaged momentum source is $\int_b^{b+D_N} F(u)
\diff z$, and then we divide by $D_i$ to get the equation for
$\MM{u}$.

The steady solutions are $\MM{u}_i=0$ $i=1,\ldots, N$, and
$D_i=\bar{D}_i=$constant for $i<N$, and $D_N - b = \bar{D}_N-b=$constant.  To
linearise, we write $D_i = \bar{D}_i+D'_i$, where $\bar{D}_i$ is the
thickness of the layer when the system is at rest, and where we assume
that $D'_i$ and $\MM{u}_i$ are small, and retain only the linear terms
in the advection terms as well as replacing $D_i$ by $\bar{D}_i$ in the
forcing terms.

\begin{align}
  \pp{\MM{u}_i}{t} + f\MM{u}_i^\perp
  = -g\nabla \left(\sum_{j=1}^N D_j' + \sum_{j=1}^{i-1} \frac{\rho_j-\rho_i}{\rho_i}
  D_j'\right) - \frac{C}{\bar{D}_i}(|\MM{u}_N|)\delta_{iN}\MM{u}_N + \frac{F(t)}{\bar{D}_i}, \\
  \pp{D_i'}{t} + \nabla\cdot \left(\bar{D}_i\MM{u}_i\right) = 0.
\end{align}
Sometimes a simplified model is used under the rigid lid assumption,
in which we assume that $\sum_{j=1}^ND_j+b$
is constant. This is relevant because typically $(\rho_j-\rho_i)/\rho_i$ is
small, and so there are very fast ``barotropic'' waves where $\MM{u}_i$
is independent of $i$, and much slower ``baroclinic'' waves where the
free surface is more-or-less flat. It is the baroclinic tides that become
interesting since that is where tidally-generated energy is thought to be
dissipated as turbulence away from the bottom boundary.

\end{document}
