\section{Attracting solution}
\label{se:attracting}
In this section we show that all solutions converge to a single solution.
We do not, however, obtain a rate of convergence.

Let $\phi_{1,h},\phi_{2,h}$ be two solutions of \eqref{eq:secondorder_discrete} with different initial data.
Set $\phi_h = \phi_{1,h}-\phi_{2,h}$.
Then $\phi_h$ satisfies
\begin{equation}
\left( \frac{1}{H}\phi_{h,tt}, v_h \right)
+ \left( \frac{f}{H\epsilon} \phi_{h,t}^\perp , v_h \right) +
\frac{\beta}{\epsilon^2} \left( \nabla\cdot \phi_h , \nabla \cdot v_h
\right) + \left( g(\cdot,\phi_{1,h,t})-g(\cdot,\phi_{2,h,t}) , v_h \right) = 0.
\label{eq:secondorder_discrete_differences}
\end{equation}
for all $v_h \in V_h$ for (almost) all $t \in [0,T]$.
We will once again define
\begin{equation}
E(t) 
= \frac{1}{2} \weightednorm{\phi_{h,t}}{\frac{1}{H}}^2 
+ \frac{\beta}{2\epsilon^2} \norm{\nabla \cdot \phi_h}^2
\label{eq:energy}
\end{equation}
Our goal is to prove that $\lim_{t \to \infty} E(t) = 0$.

Again we have the energy identity
\[
\frac{d}{dt} E(t)
+ \left( g(\cdot,\phi_{1,h,t})-g(\cdot,\phi_{2,h,t}), \phi_{h,t} \right) = 0.
\]
The monotonicity assumptions on $g$ mean that the time derivative of
$E$ must therefore be negative.

We introduce the following additional assumption on the damping:
for all $\delta > 0$, there exists $M_\delta > 0$ (which increases as $\delta$ decreases) such that
\begin{equation}
\label{eq:strictly_monotone}
|v-w|^2,|g(x,v) - g(x,w)|^2  \leq M_\delta(g(x,v) - g(x,w)) \cdot (v-w)  \ \forall v,w \in \mathbb{R}^d \setminus B_\delta(0).
\end{equation}
For example, if $g$ has the form given by \eqref{eq:pgrowth} then it satisfies \eqref{eq:strictly_monotone}.

A corollary of \eqref{eq:strictly_monotone} is the following elementary lemma.
\begin{lemma} \label{lem:elementary}
	In addition to the previous assumptions on $g$, suppose \eqref{eq:strictly_monotone} holds.
	Set $S_\delta := \sup\{|g(x,w)| : x \in \Omega, |w| \leq \delta\}$.
	Then if $|v| \geq \delta$ and $|w| < \delta$ (or vice versa), we have
	\begin{equation} \label{eq:elementary1}
	|v-w|^2 \leq 2M_\delta(g(x,v) - g(x,w)) \cdot (v-w) + 4\delta^2
	\end{equation}
	and
	\begin{equation} \label{eq:elementary2}
	|g(x,v) - g(x,w)|^2 \leq 2M_\delta(g(x,v) - g(x,w)) \cdot (v-w) + 8S_\delta^2
	\end{equation}
	for all $x \in \Omega$.
	\end{lemma}

\begin{proof}
	Let $|v| \geq \delta$ and $|w| < \delta$.
	Set $v_\lambda = \lambda w + (1-\lambda)v$ and fix $\lambda \in (0,1]$ such that $|v_\lambda| = \delta$.
	Using the identities $v-v_\lambda = \lambda(v-w)$ and $v_\lambda - w = (1-\lambda)(v-w)$, the fact that $g(x,\cdot)$ is monotone and the assumption \eqref{eq:strictly_monotone}, we get
	\begin{align*}
	(g(x,v)-g(x,w))\cdot (v-w) %&= (g(x,v)-g(x,v_\lambda))\cdot (v-w) + (g(x,v_\lambda)-g(x,w))\cdot (v-w)\\
	&\geq \frac{1}{\lambda}(g(x,v)-g(x,v_\lambda))\cdot (v-v_\lambda)\\
	&\geq \frac{1}{\lambda M_\delta}|v-v_\lambda|^2 \\
	&= \frac{1}{M_\delta}|v-w|^2 - \frac{1}{M_\delta}|v-w||v_\lambda-w|.
	\end{align*}
	Using fact that $|v_\lambda-w| \leq 2\delta$, an application of Young's inequality leads to \eqref{eq:elementary1}.
	The proof of \eqref{eq:elementary2} is similar.
	First we get, as above,
	\begin{equation*}
	(g(x,v)-g(x,w))\cdot (v-w) \geq \frac{1}{\lambda M_\delta}|g(x,v)-g(x,v_\lambda)|^2
	\end{equation*}
	which implies
	\begin{align*}
	|g(x,v)-g(x,w)|^2 &\leq 2|g(x,v)-g(x,v_\lambda)|^2 + 2|g(x,v_\lambda)-g(x,w)|^2
	\\
	&\leq 2M_\delta(g(x,v)-g(x,w))\cdot (v-w) + 2(2S_\delta)^2
	\end{align*}
	using the fact that $|v_\lambda|,|w| \leq \delta$.
	\end{proof}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Prove E(t) \to 0.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Now we prove that $E(t) \to 0.$
Proceeding in the exact same way as in Section \ref{se:energy}, we obtain
\begin{multline}
 \label{eq:differenceestimate1}
E(t) + \frac{d}{dt}\left(\phi_{h,t},\phi_h^D \right)_{\frac{1}{H}}  \leq 
\frac{3}{2}\left(1 + \frac{(f^*C_P)^2}{\beta H_*}\right)\frac{1}{H_*} \|\phi_{h,t}\|^2
\\
+ \frac{3(\epsilon C_P )^2 H^*}{2\beta H_*} \|g(\cdot,\phi_{1,h,t})-g(\cdot,\phi_{1,h,t})\|^2,
\end{multline}
which is analogous to \eqref{eq:energybound2}.
This time define
$$
\Omega_0^\delta(t) = \{x \in \Omega : |\phi_{1,h,t}(x,t)|,|\phi_{2,h,t}(x,t)| < \delta\},
$$
$$
\Omega_1^\delta(t) = \{x \in \Omega : |\phi_{1,h,t}(x,t)|,|\phi_{2,h,t}(x,t)| \geq \delta\},
$$
and
$$
\Omega_2^\delta(t) = \{x \in \Omega : |\phi_{1,h,t}(x,t)| \geq \delta,|\phi_{2,h,t}(x,t)| < \delta \ \text{or} \ |\phi_{1,h,t}(x,t)| < \delta,|\phi_{2,h,t}(x,t)| \geq \delta\}.
$$
Then we find, using \eqref{eq:elementary1} and \eqref{eq:elementary2}, that
\begin{multline*}
\|\phi_{h,t}\|^2
= \int_{\Omega_0^\delta(t)} |\phi_{h,t}|^2 \ dx + \int_{\Omega_1^\delta(t)} |\phi_{h,t}|^2 \ dx  + \int_{\Omega_2^\delta(t)} |\phi_{h,t}|^2 \ dx 
\\
\leq \delta^2 |\Omega_0^\delta(t)|  + M_\delta\int_{\Omega_1^\delta(t)} (g(\cdot,\phi_{1,h,t})-g(\cdot,\phi_{1,h,t})) \cdot \phi_{h,t} \ dx
\\
+ 2 M_\delta\int_{\Omega_2^\delta(t)} (g(\cdot,\phi_{1,h,t})-g(\cdot,\phi_{1,h,t})) \cdot \phi_{h,t} \ dx
+ 4\delta^2 |\Omega_2^\delta(t)|
\\
\leq 5\delta^2|\Omega| + 2 M_\delta\int_{\Omega} (g(\cdot,\phi_{1,h,t})-g(\cdot,\phi_{1,h,t})) \cdot \phi_{h,t} \ dx,
\end{multline*}
and, similarly,
\begin{multline*}
\|g(\cdot,\phi_{1,h,t})-g(\cdot,\phi_{1,h,t})\|^2
= \int_{\Omega_0^\delta(t)} |g(\cdot,\phi_{1,h,t})-g(\cdot,\phi_{1,h,t})|^2 \ dx 
\\
+ \int_{\Omega_1^\delta(t)} |g(\cdot,\phi_{1,h,t})-g(\cdot,\phi_{1,h,t})|^2 \ dx  + \int_{\Omega_2^\delta(t)} |g(\cdot,\phi_{1,h,t})-g(\cdot,\phi_{1,h,t})|^2 \ dx 
\\
\leq 4S_\delta^2|\Omega_0^\delta(t)|  + M_\delta\int_{\Omega_1^\delta(t)} (g(\cdot,\phi_{1,h,t})-g(\cdot,\phi_{1,h,t})) \cdot \phi_{h,t} \ dx
\\
+ 2 M_\delta\int_{\Omega_2^\delta(t)} (g(\cdot,\phi_{1,h,t})-g(\cdot,\phi_{1,h,t})) \cdot \phi_{h,t} \ dx
+ 8S_\delta^2 |\Omega_2^\delta(t)|
\\
\leq 12S_\delta^2|\Omega| + 2 M_\delta\int_{\Omega} (g(\cdot,\phi_{1,h,t})-g(\cdot,\phi_{1,h,t})) \cdot \phi_{h,t} \ dx,
\end{multline*}
where we recall that $S_\delta := \sup\{|g(x,w)| : x \in \Omega, |w| \leq \delta\}$.
Now Equation \ref{eq:differenceestimate1} becomes
\begin{multline}
\label{eq:differenceestimate2}
E(t) + \frac{d}{dt}\left(\phi_{h,t},\phi_h^D \right)_{\frac{1}{H}}  \leq 
%\frac{3}{2}\left(1 + \frac{(f^*C_P)^2}{\beta H_*}\right)\frac{1}{H_*} \left(5\delta^2|\Omega| + 2 M_\delta\int_{\Omega} (g(\cdot,\phi_{1,h,t})-g(\cdot,\phi_{1,h,t})) \cdot \phi_{h,t} \ dx\right)
%\\
%+ \frac{3(\epsilon C_P )^2 H^*}{2\beta H_*}\left(12S_\delta^2|\Omega| + 2 M_\delta\int_{\Omega} (g(\cdot,\phi_{1,h,t})-g(\cdot,\phi_{1,h,t})) \cdot \phi_{h,t} \ dx\right)
%\\
%= \frac{3}{2}\left(1 + \frac{(f^*C_P)^2}{\beta H_*}\right)\frac{1}{H_*} 5\delta^2|\Omega| + \frac{3(\epsilon C_P )^2 H^*}{2\beta H_*}12S_\delta^2|\Omega|
%\\
%+ \frac{3}{2}\left(1 + \frac{(f^*C_P)^2}{\beta H_*}\right)\frac{1}{H_*}2 M_\delta\int_{\Omega} (g(\cdot,\phi_{1,h,t})-g(\cdot,\phi_{1,h,t})) \cdot \phi_{h,t} \ dx
%\\
%+ \frac{3(\epsilon C_P )^2 H^*}{2\beta H_*}2 M_\delta\int_{\Omega} (g(\cdot,\phi_{1,h,t})-g(\cdot,\phi_{1,h,t})) \cdot \phi_{h,t} \ dx \\ =
%
 (5A_1 \delta^2 + 12A_2S_\delta^2)|\Omega|
\\
+ 2(A_1+A_2) M_\delta\int_{\Omega} (g(\cdot,\phi_{1,h,t})-g(\cdot,\phi_{1,h,t})) \cdot \phi_{h,t} \ dx
\end{multline}
where
$$
A_1 = \frac{3}{2}\left(1 + \frac{(f^*C_P)^2}{\beta H_*}\right)\frac{1}{H_*},
\ \ \
A_2 = \frac{3(\epsilon C_P )^2 H^*}{2\beta H_*}.
$$
Set $C_{\delta,1} = (5A_1 \delta^2 + 12A_2S_\delta^2)|\Omega|$ and $C_{\delta,2} = 2(A_1+A_2) M_\delta$.
By the energy identity, \eqref{eq:differenceestimate2} becomes
\begin{equation}
\label{eq:differenceestimate3}
\frac{d}{dt}\left[\left(\phi_{h,t},\phi_h^D \right)_{\frac{1}{H}} + C_{\delta,2}E(t)\right] \leq -E(t) + C_{\delta,1}.
\end{equation}
Again we observe that
\begin{equation}
\left|\left(\phi_{h,t},\phi_h^D \right)_{\frac{1}{H}}\right| \leq \frac{C_P}{\sqrt{H_*}}\|\phi_{h,t}\|_{\frac{1}{H}}\|\nabla \cdot \phi\| \leq \frac{C_P \epsilon}{\sqrt{\beta H_*}}E(t),
\end{equation}
we also set
\begin{equation}
\tilde C_{\delta,2} = \max\left\{C_{\delta,2},\frac{2C_P \epsilon}{\sqrt{\beta H_*}} \right\}.
\end{equation}
Then defining
\begin{equation}
A(t) = E(t) + \frac{1}{\tilde A_2}\left(\phi_{h,t},\phi_h^D \right)_{\frac{1}{H}},
\end{equation}
we have the estimate
\begin{equation}
\frac{1}{2}E(t) \leq A(t) \leq \frac{3}{2}E(t).
\end{equation}
We use this in \eqref{eq:differenceestimate3} first to get
\begin{equation}
\label{eq:differenceestimate4}
\frac{d}{dt}A(t) \leq -\frac{1}{2\tilde C_{\delta,2}}A(t) + \frac{C_{\delta,1}}{\tilde C_{\delta,2}}
 \ 
 \Rightarrow
 \ 
 A(t) \leq A(0)e^{-t/(2\tilde C_{\delta,2})} + 2C_{\delta,1},
\end{equation}
which then implies
\begin{equation}
E(t) \leq 3E(0)e^{-t/(2\tilde C_{\delta,2})} + 4C_{\delta,1}
\ 
\Rightarrow
\
\limsup_{t\to \infty} E(t) \leq 4C_{\delta,1} = 4(5A_1 \delta^2 + 12A_2S_\delta^2)|\Omega|.
\end{equation}
Letting $\delta \to 0$ and recalling that $g$ is continuous, we see that $\lim_{t \to \infty}E(t) = 0$.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Step 2: Controlling the nonlinearity.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%










