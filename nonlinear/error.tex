\section{Error estimates}
\label{se:error}
Now, we consider \emph{a priori} error estimates of two types.  For
one, we give an estimate which is optimal with respect to the power of
$h$ but has a possible exponential increase in time.  This is obtained
by using monotonicity of the damping term but no further techniques.
Second, we can also adapt the continuous dependence results of the
previous section to give an estimate that is \emph{uniform} in time,
but has a suboptimal rate with respect to $h$.  

As is typical, we obtain our results by 
comparing the the finite element solution to the $\Pi$ projection of the
true solution, whence the error estimates follow by the triangle
inequality. 

We define 
\begin{equation}
\begin{split}
\chi &\equiv \Pi u - u, \\
\theta_h & \equiv \Pi u - u_h,  \\
\rho & \equiv \pi \eta - \eta, \\
\zeta_h & \equiv \pi \eta - \eta_h
\end{split}
\end{equation}

The projection $\Pi \phi$ satisfies the second-order equation similar to~\eqref{eq:secondorder_discrete}
\begin{multline}
  \left( \Pi \phi_{tt} , v_h \right)
  + \left( \frac{f}{H\epsilon} \Pi \phi_t^\perp , v_h \right)
  + \frac{\beta}{\epsilon^2} \left( \nabla \cdot \Pi \phi , \nabla
  \cdot v_h \right)
  + \left( g\left( \Pi \phi_t \right) , v_h \right)
\\  =
  \left( F , v_h \right)
  + \left( \chi_{tt} , v_h \right)
  + \left( \frac{f}{H\epsilon} \chi_t^\perp , v_h \right)
  + \left( g\left( \Pi \phi_t \right)
  - g\left( \phi_t \right), v_h \right).
\end{multline}

Subtracting the discrete equation~(\ref{eq:secondorder_discrete}) from this gives
\begin{multline}
  \label{eq:secondorderror}
  \left(\theta_{h,tt} , v_h \right)
  + \left( \frac{f}{H\epsilon} \theta_{h,t}^\perp , v_h \right)
  + \frac{\beta}{\epsilon^2} \left( \nabla \cdot \theta_h , \nabla
  \cdot v_h \right)
  + \left( g\left( \Pi \phi_t \right)
         - g\left( \phi_{h,t} \right) , v_h \right)
\\  =
  \left( \chi_{tt} , v_h \right)
  + \left( \frac{f}{H\epsilon} \chi_t^\perp , v_h \right)
  + \left( g\left( \Pi \phi_t \right)
  - g\left(\left( \phi_t \right), v_h \right) \right),
\end{multline}
and putting $v_h = \theta_{h,t}$ and defining
\begin{equation}
  \label{eq:Edef}
E(t) := \frac{1}{2} \| \theta_{h,t} \|^2_{\frac{1}{H}}
+ \frac{\beta}{2\epsilon^2} \| \nabla \cdot \theta_h \|^2
\end{equation}
gives
\begin{equation}
  \frac{d}{dt} E(t)
  + \left( g\left( \Pi \phi_t \right)
         - g\left( \phi_{h,t} \right) , \theta_{h,t} \right)
=
  \left(\tilde F , \theta_{h,t} \right),
\end{equation}
where
\begin{equation}
\tilde F := \chi_{tt} + \frac{f}{H\epsilon} \chi_t^\perp
+ g\left( \Pi \phi_t \right)
  - g\left( \phi_t \right)
\end{equation}
and the Lipschitz condition for $g$ and approximation estimates for
$\chi$ give
\begin{equation}
\| \tilde F \|
\leq \left( C_\Pi |\phi_{tt}|_k
+ \left( \frac{C_\Pi f^*}{H_* \epsilon} + M \right) | \phi_{t} |_k \right) h^k
:= \kappa(\phi) h^k
\end{equation}

The initial conditions here depend on the choice of initial conditions for the discrete equation.  If they are chosen to be the appropriate $\Pi$ projection of the original initial conditions (i.~e.~the $\Pi$ projection of $\phi$ and the $\frac{1}{H}$-weighted $L^2$ projection of $\phi_{t}$)
then the initial condition for the error equation will vanish.

Simply using monotonicity of $g$ gives
\begin{equation}
  \frac{d}{dt} E(t) \leq \frac{1}{2} \| \tilde{F} \|^2 + \frac{1}{2} E(t),
\end{equation}
and it is easy to show from this that
\begin{equation}
E(t) \leq e^{\frac{t}{2}}E(0) + h^{2k} \int_0^t e^{\frac{t-s}{2}} \kappa(\phi)^2 ds.
\end{equation}
Even supposing that $\kappa(\phi)$ is uniformly bounded in time by
\begin{equation}
  \kappa(\phi) \leq \overline{\kappa}
\end{equation}
and the initial conditions are selected so that $E(0) = 0$, one still has
a bound on $E(t)$ that grows exponentially in time.  Combining this
estimate with the triangle inequality leads to the estimate.

\begin{theorem}
    Suppose that and $E(0) = 0$.  Then for all time we have the error estimate
    \begin{multline}
      \frac{1}{2} \| u(\cdot, t) - u_h(\cdot, t) \|^2
      + \frac{\beta}{2 \epsilon} \| \eta(\cdot, t) - \eta_h(\cdot, t) \|^2
      \\ \leq
      \| \chi(\cdot, t)  \|^2
      + \frac{\beta}{\epsilon} \| \rho(\cdot, t) \|^2 + 2E(t)
      \\ \leq \left[
        C_\Pi^2 |u|^2_k + \frac{\beta}{\epsilon} C_\pi^2 |\eta|^2_k
        + 4 \overline{\kappa}^2 \left( e^{\frac{t}{2} - 1} \right) \right] h^{2k}.
    \end{multline}
\end{theorem}

Now, we can employ the continuous dependence results developed earlier
to remove the exponential dependence in time at the expense of a
somewhat decreased rate in $h$.  Returning to Theorem~\ref{thm:cd}, we set
that $\delta_1 = 0$ (for appropriately chosen discrete initial conditions) and
$\delta_2 = \overline{\kappa}^2 h^{2k}$ to obtain the estimate
\begin{theorem}
  Suppose that $\phi, \phi_t \in L^\infty(0,t;H^k(\Omega)$ for all time $t$, the conditions of Theorem~\ref{thm:cd} hold.  Provided the error energy given by~\eqref{eq:Edef} satisfies $E(0) = 0$, then 
  \begin{equation}
    E(t) \leq 3 C_3 \overline{\kappa}^{\frac{2\alpha}{2-\alpha}} h^{\frac{2k\alpha}{2-\alpha}}
  \end{equation}
  and hence
  \begin{multline}
      \frac{1}{2} \| u(\cdot, t) - u_h(\cdot, t) \|^2
      + \frac{\beta}{2 \epsilon} \| \eta(\cdot, t) - \eta_h(\cdot, t) \|^2
      \\ =
      \frac{1}{2} \| \phi_t(\cdot, t) - \phi_{h,t}(\cdot, t) \|^2
      + \frac{\beta}{2 \epsilon} \| \nabla \cdot \phi(\cdot, t)
      - \nabla \cdot \phi_h(\cdot, t) \|^2
      \\ \leq
      \| \chi(\cdot, t)  \|^2
      + \frac{\beta}{\epsilon} \| \rho(\cdot, t) \|^2 + 2E(t)
      \\ \leq
      \left[
        C_\Pi^2 |u|^2_k + \frac{\beta}{\epsilon} C_\pi^2 |\eta|^2_k
        \right] h^{2k}
      + 6 C_3\overline{\kappa}^{\frac{2\alpha}{2-\alpha}} h^{\frac{2k\alpha}{2-\alpha}}
  \end{multline}
  \label{thm:longtimeerror}
  \end{theorem}
Note that this estimate is necessarily suboptimal since
$\alpha \in (0,1)$.  
In the case of a superlinear power law, we have $\alpha = \frac{2}{p}$
for $p > 2$.  For the quadratic damping case with $p = 3$, we have
$\frac{\alpha}{2-\alpha} = \frac{1}{2}$ and hence we have an estimate
on the order of $h^{\frac{k}{2}}$, or $\sqrt{h}$ in the case of the
lowest-order method.  For the cubic power law, this becomes
$\sqrt[3]{h}$.  We do not claim that the present estimates are sharp,
but we are unaware of other techniques to give estimates holding
uniformly in time.



