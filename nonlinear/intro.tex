\section{Introduction}

Accurate modeling of tides is important in several scientific
disciplines.  Tides' strong impact on sediment transport and coastal
flooding makes them of interest to geologists.  Global oceanographers 
suggest that breaking internal tides provide a mechanism for vertical
mixing of temperature and salinity that might sustain the global ocean
circulation \cite{GaKu2007,MuWu1998}. A useful tool for predicting tides are the
rotating shallow water equations, which provide a model of the
barotropic (\emph{i.e.}, depth-averaged) dynamics of the ocean.
Away from coastlines, the nonlinear advection in barotropic tides is
very weak compared to drag force, so that one frequently
solves the linear rotating shallow-water equations with a
parameterised drag term to model the effects of 
bottom friction~\cite{La45}. This approach can be
used on a global scale to set the boundary conditions for a more
complex regional scale model, as was done in \cite{Hi_etal2011}, for
example. Various additional dissipative terms have been proposed to
account for other dissipative mechanisms in the barotropic tide, due
to baroclinic tides, for example \cite{JaSt2001}. 

The possibility of unstructured triangular meshes make finite element
methods attractive for modelling the world's oceans, including irregulary coastlines and topography~\cite{We_etal2010}. 
Recent years have seen much discussion about mixed
finite element pairs to use as the horizontal discretization for
atmosphere and ocean models.  In papers such as~\cite{CoLaReLe2010,CoHa2011,Da2010,RoRoPo2007,RoRo2008,Ro2005,Ro2012,RoBe2009},
we see many details regarding the numerical dispersion relations obtained when
discretizing the rotating shallow water equations. Then, in~\cite{CoKi},
we took a different angle, studying the behavior of
discretizations of the forced-dissipative rotating shallow-water
equations used for predicting global barotropic tides.  In particular,
energy techniques were used to show that discrete solutions approach
the correct long-time solution in response to quasi-periodic forcing.
Since the linearized energy only controls the divergent part of the
solution, we chose finite element spaces for which there is 
a natural discrete Helmholtz decomposition and such that
the Coriolis term projects the divergent and divergence-free
components of vector fields correctly onto each other.
Hence, we used compatible, finite element
spaces (\emph{i.e.} those which arise naturally from the finite
element exterior calculus \cite{arnold2006finite}), first proposed
for numerical weather prediction in \cite{CoSh2012} and then extended
to develop finite element methods for the nonlinear rotating
shallow-water equations on the sphere that can conserve energy,
enstrophy and potential
vorticity~\cite{CoTh2014,McCo2014,RoHaCoMc2013}.  In~\cite{CoKi}, the 
discrete Helmholtz decomposition allowed us to show that
mixed finite  element discretizations of the forced-dissipative linear
rotating shallow-water equations have the correct long-time energy
behavior, and the linear nature of the equations also led to natural
optimal \emph{a priori} error estimates.

Finite element methods' ability to use unstructured grids also allows
coupling of global tide structure with local coastal dynamics. A
discontinuous Galerkin approach was developed in
\cite{salehipour2013higher}, whilst continuous finite element
approaches have been used in many studies \cite[for
  example]{FoHeWaBa1993,kawahara1978periodic,Le_etal2002}. The lowest
order Raviart-Thomas element for velocity combined with $P_0$ for
height was proposed for coastal tidal modeling in
\cite{walters2005coastal}; this pair fits into our current framework.

In~\cite{CoKi}, we restricted attention to the linear bottom drag
model as originally proposed in \cite{La45}.  Quadratic
damping laws are more realistic, but the nonlinearity presents
significant difficulties to analysis.  In this paper, we extend our
work in the linear case by adapting techniques from the nonlinear PDE
literature (see especially \cite{cavalcanti2007well,lasiecka1993uniform} and references therein) to the finite element setting.  We consider a family of
damping laws that are nonlinear for small velocity but behave linearly
for large velocity.  We require monotonicity and some other technical
assumptions on the on the nonlinearity, and these include the
quadratic case and other power laws.
As an alternative to modifying the damping term for large velocity,
\emph{a priori} assumptions (or better, estimates) on the size of
solutions would allow us to use an unmodified law.  At any rate, provided
that the velocity in fact remains bounded, one may compute with the
unmodified (i.~.e.~not forced to be linear at infinity) law.  As with the linear case, we believe that the
applicability of our work is not limited to the shallow water case,
but to other nonlinearly damped hyperbolic systems for which the
appropriate function spaces have discrete Helmholtz decompositions,
such as damped electromagnetics or elastodynamics.


% Connect to other mixed acoustic equations.
In addition to mixed finite elements' application to tidal
models in the geophysical literature, this work also builds on 
existing literature for mixed discretization of the acoustic
equations. The first such investigation is due to
Geveci~\cite{geveci1988application}, where exact energy conservation
and optimal error estimates are given for the semidiscrete first-order
form of the model wave equation.  Later
analysis~\cite{cowsar1990priori,jenkins2003priori} considers a second 
order in time wave equation with an auxillary flux at each time step.
In~\cite{kirbykieu}, Kirby and Kieu return to the first-order
formulation, giving additional estimates
beyond~\cite{geveci1988application} and also analyzing the symplectic
Euler method for time discretization.  From the standpoint of this
literature, our model appends additional terms for
the Coriolis force and damping to the simple acoustic model.
We restrict ourselves to semidiscrete analysis in this work, but pay
careful attention the extra terms in our estimates, showing how study
of an equivalent second-order equation in $\hdiv$ proves proper
long-term behavior of the model.

In the rest of the paper, we describe the tidal model and a general
finite element discretization in Section~\ref{se:model}.
Section~\ref{se:energy} gives the three major results of this paper.
In particular, we show that for any initial data and forcing function
with a uniform time bound, the system energy also remains uniformly
bounded.  This is \emph{geotryptic state}.  Then, we give two
continuous dependence results.  The first of these works with solutions
corresponding to identical forcing but different initial data.  In
this case, we show that the energy of the difference tends to zero
over time at a rate that depends on the particular nonlinearity.  As
corollaries of this, we obtain the existence of global attracting
solutions and also effective energy decay rates for the unforced
system.  Our second dependence result allows both the initial data and
forcing to vary, when the energy difference is bounded unformly in time by
the sum of a term that is linear in the initial energy perturbation
and nonlinear in the forcing perturbation.  In Section~\ref{se:error},
we give two kinds of \emph{a priori} error estimates.  The first,
using standard techniques, shows that the error is
optimal with the power of $h$, but the constant degrades exponentially
in time.  The second applies the continuous dependence result of
Section~\ref{se:energy} to give estimates with a generically
suboptimal power of $h$, but that hold \emph{uniformly} for all time.
Finally, we present some numerical experiments in Section~\ref{sec:num}.
As a note, our previous work~\cite{CoKi} in the linear case included
application of the techniques in~\cite{holst2012geometric} when the
domain is actually a more general manifold.  We do not include this
extension here, but the nonlinear 
should not include additional complications.
