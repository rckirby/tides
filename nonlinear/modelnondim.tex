\section{Description of finite element tidal model}
\label{se:model}
We start with the nondimensional linearized rotating shallow water
model with linear forcing and a possibly nonlinear drag term on a two
dimensional surface $\Omega$, given by
\begin{equation}
\begin{split}
u_t + \frac{f}{\epsilon} u^\perp + \frac{\beta}{\epsilon^2} \nabla
\left( \eta - \eta^\prime \right) + g(u) & = 0, \\
\eta_t + \nabla \cdot \left( H u \right) & = 0,
\end{split}
\end{equation}
where $u$ is the nondimensional two dimensional velocity field tangent
to $\Omega$, $u^\perp=(-u_2,u_1)$ is the velocity rotated by $\pi/2$, $\eta$ is the nondimensional free surface elevation above
the height at state of rest, $\nabla\eta'$ is the (spatially varying)
tidal forcing, $\epsilon$ is the Rossby number (which is small for
global tides), $f$ is the spatially-dependent non-dimensional Coriolis
parameter which is equal to the sine of the latitude (or which can be
approximated by a linear or constant profile for local area models),
$\beta$ is the Burger number (which is also small),
$H$ is the (spatially varying) nondimensional fluid
depth at rest, and $\nabla$ and $\nabla\cdot$ are the intrinsic
gradient and divergence operators on the surface $\Omega$,
respectively.

The damping function $g$ is the major focus of this work.  We assume
that $g(u)$ is possibly inhomogeneous in that $g(u) = g(x, u)$,
although for simplicity we suppress the extra argument.  All bounds
given on $g$ will be assumed to hold uniformly in $x$.  Although our
main interest is a power law, we only make structural assumptions on
$g$.  At the very least, we assume
\begin{itemize}
\item Monotonicity.  For all $v$,
  \begin{equation}
    \label{eq:monot}
    g(v) \cdot v > 0.
  \end{equation}
\item Linear growth for large velocity.  There exists an $M > 0$ such that for all $|v| > 1$, we have
  \begin{equation}
    \label{eq:lineargrowth}
    |v| + |g(v)|^2 \leq M g(v) \cdot v.
  \end{equation}
\end{itemize}
These assumptions are sufficient to give long-time stability of
solutions, although the continuous dependence results will require
stronger assumptions (which still hold for $g$ of practical interest).
These will be made precise later in the paper.


We will work with a slightly generalized version of the forcing term,
which will be necessary for our later error analysis.  Instead of
assuming forcing of the form 
$\frac{\beta}{\epsilon^2} \nabla \eta^\prime$, 
we assume some $F \in L^2$, giving our model as
\begin{equation}
\begin{split}
u_t + \frac{f}{\epsilon} u^\perp + \frac{\beta}{\epsilon^2} \nabla
 \eta  + g(u) & = F, \\
\eta_t + \nabla \cdot \left( H u \right) & = 0.
\end{split}
\end{equation}


It also becomes useful to work in terms of the linearized momentum
$\widetilde{u} = H u$ rather than velocity. After making this substitution and dropping the
tildes, we obtain
\begin{equation}
\begin{split}
\frac{1}{H}u_t + \frac{f}{H\epsilon} u^\perp + \frac{\beta}{\epsilon^2} \nabla
\eta  + g(u) & = F, \\
\eta_t + \nabla \cdot  u & = 0.
\end{split}
\label{eq:thepde}
\end{equation}
A natural weak formulation of this equations is to seek $u \in \hdiv$
and $\eta \in L^2$ so that
\begin{equation}
\begin{split}
\left( \frac{1}{H}u_t , v \right) 
+ \frac{1}{\epsilon} \left( \frac{f}{H} u^\perp , v \right) 
- \frac{\beta}{\epsilon^2} \left( \eta ,
\nabla \cdot v \right) + \left( g(u) , v \right) & = 
\left( F , v \right)
, \quad \forall v \in \hdiv, \\
\left( \eta_t , w \right) + \left( \nabla \cdot u  , w \right)& = 0, \quad
\forall w \in L^2.
\end{split}
\label{eq:mixed}
\end{equation}

We now develop mixed discretizations with $V_h \subset \hdiv$ and
$W_h \subset L^2$.  Conditions on the spaces are the commuting projection
and divergence mapping $V_h$ onto $W_h$. We define $u_h:[0,T]\rightarrow V_h$
and $\eta_h :[0,T] \rightarrow  W_h$ as solutions of the discrete variational
problem
\begin{equation}
\begin{split}
\left( \frac{1}{H}u_{h,t} , v_h \right) 
+ \frac{1}{\epsilon} \left( \frac{f}{H} u_h^\perp , v_h \right) 
- \frac{\beta}{\epsilon^2} \left( \eta_h ,
\nabla \cdot v_h \right) + \left( g(u_h) , v_h \right) & =
\left( F , v_h \right)
, \\
\left( \eta_{h,t} , w_h \right) + \left( \nabla \cdot u_h  , w_h \right)& = 0.
\end{split}
\label{eq:discrete_mixed}
\end{equation}

Our analysis will proceed by working with an equivalent second-order
form.  While in the linear case~\cite{CoKi}, one readily 
obtains a second-order $\hdiv$ wave equation by differentiating the 
the first equation and using that $\nabla \cdot V_h = W_h$, this leads
to the somewhat awkward situation of differentiating through the nonlinearity.
A different approach allows us to avoid this unpleasantness.
Let $\phi \in \hdiv \times [0,T]$ satisfy the equation

\begin{equation}
  \frac{1}{H}\phi_{tt} + \frac{f}{H\epsilon} \phi_t^\perp -
  \frac{\beta}{\epsilon^2} \nabla \left( \nabla \cdot \phi \right) + g(\phi_t) = F
  \label{eq:secondorderpde}
\end{equation}
Then, we identify $u$ with $\phi_t$ and $\eta$ with $-\nabla \cdot
\phi$, and we see that solutions of~\eqref{eq:thepde}
and~\eqref{eq:secondorderpde} are in fact equivalent.  As an added
advantage over the technique in~\cite{CoKi}, the natural energy
functionals for the first- and second-order forms of the equation turn
out to coincide using this approach.

To analyze the semidiscrete setting, we need to adapt this observation
to the weak forms.  One may take the natural $\hdiv$ finite element
discretization of~\eqref{eq:secondorderpde}, seeking
$\phi_{h}: [0,T] \rightarrow V_h$ such that
\begin{equation}
  \left( \frac{1}{H}\phi_{h,tt}, v_h \right)
  + \left( \frac{f}{H\epsilon} \phi_{h,t}^\perp , v_h \right) +
  \frac{\beta}{\epsilon^2} \left( \nabla\cdot \phi_h , \nabla \cdot v_h
  \right) + \left( g(\phi_t) , v_h \right) = \left( F , v_h \right).
  \label{eq:secondorder_discrete}
\end{equation}
for all $v_h \in V_h$ for (almost) all $t \in [0,T]$.  Equivalently,
one could define $\phi_h$ to satisfy~\eqref{eq:secondorder_discrete}
and then note that standard properties of mixed finite element spaces
allow one to identify $u_h$ with $\phi_{h,t}$ and $\eta_h$ with
$\nabla \cdot \phi_h$ in~\eqref{eq:discrete_mixed}.

For the velocity space $V_h$, we will work with standard $\hdiv$ mixed
finite element spaces on triangular elements, such as Raviart-Thomas
(RT), Brezzi-Douglas-Marini (BDM), and Brezzi-Douglas-Fortin-Marini
(BDFM)~\cite{RavTho77a,brezzi1985two,brezzi1991mixed}.  We label the
lowest-order Raviart-Thomas space with index $k=1$, following the
ordering used in the finite element exterior
calculus~\cite{arnold2006finite}.  Similarly, the lowest-order
Brezzi-Douglas-Fortin-Marini and Brezzi-Douglas-Marini spaces
correspond to $k=1$ as well.  We will always take $W_h$ to consist of
piecewise polynomials of degree $k-1$, not constrained to be
continuous between cells.  We require the strong boundary condition
$u\cdot n = 0$ on all external boundaries.

Throughout, we shall let $\norm{\cdot}$ denote the standard $L^2$ norm.  We
will frequently work with weighted $L^2$ norms as well.  For a
positive-valued weight function $\kappa$, we define the weighted 
$L^2$ norm 
\begin{equation}
\weightednorm{v}{\kappa}^2
= \int_\Omega \kappa \left| v \right|^2 dx.
\end{equation}
If there exist positive constants $\kappa_*$ and $\kappa^*$ such that
$0 < \kappa_* \leq \kappa \leq \kappa^* < \infty$ 
almost everywhere, then the weighted norm is equivalent to
the standard $L^2$ norm by
\begin{equation}
\sqrt{\kappa_*} \norm{v} 
\leq \weightednorm{v}{\kappa} 
\leq \sqrt{\kappa^*} \norm{v}.
\end{equation}

A Cauchy-Schwarz inequality 
\begin{equation}
(\kappa v_1 , v_2) \leq 
\weightednorm{v_1}{\kappa}
\weightednorm{v_2}{\kappa}
\end{equation}
holds for the weighted inner product, and we can also incorporate
weights into Cauchy-Schwarz for the standard $L^2$ inner product by
\begin{equation}
(v_1,v_2) = 
(\sqrt{\kappa} v_1 , \frac{1}{\sqrt{\kappa}} v_2)
\leq \weightednorm{v_1}{\kappa} \weightednorm{v_2}{\frac{1}{\kappa}}.
\end{equation}

We refer the reader to references such as~\cite{brezzi1991mixed} for full
details about the particular definitions and properties of these
spaces, but here recall several facts essential for our analysis.  For
all velocity spaces $V_h$ we consider, the divergence maps $V_h$ onto $W_h$.
Also, the spaces of interest all have a projection, $\Pi : \hdiv
\rightarrow V_h$ that commutes with the $L^2$ projection $\pi$ into
$W_h$: 
\begin{equation}
\left( \nabla \cdot \Pi u , w_h \right)
= \left( \pi \nabla \cdot u , w_h \right)
\end{equation}
for all $w_h \in W_h$ and any $u \in \hdiv$.  
We have the error estimate
\begin{equation}
\norm{u - \Pi u} \leq C_{\Pi} h^{k+\sigma} \weightedseminorm{u}{k}
\label{eq:PiL2}
\end{equation}
when $u \in (H^{k+1})^2$.  Here, $\sigma = 1$ for the BDM spaces but
$\sigma =0$ for the RT or BDFM spaces. The projection also has an
error estimate for the divergence
\begin{equation}
\norm{ \nabla \cdot \left( u - \Pi u \right) } \leq C_\Pi h^{k} 
\weightedseminorm{\nabla \cdot u}{k}
\label{eq:PiDiv}
\end{equation}
for all the spaces of interest, whilst the pressure projection has the
error estimate 
\begin{equation}
\norm{ \eta - \pi \eta } \leq C_{\pi} h^k \weightedseminorm{\eta}{k}.
\label{eq:piL2}
\end{equation}
Here, $C_\Pi$ and $C_\pi$ are positive constants independent of $u$,
$\eta$, and $h$, although not necessarily of the shapes of the
elements in the mesh.

We will utilize a Helmholtz decomposition of $\hdiv$ under a weighted inner product.  For a very general treatment of such decompositions,
we refer the reader to~\cite{arnold2010finite}.  For each $u \in V$, there exist
unique vectors $u^D$ and $u^S$ such that $u = u^D + u^S$, $\nabla
\cdot u^S = 0$, and also $\left( \frac{1}{H} u^D , u^S \right) = 0$.
That is, $\hdiv$ is decomposed into the direct sum of the space of solenoidal
vectors, which we denote by
\begin{equation}
\mathcal{N} \left( \nabla \cdot \right)
= \left\{ u \in V : \nabla \cdot u = 0 \right\},
\end{equation}
 and its orthogonal complement under the $\left( \frac{1}{H}
  \cdot , \cdot \right)$ inner product, which we denote by
\begin{equation}
\mathcal{N} \left( \nabla \cdot \right)^\perp
= \left\{ u \in V : \left( \frac{1}{H} u , v \right) = 0, \ \forall v
  \in \mathcal{N} \left( \nabla \cdot \right) \right\}.
\end{equation}
Functions in $\mathcal{N} \left( \nabla \cdot \right)^\perp$
satisfy a generalized Poincar\'e-Friedrichs inequality, that there
exists some $C_P$ such that 
\begin{equation}
\weightednorm{u^D}{\frac{1}{H}}
\leq C_P \weightednorm{\nabla \cdot u^D}{\frac{1}{H}},
\label{eq:pf}
\end{equation}
or, via norm equivalence,
\begin{equation}
\weightednorm{u^D}{\frac{1}{H}}
\leq \frac{C_P}{\sqrt{H_*}} \norm{\nabla \cdot u^D}.
\end{equation}
Because our mixed spaces $V_h$ are contained in $H(\mathrm{div})$, the same decompositions can be applied, and the Poincar\'e-Friedrichs inequality holds with a constant no larger than $C_p$.




