\section{Energy estimates}
\label{se:energy}
Now, we demonstrate the stability of this system.  In the linear
damping case, studied in~\cite{}, exponential damping was demonstrated
in the unforced case.  Moreover, subject to very mild growth
assumptions on the forcing term, we showed that the system energy
remained bounded for all time.  In this section, we revisit these
questions for the case of nonlinear damping, adapting techniques from
the PDE literature~\cite{}.

It is easy to show that, absent forcing or damping ($F = g = 0$), that
selecting $v_h = u_h$ and $w_h = \frac{\beta}{\epsilon^2}$
in~\eqref{eq:discrete_mixed} or $v_h = \phi_{h,t}$
in~\eqref{eq:secondorder_discrete} gives that the energy functional
\begin{equation}
E(t) = \frac{1}{2} \weightednorm{ u_h}{\frac{1}{H}}^2 
+ \frac{\beta}{2\epsilon^2} \norm{\eta_h}^2
= \frac{1}{2} \weightednorm{\phi_{h,t}}{\frac{1}{H}}^2 
+ \frac{\beta}{2\epsilon^2} \norm{\nabla \cdot \phi_h}^2
\label{eq:energy}
\end{equation}
is exactly conserved.


With a nonzero damping satisfying the assumptions, it is also easy to
demonstrate that the energy is nonincreasing in time.  By selecting
$v_h = \phi_{h,t}$ in~\eqref{eq:secondorder_discrete} with $F=0$, we
find that
\[
\frac{d}{dt} E(t)
+ \left( g(\cdot,\phi_{h,t}), \phi_{h,t} \right) = 0.
\]
The monotonicity assumptions on $g$ mean that the time derivative of
$E$ must therefore be negative.

In fact, with nondegenerate linear damping, we also showed that the
initial energy decays exponentially to zero in the absence of forcing
by deriving a first-order linear ODE for the energy.  However, in the
nonlinear case, the relative damping rate may decrease concurrently
with the energy.  While this rules out exponential decay, it is
possible to give concrete decay rates.

What follows are two subsections, the first of which deals with decay rates (in the absence of a source, i.e.~$F=0$) and the second with long time stability (in the presence of a source that is globally bounded in time).
We obtain each of these as a corollary of a stronger result, namely that each of these holds for \emph{differences} of solutions.
In particular, we will show that for any source term $F$ there is a unique attracting solution which attracts at a specified rate.
As a corollary, when there is no source term, solutions converge to zero with rate depending only on the size of the initial data.
Likewise, we show that the difference of solutions which correspond to two different source terms (which are globally bounded in time) has a long time energy bound depending only on the sources.
As another corollary, all solutions all globally bounded in the energy norm.

\subsection{Decay rates}

\subsubsection{Assumptions on $g$}

We will make the following assumptions on $g$.
First, it is strictly monotone:
\begin{equation}
\label{eq:monotone-strict}
(g(x,v) - g(x,w)) \cdot (v-w) > 0 \ \forall v \neq w, \forall x \in \Omega.
\end{equation}
Second, it behaves linearly for large vectors $v$. That is, for some $M > 0$ we have
\begin{equation} \label{eq:linearatinfinity}
|v-w|^2, |g(x,v)-g(x,w)|^2 \leq M (g(x,v)-g(x,w)) \cdot (v-w) \ \ \ \forall |v|,|w| \geq 1, \forall x \in \Omega.
\end{equation}
Finally, $g$ is assumed to be continuous in both variables on the closure of the domain $\overline{\Omega} \times \mathbb{R}^2$.
%Note that \eqref{eq:monotone-strict} and continuity imply $g(x,0) = 0$.

As a corollary of these assumptions, we have the following technical lemma.
\begin{lemma} \label{lem:auxiliaryfunction2}
	Let $g = g(x,v)$ be a continuous function on $\overline{\Omega} \times \mathbb{R}^d$, where $\Omega$ is a bounded domain, satisfying  \eqref{eq:monotone-strict} and \eqref{eq:linearatinfinity}.
	Then there exists an increasing, concave function $J:[0,\infty) \to [0,\infty)$ such that $J(0) = 0$ and
	\begin{equation} \label{eq:J2}
	|v-w|^2 + |g(x,v)-g(x,w)|^2 \leq J((v-w) \cdot (g(x,v)-g(x,w))) \ \ \ \forall |w|,|v| \leq 1, \ \forall x \in \overline{\Omega}.
	\end{equation}
\end{lemma}

\begin{proof}
	Let $B_1 = \{v \in \mathbb{R}^d : |v| \leq 1\}$ and let $\partial B_1$ be its boundary.
	For $v \in B_1, e \in \partial B_1$ and $x \in \overline{\Omega}$, set
	$$
	h_{v,e,x}(s) = se \cdot (g(x,v+se)-g(x,v)), \ \ j_{v,e,x}(s) = s^2 + \max\{|g(x,v+te)-g(x,v)|^2 : 0 \leq t \leq s\}.
	$$
	Note that both functions are strictly increasing in $s$; $j_{e,x}$ is the sum of two increasing functions, one of them strictly increasing, while in the case of $h_{v,e,x}(s)$, we use \eqref{eq:monotone-strict} to check:
	\begin{align*}
	s > t \ \Rightarrow h_{v,e,x}(s) &- h_{v,e,x}(t) 
	\\
	&= se \cdot (g(x,v+se)-g(x,v)) - te \cdot (g(x,v+te)-g(x,v))\\
	&> (s-t)e \cdot (g(x,v+te)-g(x,v)) \geq 0.
	\end{align*}
	Moreover by \eqref{eq:linearatinfinity} we have that $h_{v,e,x}(s) \to \infty$ as $s \to \infty$.
	Let $h_{v,e,x}^{-1}:[0,\infty) \to [0,\infty)$ be the inverse function of $h_{v,e,x}(\cdot)$.
	Our goal is to show that 
	$$
	j(t) := \max_{(v,e,x) \in B_1 \times \partial B_1 \times \overline{\Omega}} j_{v,e,x}(h_{e,x}^{-1}(t))
	$$
	exists (that is, it is finite for all $t$).
	To do this, it is sufficient to see that $j_{v,e,x}(s)$ and $h_{v,e,x}^{-1}(t)$ are both continuous in the tripe $(v,e,x)$.
	The continuity of $(v,e,x) \mapsto j_{v,e,x}(s)$ follows in a straightforward manner from the uniform continuity of $g$ on compact sets.
	Likewise, $h_{v,e,x}(s)$ is continuous in $(v,e,x)$.
	To see that $h_{v,e,x}^{-1}(t)$ is continuous in $(v,e,x)$, we assume to the contrary that there exists some sequence $(v_n,e_n,x_n) \in B_1 \times \partial B_1 \times \overline{\Omega}$ such that $(v_n,e_n,x_n) \to (v,e,x)$ while $|h_{v_n,e_n,x_n}^{-1}(t) - h_{v,e,x}^{-1}(t)| \geq \epsilon$.
	Let $s_n = h_{v_n,e_n,x_n}^{-1}(t)$ and $s = h_{e,x}^{-1}(t)$.
	There are two cases:
	\begin{enumerate}
		\item[1:] $s_n \geq s + \epsilon$ (up to a subsequence).
		Since $h_{e_n,x_n}$ and $h_{e,x}$ are strictly increasing, it follows that $h_{e_n,x_n}(s_n) \geq h_{e_n,x_n}(s+\epsilon) \to h_{e,x}(s+\epsilon) > h_{e,x}(s)$.
		But this implies $t > t$, a contradiction.
		
		\item[2:] $s_n \leq s - \epsilon$ (up to a subsequence).
		We have $h_{e_n,x_n}(s_n) \leq h_{e_n,x_n}(s-\epsilon) \to h_{e,x}(s-\epsilon) < h_{e,x}(s)$, so $t < t$, a contradiction.
	\end{enumerate}
	We now see that $h_{v,e,x}^{-1}(t)$ is continuous in $(v,e,x)$ for every $t \geq 0$.
	
	To complete the proof, observe that $j(t)$ is well-defined and finite for all $t \geq 0$, that $j(0) = 0$, and $j$ is increasing.
	Set
	\begin{multline*}
	t_1 := \max\{(v-w) \cdot (g(x,v)-g(x,w)) : v,w \in B_1, x \in \overline{\Omega}\} 
	\\
	= \max\{h_{v,e,x}(s) : v,v+se \in B_1, x \in \overline{\Omega}\}.
	\end{multline*}
	Finally, let $J$ be the concave envelope of $j$ restricted to $[0,t_1]$ (and constant on $[t_1,\infty)$).
	Then $J$ satisfies all the desired properties.
\end{proof}

The function $J$ derived in Lemma \ref{lem:auxiliaryfunction2} determines the decay rates via an ordinary differential equation \eqref{eq:ode-final}.
Loosely speaking, it determines how much the damping is able to ``coerce" the energy.
We note that \eqref{eq:J2} only applies to vectors in the unit ball.
On the other hand, for vectors outside the unit ball, we can use the structure assumed in \eqref{eq:linearatinfinity}.
For the case when one vector is inside the unit ball while the other is outside, we will appeal to this elementary lemma:
A corollary of \eqref{eq:monotone-strict} is the following:

\begin{lemma} \label{lem:elementary}
	Given the above assumptions on $g$, then if $|v| \geq 1$ and $|w| < 1$ (or vice versa), we have
	\begin{multline} \label{eq:elementary}
	|v-w|^2,|g(x,v) - g(x,w)|^2
	\\ \leq 2M((g(x,v)-g(x,w))\cdot (v-w)) + 2J((g(x,v)-g(x,w))\cdot (v-w))
	\end{multline}
	for all $x \in \Omega$.
	\end{lemma}

\begin{proof}
	Let $|v| \geq 1$ and $|w| < 1$.
	Set $v_\lambda = \lambda w + (1-\lambda)v$ and fix $\lambda \in (0,1]$ such that $|v_\lambda| = 1$.
	Using the identities $v-v_\lambda = \lambda(v-w)$ and $v_\lambda - w = (1-\lambda)(v-w)$, the fact that $g(x,\cdot)$ is monotone and satisfies \eqref{eq:linearatinfinity}, and Lemma \ref{lem:auxiliaryfunction2}, we get
	\begin{align*}
	|v-w|^2 &\leq 2|v-v_\lambda|^2 + 2|v_\lambda-w|^2\\
	&\leq 2M(g(x,v)-g(x,v_\lambda))\cdot (v-v_\lambda) + 2J((g(x,v_\lambda)-g(x,w))\cdot (v_\lambda-w))
	\\
	&\leq 2M(g(x,v)-g(x,w))\cdot (v-w) + 2J((g(x,v)-g(x,w))\cdot (v-w))
	\end{align*}
	The second part of \eqref{eq:elementary} is similar, and we omit the details.
	\end{proof}

\subsubsection{Derivation of decay rates}

Let $\phi_{1,h},\phi_{2,h}$ be two solutions of \eqref{eq:secondorder_discrete} with different initial data.
Set $\phi_h = \phi_{1,h}-\phi_{2,h}$.
Then $\phi_h$ satisfies
\begin{multline}
\left( \frac{1}{H}\phi_{h,tt}, v_h \right)
+ \left( \frac{f}{H\epsilon} \phi_{h,t}^\perp , v_h \right) +
\frac{\beta}{\epsilon^2} \left( \nabla\cdot \phi_h , \nabla \cdot v_h
\right) 
\\
+ \left( g(\cdot,\phi_{1,h,t})-g(\cdot,\phi_{2,h,t}) , v_h \right) = 0.
\label{eq:secondorder_discrete_differences}
\end{multline}
for all $v_h \in V_h$ for (almost) all $t \in [0,T]$.
We will once again define
\begin{equation}
E(t) 
= \frac{1}{2} \weightednorm{\phi_{h,t}}{\frac{1}{H}}^2 
+ \frac{\beta}{2\epsilon^2} \norm{\nabla \cdot \phi_h}^2
\label{eq:energy-differences}
\end{equation}
Again we have the energy identity
\[
\frac{d}{dt} E(t)
+ \left( g(\cdot,\phi_{1,h,t})-g(\cdot,\phi_{2,h,t}), \phi_{h,t} \right) = 0.
\]
Our goal is to prove that $\lim_{t \to \infty} E(t) = 0$ with a specified decay rate.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Step 1: Bounds on the divergence part
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\textit{Step 1.}
Take $v_h = \phi_h^D$ in \eqref{eq:secondorder_discrete_differences} and integrate in time.  Integration by parts gives
\begin{multline} \label{eq:energyestimate1}
\left. \left(\phi_{h,t},\phi_h^D \right)_{\frac{1}{H}} \right|_0^T 
- \int_0^T \|\phi_{h,t}^D\|_{\frac{1}{H}}^2 dt + \frac{1}{\epsilon} \int_0^T \left(f\phi_{h,t}^\perp,\phi_h^D \right)_{\frac{1}{H}}dt 
\\
+ \frac{\beta}{\epsilon^2} \int_0^T \|\nabla \cdot \phi_h\|^2 dt 
+ \int_0^T \left(g(\cdot,\phi_{1,h,t})-g(\cdot,\phi_{2,h,t}),\phi_h^D\right) dt = 0.
\end{multline}
Here and in the following we use that $\nabla \cdot \phi_h =\nabla \cdot \phi_h^D$.
Using the Cauchy-Schwartz inequality and \eqref{eq:pf}, Equation \eqref{eq:energyestimate1} becomes
\begin{multline} \label{eq:energyestimate2}
\frac{\beta}{\epsilon^2} \int_0^T \|\nabla \cdot \phi_h\|^2 dt 
\leq
\frac{C_P}{\sqrt{H_*}}\|\phi_{h,t}(T)\|_{\frac{1}{H}}\|\nabla \cdot \phi_h(T)\|
+ \frac{\epsilon^2 C_P}{\beta \sqrt{H_*}}\|\phi_{h,t}(0)\|_{\frac{1}{H}}\|\nabla \cdot \phi_h(0)\| \\
+ \int_0^T \|\phi_{h,t}^D\|_{\frac{1}{H}}^2 dt 
 + \frac{f^* C_P}{\epsilon\sqrt{H_*}} \int_0^T \|f\phi_{h,t}^\perp\|_{\frac{1}{H}}\|\nabla \cdot \phi_h\|dt \\
+ \frac{C_P\sqrt{H^*}}{\sqrt{H_*}}\int_0^T \|g(\cdot,\phi_{1,h,t})-g(\cdot,\phi_{2,h,t})\|\|\nabla \cdot \phi_h\| dt.
\end{multline}
We handle the terms at time $T$ and $0$ by the weighted inequality
$ab \leq \frac{a^2}{2\delta} + \frac{b^2\delta}{2}$ with $\delta= \frac{\epsilon}{\sqrt{\beta}}$.  Then, we pull out $f^*$ from $\| f \phi_{h,t}^\perp \|$ and use that $\| \phi_{h,t}^D \| \leq \| \phi_{h,t} \|$ and that $\cdot^\perp$ is an isometry to obtain
\begin{multline}
  \label{eq:energyestimate2.5}
  \frac{\beta}{\epsilon^2} \int_0^T \|\nabla \cdot \phi_h\|^2 dt
  \leq \frac{C_P\sqrt{\beta}}{\epsilon \sqrt{H_*}} \left[ E(T) + E(0) \right]
  + \int_0^T \|\phi_{h,t}\|_{\frac{1}{H}}^2 dt  \\
 + \frac{f^* C_P}{\epsilon\sqrt{H_*}} \int_0^T \|\phi_{h,t}\|_{\frac{1}{H}}\|\nabla \cdot \phi_h\|dt
+ \frac{C_P\sqrt{H^*}}{\sqrt{H_*}}\int_0^T \|g(\cdot,\phi_{1,h,t})-g(\cdot,\phi_{2,h,t})\|\|\nabla \cdot \phi_h\| dt.
\end{multline}
Next, we handle the terms under the integrals with the same weighted inequality.  In the first case we use $\delta = \frac{\beta\sqrt{H_*}}{2\epsilon f^* C_P}$ and in the second we use $\delta = \frac{\beta\sqrt{H_*}}{2\epsilon^2 C_P \sqrt{H^*}}$.  Then, collecting terms and using that $E(T) \leq E(0)$, we have
\begin{multline} \label{eq:energyestimate3}
  \frac{\beta}{\epsilon^2} \int_0^T \| \nabla \cdot \phi_h \|^2 dt
  \leq \frac{2C_P\sqrt{\beta}}{\epsilon \sqrt{H_*}} E(0) \\
  + \left(1+\frac{f^* C_P^2}{\beta H_*} \right)
  \int_0^T \weightednorm{\phi_{h,t}}{\frac{1}{H}}^2 dt
  + \frac{C_P^2 H^* \epsilon^2}{\beta H_*}
  \int_0^T \norm{g(\cdot, \phi_{h,t})}^2 dt
\end{multline}

So then, it follows that
\begin{multline}\label{eq:energyestimate4}
\int_0^T E(t)dt = \int_0^T \left(\frac{1}{2} \weightednorm{\phi_{h,t}}{\frac{1}{H}}^2 
+ \frac{\beta}{2\epsilon^2} \norm{\nabla \cdot \phi_h}^2  \right)dt \\
  \leq \frac{2C_P\sqrt{\beta}}{\epsilon \sqrt{H_*}} E(0) \\
  + \left(\frac{3}{2}+\frac{f^* C_P^2}{\beta H_*} \right)
  \int_0^T \weightednorm{\phi_{h,t}}{\frac{1}{H}}^2 dt
  + \frac{C_P^2 H^* \epsilon^2}{\beta H_*}
  \int_0^T \norm{g(\cdot, \phi_{h,t})}^2 dt
\end{multline}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Step 2: Getting observability inequality.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\textit{Step 2.}
Set $\Sigma := \Omega \times (0,T)$.
Rewrite \eqref{eq:energyestimate4} as
\begin{multline}
\label{eq:energyestimate5}
\int_0^T E(t)dt \leq \frac{2C_P\sqrt{\beta}}{\epsilon \sqrt{H_*}} E(0) \\
  + \left(\frac{3}{2}+\frac{f^* C_P^2}{\beta H_*} \right)\frac{1}{H_*}
  \int_\Sigma |\phi_{h,t}|^2 \ dxdt
  + \frac{C_P^2 H^* \epsilon^2}{\beta H_*}
  \int_\Sigma |g(x, \phi_{h,t})|^2 \ dx dt.
\end{multline}
Define
$$
\Sigma_0 = \{(x,t) \in \Sigma : |\phi_{1,h,t}(x,t)|,|\phi_{2,h,t}(x,t)| \leq 1\}, \ \Sigma_1 = \Sigma \setminus \Sigma_0.
$$
We can break down $\Sigma_1$ further into
$$
\Sigma_{1,1} = \{(x,t) \in \Sigma : |\phi_{1,h,t}(x,t)|,|\phi_{2,h,t}(x,t)| \geq 1\}, \ \Sigma_{1,0} = \Sigma_1 \setminus \Sigma_{1,1}.
$$
Then we find, using Assumption \eqref{eq:linearatinfinity} and Lemmas \ref{lem:auxiliaryfunction2} and \ref{lem:elementary}, that
\begin{multline} \label{eq:differencesJ1}
\int_\Sigma |\phi_{h,t}|^2 \ dxdt
= \left(\int_{\Sigma_0} + \int_{\Sigma_{1,0}} + \int_{\Sigma_{1,1}}\right) |\phi_{h,t}|^2 \ dxdt
\\
\leq \int_{\Sigma_0} J(\phi_{h,t} \cdot (g(\cdot,\phi_{1,h,t})-g(\cdot,\phi_{2,h,t}))) \ dxdt
\\
+  \int_{\Sigma_{1,0}} \left\{2J(\phi_{h,t} \cdot (g(\cdot,\phi_{1,h,t})-g(\cdot,\phi_{2,h,t}))) + 2M\phi_{h,t} \cdot (g(\cdot,\phi_{1,h,t})-g(\cdot,\phi_{2,h,t}))\right\} \ dxdt
\\
+ \int_{\Sigma_{1,1}} M\phi_{h,t} \cdot (g(\cdot,\phi_{1,h,t})-g(\cdot,\phi_{2,h,t})) \ dxdt
\\
\leq 2M \int_\Sigma \phi_{h,t} \cdot (g(\cdot,\phi_{1,h,t})-g(\cdot,\phi_{2,h,t})) \ dxdt
+ 2\int_\Sigma J\left(\phi_{h,t} \cdot (g(\cdot,\phi_{1,h,t})-g(\cdot,\phi_{2,h,t}))\right) \ dxdt.
\end{multline}
In the same way,
\begin{multline} \label{eq:differencesJ2}
\int_\Sigma |g(\cdot,\phi_{1,h,t})-g(\cdot,\phi_{2,h,t})|^2 \ dxdt
\leq 2M \int_\Sigma \phi_{h,t} \cdot (g(\cdot,\phi_{1,h,t})-g(\cdot,\phi_{2,h,t})) \ dxdt
\\
+ 2\int_\Sigma J\left(\phi_{h,t} \cdot (g(\cdot,\phi_{1,h,t})-g(\cdot,\phi_{2,h,t}))\right) \ dxdt.
\end{multline}
Inserting \eqref{eq:differencesJ1} and \eqref{eq:differencesJ2} into \eqref{eq:energyestimate5} we get
\begin{multline}\label{eq:difference-estimate2}
\int_0^T E(t)dt 
\leq \frac{2C_P\sqrt{\beta}}{\epsilon \sqrt{H_*}} E(0) 
+ D_1 \int_\Sigma \phi_{h,t} \cdot (g(\cdot,\phi_{1,h,t})-g(\cdot,\phi_{2,h,t})) \ dxdt
\\
+ D_2\int_\Sigma J\left(\phi_{h,t} \cdot (g(\cdot,\phi_{1,h,t})-g(\cdot,\phi_{2,h,t}))\right) \ dxdt
\end{multline}
where
\begin{equation}
D_1 = 2M\left(\frac{3}{2}+\frac{f^* C_P^2}{\beta H_*} \right)\frac{1}{H_*}
 + 2M\frac{C_P^2 H^* \epsilon^2}{\beta H_*}
,
\ \
D_2 = 2\left(\frac{3}{2}+\frac{f^* C_P^2}{\beta H_*} \right)\frac{1}{H_*} + 2\frac{C_P^2 H^* \epsilon^2}{\beta H_*}.
\end{equation}
Recall Jensen's inequality: since $J$ is concave and nonnegative,
\begin{multline}
\int_\Sigma J\left(\phi_{h,t} \cdot (g(\cdot,\phi_{1,h,t})-g(\cdot,\phi_{2,h,t}))\right) \ dxdt
\\
\leq |\Sigma|J\left(\frac{1}{|\Sigma|}\int_\Sigma\phi_{h,t} \cdot (g(\cdot,\phi_{1,h,t})-g(\cdot,\phi_{2,h,t})) \ dxdt\right)
\end{multline}
Then since $\int_\Sigma\phi_{h,t} \cdot (g(\cdot,\phi_{1,h,t})-g(\cdot,\phi_{2,h,t})) \ dxdt = E(0) - E(T)$, we can deduce from \eqref{eq:difference-estimate2} that
\begin{equation}\label{eq:difference-estimate3}
\int_0^T E(t)dt 
\leq \frac{2C_P\sqrt{\beta}}{\epsilon \sqrt{H_*}} E(0) 
+ D_1(E(0)-E(T))
+ D_2|\Sigma|J\left(\frac{E(0)-E(T)}{|\Sigma|}\right).
\end{equation}
Now fix $T:= \frac{2C_P\sqrt{\beta}}{\epsilon \sqrt{H_*}} + 1$.
Since $E(t)$ is monotone decreasing, \eqref{eq:difference-estimate3} yields
\begin{equation}\label{eq:difference-estimate4}
E(T)
\leq \tilde D_1( E(0) -E(T))
+ D_2|\Sigma|J\left(\frac{E(0)-E(T)}{|\Sigma|}\right)
\end{equation}
where
\begin{equation}
\tilde D_1 = \frac{2C_P\sqrt{\beta}}{\epsilon \sqrt{H_*}}+ D_1
= \frac{2C_P\sqrt{\beta}}{\epsilon \sqrt{H_*}}+ 2M\left(\frac{3}{2}+\frac{f^* C_P^2}{\beta H_*} \right)\frac{1}{H_*}
+ 2M\frac{C_P^2 H^* \epsilon^2}{\beta H_*}
\end{equation}
We define a strictly increasing function $p(s)$ by defining its inverse:
\begin{equation}
p^{-1}(s) = \tilde D_1 s
+ D_2|\Sigma|J\left(\frac{s}{|\Sigma|}\right).
\end{equation}
It follows that
\begin{equation}
E(T) + p(E(T)) \leq E(0).
\end{equation}
By repeating the same argument on any time interval, we get
\begin{equation}
E((n+1)T) + p(E((n+1)T)) \leq E(nT), \ \ n = 1,2,3,\ldots
\end{equation}
We now appeal to Lemma 3.3 and the argument that follows in (Lasiecka-Tataru 1993) to assert 
\begin{equation}
E(t) \leq S\left(\frac{t}{T}-1\right) \ \forall t \geq T,
\end{equation}
where $S$ solves the ordinary differential equation
\begin{equation} \label{eq:ODE-q}
S'(t) + q(S(t)) = 0, \ S(0) = E(0)
\end{equation}
and $q$ is any increasing function such that $q \leq I - (I+p)^{-1} = (I+p^{-1})^{-1}$.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Step 3: Computing decay rates explicitly.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\textit{Step 3.}
To find an appropriate $q$, we estimate $(I+p^{-1})^{-1}$ or, equivalently, $I+p^{-1}$, which is given by
\begin{equation}
(I+p^{-1})(s) = (1+\tilde D_1) s
+ D_2|\Sigma|J\left(\frac{s}{|\Sigma|}\right).
\end{equation}
Note that since $S(t)$ will always be positive and bounded above by $E(0)$, it suffices to restrict our attention only to the interval $[0,E(0)]$.
Since $J$ is concave and $J(0) = 0$, we can write
\begin{equation}
J\left(\frac{s}{|\Sigma|}\right) \geq \frac{s}{E(0)}J\left(\frac{E(0)}{|\Sigma|}\right)
\ \ \
\forall s \in [0,E(0)].
\end{equation}
Therefore,
\begin{equation} \label{eq:inverse-estimate}
(I+p^{-1})(s) \leq D_J J\left(\frac{s}{|\Sigma|}\right)
\ \ \text{where} \ \
D_J := \left(1+ \tilde D_1\right)\frac{E(0)}{J\left(\frac{E(0)}{|\Sigma|}\right)}
+ D_2|\Sigma|.
\end{equation}
Inverting \eqref{eq:inverse-estimate} we see that an appropriate $q$ is given by
\begin{equation}
q(s) := |\Sigma|J^{-1}\left(\frac{s}{D_J}\right),
\end{equation}
i.e.~$S$ can be taken in the solution of the ODE
\begin{equation} \label{eq:ode-final}
S'(t) + |\Sigma|J^{-1}\left(\frac{S(t)}{D_J}\right) = 0, \ S(0) = E(0).
\end{equation}

\textbf{Examples.}
Let $p > 1$ and set
\begin{equation}
\label{eq:pgrowth}
g(x,v) = g(v) = \left\{
\begin{array}{cc}
|v|^{p-2}v & \text{if} \ |v| \leq 1\\
v & \text{if} \ |v| \geq 1
\end{array}\right.
\end{equation}
When $p > 2$ we refer to this as \textit{superlinear growth} while $p < 2$ is called \textit{sublinear growth}.

\textit{Superlinear growth:} If $p > 2$, we have
$$
||v|^{p-2}v-|w|^{p-2}w| \leq |v-w| \ \forall v,w \in B_1
$$
and so \eqref{eq:J2} can be replaced by
$$
2|v-w|^2 \leq J((v-w) \cdot (|v|^{p-2}v-|w|^{p-2}w)).
$$
Now on the other hand, we have
$$
(v-w) \cdot (|v|^{p-2}v-|w|^{p-2}w) \geq \frac{1}{2^{p-2}}|v-w|^p \ \forall v,w.
$$
This can be proved by vector calculus.
Thus it suffices to choose $J(s) = 2^{3-4/p}s^{2/p}$.
In this case the ODE \eqref{eq:ode-final} becomes
\begin{equation} \label{eq:ode-superlinear}
S'(t) + \frac{2^{2-3p/2}|\Sigma|}{D_J^{p/2}}S(t)^{p/2} = 0, \ S(0) = E(0).
\end{equation}

\textit{Sublinear growth:} If $p < 2$, we can simply invert $g(v)$ for $|v| \leq 1$ to get $v = |g(v)|^{q-2}g(v)$, where $q$ is the conjugate exponent for $p$, namely $q = p/(p-1)$.
Hence it suffices to choose $J(s) = 2^{3-4/q}s^{2/q}$.
The ODE \eqref{eq:ode-final} is the same as \eqref{eq:ode-superlinear} with $p$ replaced by $q$ (note that $q > 2$).




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Long time stability or Zero Energy Growth: F bounded
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Long time stability}

We now consider solutions $\phi_{1,h},\phi_{2,h}$ corresponding to different source terms, $F_1,F_2$, respectively.
We set $F = F_1-F_2$, so that $\phi_h = \phi_{1,h}-\phi_{2,h}$ satisfies
\begin{multline}
\left( \frac{1}{H}\phi_{h,tt}, v_h \right)
+ \left( \frac{f}{H\epsilon} \phi_{h,t}^\perp , v_h \right) +
\frac{\beta}{\epsilon^2} \left( \nabla\cdot \phi_h , \nabla \cdot v_h
\right) 
\\
+ \left( g(\cdot,\phi_{1,h,t})-g(\cdot,\phi_{2,h,t}) , v_h \right) = \left(F, v_h\right).
\label{eq:secondorder_discrete_differences_sources}
\end{multline}

\textit{Step 1.}
As before, take $v_h = \phi_h^D$ in \eqref{eq:secondorder_discrete_differences} and rewrite
the time derivatives
\begin{multline} \label{eq:energybound1}
\frac{d}{dt}\left(\phi_{h,t},\phi_h^D \right)_{\frac{1}{H}} 
- \|\phi_{h,t}^D\|_{\frac{1}{H}}^2
 + \frac{1}{\epsilon}  \left(f\phi_{h,t}^\perp,\phi_h^D \right)_{\frac{1}{H}}
\\
+ \frac{\beta}{\epsilon^2} \|\nabla \cdot \phi_h\|^2 
+ \left(g(\cdot,\phi_{1,h,t})-g(\cdot,\phi_{2,h,t}),\phi_h^D\right) = (F,\phi_h^D).
\end{multline}
Rearranging this and making estimates, we have
\begin{multline}
  \label{eq:energybound2}
  \frac{d}{dt}\left(\phi_{h,t},\phi_h^D \right)_{\frac{1}{H}}
  + \frac{\beta}{\epsilon^2} \|\nabla \cdot \phi_h\|^2
  + \|\phi_{h,t} \|_{\frac{1}{H}}^2
  \leq
  2 \|\phi_{h,t} \|_{\frac{1}{H}}^2
  + \frac{f^* C_P}{\epsilon} \|\phi_{h,t} \|_{\frac{1}{H}}
  \|\nabla \cdot \phi_h\| \\
  + \frac{C_P \sqrt{H^*}}{\sqrt{H_*}}
  \| g(\cdot,\phi_{1,h,t})-g(\cdot,\phi_{2,h,t}) \|
  \|\nabla \cdot \phi_h\|
  + \frac{C_P \sqrt{H^*}}{\sqrt{H_*}} \| F \|
    \|\nabla \cdot \phi_h\|
\end{multline}
%% Using the same type of estimations as before, we arrive at
%% \begin{multline}
%% \label{eq:energybound2}
%% E(t) + \frac{d}{dt}\left(\phi_{h,t},\phi_h^D \right)_{\frac{1}{H}}  \leq 
%% \left(\frac{1}{2} + \frac{3(f^*C_P)^2}{2\beta H_*}\right) \|\phi_{h,t}\|_{\frac{1}{H}}^2
%% \\
%% + \frac{3(\epsilon C_P )^2 H^*}{2\beta H_*} \|g(\cdot,\phi_{1,h,t})-g(\cdot,\phi_{2,h,t})\|^2
%% + \frac{3(\epsilon C_P)^2 H^*}{2\beta H_*} \|F\|^2.
%% \end{multline}

\textit{Step 2.}
As before, we separate into regions where $\phi_{h,t}$ is ``small" and where it is ``large."
Define
$$
\Omega_0(t) = \{x \in \Omega : |\phi_{1,h,t}(x,t)|, |\phi_{1,h,t}(x,t)| < 1\}, \ \Omega_1(t) = \Omega \setminus \Omega_0(t).
$$
We define further
$$
\Omega_{1,1}(t) = \{x \in \Omega : |\phi_{1,h,t}(x,t)|, |\phi_{1,h,t}(x,t)| \geq 1\}, \ 
\Omega_{1,0}(t) = \Omega_{1}(t) \setminus \Omega_{1,1}(t).
$$
Then using \eqref{eq:linearatinfinity}, the continuity of $g$, and an argument similar to (and simpler than) Lemma \ref{lem:elementary}, we have
\begin{multline}
\label{eq:boundong}
\|g(\cdot,\phi_{1,h,t})-g(\cdot,\phi_{2,h,t})\|^2  = \left(\int_{\Omega_0(t)} + \int_{\Omega_{1,0}(t)} + \int_{\Omega_{1,1}(t)} \right) |g(x,\phi_{1,h,t})-g(x,\phi_{2,h,t})|^2 \ dx 
\\
\leq (|\Omega_0(t)|+|\Omega_{1,0}(t)|) g^*
+ 2M\int_{\Omega_{1,0}(t)} (g(x,\phi_{1,h,t})-g(x,\phi_{2,h,t})) \cdot \phi_{h,t} \ dx
\\
+ M\int_{\Omega_{1,1}(t)} (g(x,\phi_{1,h,t})-g(x,\phi_{2,h,t})) \cdot \phi_{h,t} \ dx
\\
\leq 2|\Omega| g^*
+ 2M(g(\cdot,\phi_{1,h,t})-g(\cdot,\phi_{2,h,t}), \phi_{h,t}),
\end{multline}
where
\begin{equation}
  g^* := \max\{|g(x,v)|^2 : x \in \Omega, |v| \leq 1\}
\end{equation}
Similarly, we have
\begin{equation}
\|\phi_{h,t}\|_{\frac{1}{H}}^2  \leq 
\frac{2|\Omega|}{H_*} + \frac{2M}{H_*} (g(\cdot,\phi_{h,t}), \phi_{h,t}).
\end{equation}
Then \eqref{eq:energybound2} becomes
\begin{multline}
\label{eq:energybound3}
\frac{d}{dt}\left(\phi_{h,t},\phi_h^D \right)_{\frac{1}{H}}
+ \frac{\beta}{2\epsilon^2} \| \nabla \cdot \phi_h \|^2
+ \| \phi_{h,t} \|^2_{\frac{1}{H}}
\\
\leq
A_1 + A_2 \left( g(\cdot, \phi_{h,t}), \phi_{h,t} \right) + A_3 \| F \|^2,
\end{multline}
where
\begin{equation*}
  \begin{split}
A_1 := & \frac{3(\epsilon C_P )^2 H^*|\Omega| g^*}{\beta H_*}
+ \left( 4 + \frac{3(f^* C_P)^2}{\beta}\right) \frac{|\Omega|}{H^*} \\
A_2 := & \frac{3(\epsilon C_P )^2 H^* M}{\beta H_*} 
+ \left( 4 + \frac{3(f^* C_P)^2}{\beta}\right) \frac{M}{H^*} \\
A_3 : = & \frac{3(\epsilon C_P )^2 H^* M}{\beta H_*}.
  \end{split}
\end{equation*}
The estimate~\eqref{eq:energybound3} also holds with $A_2$ replaced by $\tilde A_2 \geq A_2$, which we shall specify shortly.

Now, we have that
\(
\left( g(\cdot,\phi_{1,h,t})-g(\cdot,\phi_{2,h,t}) , \phi_{h,t}) \right)
= \left( F, \phi_{h,t} \right) - \frac{d}{dt} E(t),
\)
so that
\begin{multline}
\label{eq:energybound4}
\frac{d}{dt}\left[ \left(\phi_{h,t},\phi_h^D \right)_{\frac{1}{H}} + \tilde A_2 E(t) \right]
+ \frac{\beta}{2\epsilon^2} \| \nabla \cdot \phi_h \|^2
+ \| \phi_{h,t} \|^2_{\frac{1}{H}}
\leq
A_1 + \tilde A_2 \left(F , \phi_{h,t} \right) + A_3 \| F \|^2.
\end{multline}
Then, using Young's inequality with appropriate weighting and dividing through by $\tilde A_2$ gives
\begin{equation}
\frac{d}{dt} A(t) + E(t) \leq \tilde{A_1} + \tilde{A}_3 \| F \|^2,
\end{equation}
where
\begin{equation}
  \begin{split}
    A(t) := & E(t) + \frac{1}{\tilde A_2} \left( \phi_{h,t} , \phi_h^D \right)_{\frac{1}{H}}, \\
    \tilde{A}_1 := & \frac{A_1}{\tilde A_2}, \\
    \tilde{A}_3 := & \frac{\tilde A_2}{2} + \frac{\tilde A_3}{\tilde A_2}.
  \end{split}
\end{equation}

Since we observe that
\begin{equation}
\left|\left(\phi_{h,t},\phi_h^D \right)_{\frac{1}{H}}\right| \leq \frac{C_P}{\sqrt{H_*}}\|\phi_{h,t}\|_{\frac{1}{H}}\|\nabla \cdot \phi\| \leq \frac{C_P \epsilon}{\sqrt{\beta H_*}}E(t),
\end{equation}
we set
\begin{equation}
\tilde A_2 = \max\left\{A_2,\frac{2C_P \epsilon}{\sqrt{\beta H_*}} \right\},
\end{equation}
which readily gives that
\begin{equation}
\label{eq:equivalence}
\frac{1}{2}E(t) \leq A(t) \leq \frac{3}{2}E(t).
\end{equation}
So, $A(t)$ is asymptotically equivalent to the energy, and, from \eqref{eq:energybound3}, we have the bound
\begin{equation}
   \frac{d}{dt}A(t) + \frac{2}{3\tilde A_2}A(t) + \leq \tilde A_1 + \tilde A_3 \| F \|^2,
\end{equation}
which implies
\begin{equation}
  A(T) \leq e^{-\frac{2t}{3\tilde A_2}} \left( A(0) + \tilde A_1 \right)
+ \int_0^T e^{-\frac{2t}{3\tilde A_2}(T-t)} \tilde A_3 \| F \|^2 dt
\end{equation}
and hence that
\begin{equation}
  \label{eq:ETbound}
  E(T) \leq e^{-\frac{2t}{3\tilde A_2}} \left( 3 E(0) + 2\tilde A_1 \right)
  + 2 \tilde A_3 \int_0^T e^{-\frac{2t}{3\tilde A_2}(T-t)} \| F \|^2 dt
\end{equation}
In particular, if $F \in L^\infty(0,T;L^2(\Omega))$, then the fact
that $\int_0^t e^{2(s-t)/3\tilde A_3} ds$ is bounded above for all $t$
shows that $E(t)$ remains bounded for all time.
This demonstrates the geotryptic balance of the model under nonlinear damping.
