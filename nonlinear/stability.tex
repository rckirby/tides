\section{Energy estimates}
\label{se:energy}
Now, we demonstrate the stability of this system.  In the linear
damping case, studied in~\cite{}, exponential damping was demonstrated
in the unforced case.  Moreover, subject to very mild growth
assumptions on the forcing term, we showed that the system energy
remained bounded for all time.  In this section, we revisit these
questions for the case of nonlinear damping, adapting techniques from
the PDE literature~\cite{}.

It is easy to show that, absent forcing or damping ($F = g = 0$), that
selecting $v_h = u_h$ and $w_h = \frac{\beta}{\epsilon^2}$
in~\eqref{eq:discrete_mixed} or $v_h = \phi_{h,t}$
in~\eqref{eq:secondorder_discrete} gives that the energy functional
\begin{equation}
E(t) = \frac{1}{2} \weightednorm{ u_h}{\frac{1}{H}}^2 
+ \frac{\beta}{2\epsilon^2} \norm{\eta_h}^2
= \frac{1}{2} \weightednorm{\phi_{h,t}}{\frac{1}{H}}^2 
+ \frac{\beta}{2\epsilon^2} \norm{\nabla \cdot \phi_h}^2
\label{eq:energy}
\end{equation}
is exactly conserved.


With a nonzero damping satisfying the assumptions, it is also easy to
demonstrate that the energy is nonincreasing in time.  By selecting
$v_h = \phi_{h,t}$ in~\eqref{eq:secondorder_discrete} with $F=0$, we
find that
\[
\frac{d}{dt} E(t)
+ \left( g(\cdot,\phi_{h,t}), \phi_{h,t} \right) = 0.
\]
The monotonicity assumptions on $g$ mean that the time derivative of
$E$ must therefore be negative.

In fact, with nondegenerate linear damping, we also showed that the
initial energy decays exponentially to zero in the absence of forcing
by deriving a first-order linear ODE for the energy.  However, in the
nonlinear case, the relative damping rate may decrease concurrently
with the energy.  While this rules out exponential decay, it is
possible to give concrete decay rates.

\subsection{Decay rates}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Step 1: Bounds on the divergence part
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\textit{Step 1.}
Take $v_h = \phi_h^D$ in \eqref{eq:secondorder_discrete} and integrate in time.  Integration by parts gives
\begin{multline} \label{eq:energyestimate1}
\left. \left(\phi_{h,t},\phi_h^D \right)_{\frac{1}{H}} \right|_0^T 
- \int_0^T \|\phi_{h,t}^D\|_{\frac{1}{H}}^2 dt + \frac{1}{\epsilon} \int_0^T \left(f\phi_{h,t}^\perp,\phi_h^D \right)_{\frac{1}{H}}dt 
\\
+ \frac{\beta}{\epsilon^2} \int_0^T \|\nabla \cdot \phi_h\|^2 dt 
+ \int_0^T \left(g(\cdot,\phi_{h,t}),\phi_h^D\right) dt = 0.
\end{multline}
Here and in the following we use that $\nabla \cdot \phi_h =\nabla \cdot \phi_h^D$.
Using the Cauchy-Schwartz inequality and \eqref{eq:pf}, Equation \eqref{eq:energyestimate1} becomes
\begin{multline} \label{eq:energyestimate2}
\frac{\beta}{\epsilon^2} \int_0^T \|\nabla \cdot \phi_h\|^2 dt 
\leq
\frac{C_P}{\sqrt{H_*}}\|\phi_{h,t}(T)\|_{\frac{1}{H}}\|\nabla \cdot \phi_h(T)\|
+ \frac{\epsilon^2 C_P}{\beta \sqrt{H_*}}\|\phi_{h,t}(0)\|_{\frac{1}{H}}\|\nabla \cdot \phi_h(0)\| \\
+ \int_0^T \|\phi_{h,t}^D\|_{\frac{1}{H}}^2 dt 
 + \frac{f^* C_P}{\epsilon\sqrt{H_*}} \int_0^T \|f\phi_{h,t}^\perp\|_{\frac{1}{H}}\|\nabla \cdot \phi_h\|dt \\
+ \frac{C_P\sqrt{H^*}}{\sqrt{H_*}}\int_0^T \|g(\cdot,\phi_{h,t})\|\|\nabla \cdot \phi_h\| dt.
\end{multline}
We handle the terms at time $T$ and $0$ by the weighted inequality
$ab \leq \frac{a^2}{2\delta} + \frac{b^2\delta}{2}$ with $\delta= \frac{\epsilon}{\sqrt{\beta}}$.  Then, we pull out $f^*$ from $\| f \phi_{h,t}^\perp \|$ and use that $\| \phi_{h,t}^D \| \leq \| \phi_{h,t} \|$ and that $\cdot^\perp$ is an isometry to obtain
\begin{multline}
  \label{eq:energyestimate2.5}
  \frac{\beta}{\epsilon^2} \int_0^T \|\nabla \cdot \phi_h\|^2 dt
  \leq \frac{C_P\sqrt{\beta}}{\epsilon \sqrt{H_*}} \left[ E(T) + E(0) \right]
  + \int_0^T \|\phi_{h,t}\|_{\frac{1}{H}}^2 dt  \\
 + \frac{f^* C_P}{\epsilon\sqrt{H_*}} \int_0^T \|\phi_{h,t}\|_{\frac{1}{H}}\|\nabla \cdot \phi_h\|dt
+ \frac{C_P\sqrt{H^*}}{\sqrt{H_*}}\int_0^T \|g(\cdot,\phi_{h,t})\|\|\nabla \cdot \phi_h\| dt.
\end{multline}
Next, we handle the terms under the integrals with the same weighted inequality.  In the first case we use $\delta = \frac{\beta\sqrt{H_*}}{2\epsilon f^* C_P}$ and in the second we use $\delta = \frac{\beta\sqrt{H_*}}{2\epsilon^2 C_P \sqrt{H^*}}$.  Then, collecting terms and using that $E(T) \leq E(0)$, we have
\begin{multline} \label{eq:energyestimate3}
  \frac{\beta}{\epsilon^2} \int_0^T \| \nabla \cdot \phi_h \|^2 dt
  \leq \frac{2C_P\sqrt{\beta}}{\epsilon \sqrt{H_*}} E(0) \\
  + \left(1+\frac{f^* C_P^2}{\beta H_*} \right)
  \int_0^T \weightednorm{\phi_{h,t}}{\frac{1}{H}}^2 dt
  + \frac{C_P^2 H^* \epsilon^2}{\beta H_*}
  \int_0^T \norm{g(\cdot, \phi_{h,t})}^2 dt
\end{multline}

So then, it follows that
\begin{multline}\label{eq:energyestimate4}
\int_0^T E(t)dt = \int_0^T \left(\frac{1}{2} \weightednorm{\phi_{h,t}}{\frac{1}{H}}^2 
+ \frac{\beta}{2\epsilon^2} \norm{\nabla \cdot \phi_h}^2  \right)dt \\
  \leq \frac{2C_P\sqrt{\beta}}{\epsilon \sqrt{H_*}} E(0) \\
  + \left(\frac{3}{2}+\frac{f^* C_P^2}{\beta H_*} \right)
  \int_0^T \weightednorm{\phi_{h,t}}{\frac{1}{H}}^2 dt
  + \frac{C_P^2 H^* \epsilon^2}{\beta H_*}
  \int_0^T \norm{g(\cdot, \phi_{h,t})}^2 dt
\end{multline}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Step 2: Controlling the nonlinearity.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\textit{Step 2.}
We make the following assumptions on $g$.
First, it is monotone, non-degenerate:
\begin{equation}
\label{eq:monotone}
(g(x,v) - g(x,w)) \cdot (v-w) \geq 0 \ \forall v,w, \ g(x,v) \cdot v > 0 \ \forall v \neq 0.
\end{equation}
Second, it behaves linearly for large vectors $v$. That is, for some $M > 0$ we have
\begin{equation} \label{eq:growthatinfinity}
|v|^2, |g(x,v)|^2 \leq M g(x,v) \cdot v \ \ \ \forall |v| \geq 1, \forall x \in \Omega.
\end{equation}
Finally, we assume $g$ is continuous in both variables.
Note that \eqref{eq:monotone} and continuity imply $g(x,0) = 0$.


Now we analyze the right-hand side of \eqref{eq:energyestimate4} by separating into regions of space-time where $\phi_{h,t}$ is large and small.
Set $\Sigma = \Omega \times [0,T]$, and let
$$
\Sigma_1 = \{(x,t) \in \Sigma : |\phi_{h,t}(x,t)| \geq 1\}, \ \Sigma_0 = \{(x,t) \in \Sigma : |\phi_{h,t}(x,t)| < 1\}.
$$
Then use \eqref{eq:growthatinfinity} to write \eqref{eq:energyestimate4} as
\begin{multline}\label{eq:energyestimate5}
\int_0^T E(t)dt = \int_0^T \left(\frac{1}{2} \weightednorm{\phi_{h,t}}{\frac{1}{H}}^2 
+ \frac{2 C_P \sqrt{\beta}}{2\epsilon^2} \norm{\nabla \cdot \phi_h}^2  \right)dt
\leq \frac{ C_P}{\sqrt{H_*}}E(0) \\
+ \left(\frac{3M}{2H_*} + \frac{M C_P^2 (f^*)^2}{ H_*^2\beta} + \frac{M\epsilon^2 C_P^2 H^*}{\beta H_*}\right)\iint_{\Sigma_1} g(x,\phi_{h,t}) \cdot \phi_{h,t} dx dt \\
+ \left(\frac{3}{2} + \frac{ C_P^2 (f^*)^2}{\beta H_*^2}\right) \iint_{\Sigma_0} |\phi_{h,t}|^2 dx dt 
+ \frac{\epsilon^2 C_P^2 H^*}{\beta H_*}\iint_{\Sigma_0} |g(\cdot,\phi_{h,t})|^2 dx dt .
\end{multline}
The second term on the right-hand side is estimated above by the dissipation term from the energy identity.
It remains to estimate the last two terms.
For this we need the following technical lemma.

\begin{lemma} \label{lem:auxiliaryfunction}
	Let $g = g(x,v)$ be a continuous function on $\overline{\Omega} \times \mathbb{R}^d$, where $\Omega$ is a bounded domain, satisfying  \eqref{eq:monotone} and \eqref{eq:growthatinfinity}.
	Then there exists an increasing, concave function $J:[0,\infty) \to [0,\infty)$ such that $J(0) = 0$ and
	\begin{equation} \label{eq:J}
	|v|^2 + |g(x,v)|^2 \leq J(v \cdot g(x,v)) \ \ \ \forall |v| \leq 1, \ \forall x \in \overline{\Omega}.
	\end{equation}
\end{lemma}

\begin{proof}
Let $\partial B_1 = \{e \in \mathbb{R}^d : |e| = 1\}$.
For $e \in \partial B_1$ and $x \in \overline{\Omega}$, set
$$
h_{e,x}(s) = se \cdot g(x,se), \ \ j_{e,x}(s) = s^2 + \max\{|g(x,te)|^2 : 0 \leq t \leq s\}.
$$
Note that both functions are strictly increasing in $s$; $j_{e,x}$ is the sum of two increasing functions, one of them strictly increasing, while in the case of $h_{e,x}(s)$, we use \eqref{eq:monotone} to check:
\begin{align*}
s > t \ \Rightarrow h_{e,x}(s) - h_{e,x}(t) &= (s-t)e \cdot g(x,se) + te \cdot (g(x,se)-g(x,te))\\
&= (s-t)e \cdot g(x,se) + \frac{t}{s-t}(se-te) \cdot (g(x,se)-g(x,te))\\
&\geq (s-t)e \cdot g(x,se) > 0.
\end{align*}
Moreover by \eqref{eq:growthatinfinity} we have that $h_{e,x}(s) \to \infty$ as $s \to \infty$.
Let $h_{e,x}^{-1}:[0\infty) \to [0,\infty)$ be the inverse function of $h_{e,x}(\cdot)$.
Our goal is to show that 
$$
j(t) := \max_{(e,x) \in \partial B_1 \times \overline{\Omega}} j_{e,x}(h_{e,x}^{-1}(t))
$$
exists (that is, it is finite for all $t$).
To do this, it is sufficient to see that $j_{e,x}(s)$ and $h_{e,x}^{-1}(t)$ are both continuous in the pair $(e,x)$.
The continuity of $(e,x) \mapsto j_{e,x}(s)$ follows in a straightforward manner from the uniform continuity of $g$ on compact sets.
Likewise, $h_{e,x}(s)$ is continuous in $(e,x)$.
To see that $h_{e,x}^{-1}(t)$ is continuous in $(e,x)$, we assume to the contrary that there exists some sequence $(e_n,x_n) \in \partial B_1 \times \overline{\Omega}$ such that $(e_n,x_n) \to (e,x)$ while $|h_{e_n,x_n}^{-1}(t) - h_{e,x}^{-1}(t)| \geq \epsilon$.
Let $s_n = h_{e_n,x_n}^{-1}(t)$ and $s = h_{e,x}^{-1}(t)$.
There are two cases:
\begin{enumerate}
	\item[1:] $s_n \geq s + \epsilon$.
	Since $h_{e_n,x_n}$ and $h_{e,x}$ are strictly increasing, it follows that $h_{e_n,x_n}(s_n) \geq h_{e_n,x_n}(s+\epsilon) \to h_{e,x}(s+\epsilon) > h_{e,x}(s)$.
	But this implies $t > t$, a contradiction.
	
	\item[2:] $s_n \leq s - \epsilon$.
	We have $h_{e_n,x_n}(s_n) \leq h_{e_n,x_n}(s-\epsilon) \to h_{e,x}(s-\epsilon) < h_{e,x}(s)$, so $t < t$, a contradiction.
\end{enumerate}
We now see that $h_{e,x}^{-1}(t)$ is continuous in $(e,x)$ for every $t \geq 0$.

To complete the proof, observe that $j(t)$ is well-defined and finite for all $t \geq 0$, that $j(0) = 0$, and $j$ is increasing.
Set
$$
t_1 := \max\{v \cdot g(x,v) : |v| \leq 1, x \in \overline{\Omega}\}.
$$
Finally, let $J$ be the concave envelope of $j$ restricted to $[0,t_1]$ (and constant on $[t_1,\infty)$).
Then $J$ satisfies all the desired properties.
\end{proof}

Before continuing with energy estimates, we consider examples where the function $J$ can be chosen explicitly.
Let $p > 1$ and set
\begin{equation}
\label{eq:pgrowth}
g(x,v) = \left\{
\begin{array}{cc}
|v|^{p-2}v & \text{if} \ |v| \leq 1\\
v & \text{if} \ |v| \geq 1
\end{array}\right.
\end{equation}
When $p > 2$ we refer to this as \textit{superlinear growth} while $p < 2$ is called \textit{sublinear growth}.

\textit{Superlinear growth:} If $p > 2$, $|v|$ dominates $|g(x,v)|$ and so \eqref{eq:J} can be replaced by
$$
2|v|^2 \leq J(|v|^p).
$$
It suffices to choose $J(s) = 2s^{2/p}$.

\textit{Sublinear growth:} If $p < 2$, $|g(x,v)|$ dominates $|v|$ and so \eqref{eq:J} can be replaced by
$$
2|v|^{2(p-1)} \leq J(|v|^p).
$$
It suffices to choose $J(s) = 2s^{2(p-1)/p}$.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Step 3: Getting an observability inequality.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\textit{Step 3.}
Now taking the function $J$ from Lemma we deduce from \eqref{eq:energyestimate5} that
\begin{multline}\label{eq:energyestimate6}
\int_0^T E(t)dt 
\leq C_0 E(0)
+ C_1\iint_{\Sigma_1} g(x,\phi_{h,t}) \cdot \phi_{h,t} dx dt \\
+ C_2 \iint_{\Sigma_0} J(g(x,\phi_{h,t}) \cdot \phi_{h,t}) dx dt .
\end{multline}
where
\begin{multline}
C_0 := \frac{2 C_P \sqrt{\beta}}{\epsilon \sqrt{H_*}}, \\
C_1 := \frac{3M}{2H_*} + \frac{M C_P^2 (f^*)^2}{\beta H_*^2} + \frac{M\epsilon^2 C_P^2 H^*}{\beta H_*},
\\ 
C_2 := \max\left\{\frac{3}{2\beta H_*} + \frac{ C_P^2 (f^*)^2}{\beta H_*^2},\frac{\epsilon^2 C_P^2 H^*}{\beta H_*}\right\}.
\end{multline}
Then using the fact that $g(x,\phi_{h,t}) \cdot \phi_{h,t}$ is non-negative, the fact that $H$ is increasing and concave, and Jensen's (reverse) inequality, we get
\begin{multline}\label{eq:energyestimate7}
\int_0^T E(t)dt 
\leq \frac{ C_P}{\sqrt{H_*}}E(0)
+ C_1\iint_{\Sigma} g(x,\phi_{h,t}) \cdot \phi_{h,t} dx dt \\
+ C_2|\Sigma| J\left(\frac{1}{|\Sigma|}\iint_{\Sigma} g(x,\phi_{h,t}) \cdot \phi_{h,t} dx dt \right),
\end{multline}
$|\Sigma|$ denoting the measure of $\Sigma$.
Now we use the energy identity to deduce that $\iint_{\Sigma} g(x,\phi_{h,t}) \cdot \phi_{h,t} dx dt = E(0) - E(T)$.
Since $E(t)$ is decreasing, \eqref{eq:energyestimate7} becomes
\begin{equation}
\label{eq:energyestimate8}
TE(T) \leq \frac{ C_P}{\sqrt{H_*}}E(0) + C_1(E(0)-E(T)) + C_2|\Sigma|J\left(\frac{E(0)-E(T)}{|\Sigma|}\right).
\end{equation}
Now fix $T = \displaystyle \frac{ C_P}{\sqrt{H_*}} + 1$.
Then from \eqref{eq:energyestimate8} we get
\begin{equation}
\label{eq:energyestimate9}
E(T) \leq \tilde{J}(E(0)-E(T))
\ \ \
\Rightarrow
\ \ \
E(T) + \tilde{J}^{-1}(E(T)) \leq E(0).
\end{equation}
where
$$
\tilde J(s) = \left(\frac{ C_P}{\sqrt{H_*}}+ C_1\right)s + C_2|\Sigma|J\left(\frac{1}{|\Sigma|}s\right).
$$
Of course, we may reiterate the same arguments on any interval $[nT,(n+1)T]$ to get
$$
E((n+1)T) + \tilde{J}^{-1}(E((n+1)T)) \leq E(nT).
$$
We may now appeal to Lemma 3.3 and the argument which follows in (Lasiecka-Tataru 1993) to assert that
\begin{equation} \label{eq:abstractdecayrate}
E(t) \leq S\left(\frac{t}{T}-1\right) = S\left(\frac{\sqrt{H_*}t}{C_P + \sqrt{H_*}}-1\right)
\end{equation}
where $S$ is the solution of the ordinary differential equation
\begin{equation} \label{eq:ode}
S'(t) + (I+\tilde J)^{-1}(S(t)) = 0, \ S(0) = E(0).
\end{equation}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Step 4: Computing decay rates explicitly.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\textit{Step 4.}
Equation \eqref{eq:abstractdecayrate} gives the general form of the decay rate of the energy.
Now we wish to give some applications.
It is important to note that because the energy is decreasing, it is only the concave $J$ that gives the dissipation term $(I+\tilde J)^{-1}$ its essential structure.
In particular, we can replace \eqref{eq:ode} with
\begin{equation} \label{eq:odeq}
S'(t) + q(S(t)) = 0, \ S(0) = E(0).
\end{equation}
where $q$ is any nonnegative, nondecreasing function such that $q(0) 0$ and $q \leq (I+\tilde J)^{-1}$.
Moreover, since $S$ is decreasing, we need only define $q$ on the interval $[0,E(0)]$.
Since $J$ is concave and $J(0) = 0$, we can write
\begin{equation}
J\left(\frac{s}{|\Sigma|}\right) \geq \frac{s}{E(0)}J\left(\frac{E(0)}{|\Sigma|}\right)
\ \ \
\forall s \in [0,E(0)]
\end{equation}
and therefore
\begin{equation}
(I+\tilde J)(s) = \left(1 + \frac{ C_P}{\sqrt{H_*}}+ C_1\right)s + C_2|\Sigma|J\left(\frac{s}{|\Sigma|}\right)
\leq C_3J\left(\frac{s}{|\Sigma|}\right)
\ \ \
\forall s \in [0,E(0)],
\end{equation}
where
\begin{equation}
C_J = \left(1 + \frac{ C_P}{\sqrt{H_*}}+ C_1\right)\frac{E(0)}{J\left(\frac{E(0)}{|\Sigma|}\right)} + C_2|\Sigma|.
\end{equation}
Then the desire properties are satisfied by
\begin{equation}
q(r) = |\Sigma|J^{-1}\left(\frac{r}{C_J}\right)
\end{equation}
so that, in other words, the decay rate is given by
\begin{equation} \label{eq:odeq2}
S'(t) + |\Sigma|J^{-1}\left(\frac{S(t)}{C_J}\right) = 0, \ S(0) = E(0).
\end{equation}

\textbf{Examples.}
Let us return to \eqref{eq:pgrowth}.

\textit{Superlinear growth:} If $p > 2$, we saw that $J(s) = 2s^{2/p}$, so that $J^{-1}(r) = (r/2)^{p/2}$.
In this case \eqref{eq:odeq2} becomes
\begin{equation}
S'(t) + \frac{|\Sigma|}{(2C_J)^{p/2}}S(t)^{p/2} = 0, \ S(0) = E(0).
\end{equation}

\textit{Sublinear growth:} If $p < 2$, we saw that $J(s) = 2s^{2(p-1)/p}$, so that $J^{-1}(r) = (r/2)^{p/2(p-1)}$.
In this case \eqref{eq:odeq2} becomes
\begin{equation}
S'(t) + \frac{|\Sigma|}{(2C_J)^{p/2(p-1)}}S(t)^{p/2(p-1)} = 0, \ S(0) = E(0).
\end{equation}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Long time stability or Zero Energy Growth: F bounded
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Long time stability}

\textit{Step 1.}
Take $v_h = \phi_h^D$ in \eqref{eq:secondorder_discrete} and rewrite
the time derivatives
\begin{multline} \label{eq:energybound1}
\frac{d}{dt}\left(\phi_{h,t},\phi_h^D \right)_{\frac{1}{H}} 
- \|\phi_{h,t}^D\|_{\frac{1}{H}}^2
 + \frac{1}{\epsilon}  \left(f\phi_{h,t}^\perp,\phi_h^D \right)_{\frac{1}{H}}
\\
+ \frac{\beta}{\epsilon^2} \|\nabla \cdot \phi_h\|^2 
+ \left(g(\cdot,\phi_{h,t}),\phi_h^D\right) = (F,\phi_h^D).
\end{multline}
Rearranging this and making estimates, we have
\begin{multline}
  \label{eq:energybound2}
  \frac{d}{dt}\left(\phi_{h,t},\phi_h^D \right)_{\frac{1}{H}}
  + \frac{\beta}{\epsilon^2} \|\nabla \cdot \phi_h\|^2
  + \|\phi_{h,t} \|_{\frac{1}{H}}^2
  \leq
  2 \|\phi_{h,t} \|_{\frac{1}{H}}^2
  + \frac{f^* C_P}{\epsilon} \|\phi_{h,t} \|_{\frac{1}{H}}
  \|\nabla \cdot \phi_h\| \\
  + \frac{C_P \sqrt{H^*}}{\sqrt{H_*}}
  \| g(\cdot,\phi_{h,t}) \|
  \|\nabla \cdot \phi_h\|
  + \frac{C_P \sqrt{H^*}}{\sqrt{H_*}} \| F \|
    \|\nabla \cdot \phi_h\|
\end{multline}
%% Using the same type of estimations as before, we arrive at
%% \begin{multline}
%% \label{eq:energybound2}
%% E(t) + \frac{d}{dt}\left(\phi_{h,t},\phi_h^D \right)_{\frac{1}{H}}  \leq 
%% \left(\frac{1}{2} + \frac{3(f^*C_P)^2}{2\beta H_*}\right) \|\phi_{h,t}\|_{\frac{1}{H}}^2
%% \\
%% + \frac{3(\epsilon C_P )^2 H^*}{2\beta H_*} \|g(\cdot,\phi_{h,t})\|^2
%% + \frac{3(\epsilon C_P)^2 H^*}{2\beta H_*} \|F\|^2.
%% \end{multline}

\textit{Step 2.}
As before, we separate into regions where $\phi_{h,t}$ is ``small" and where it is ``large."
Define
$$
\Omega_1(t) = \{(x,t) \in \Sigma : |\phi_{h,t}(x,t)| \geq 1\}, \ \Omega_0(t) = \{(x,t) \in \Sigma : |\phi_{h,t}(x,t)| < 1\}.
$$
Then using \eqref{eq:growthatinfinity} and the continuity of $g$, we have
\begin{multline}
\label{eq:boundong}
\|g(\cdot,\phi_{h,t})\|^2  = \int_{\Omega_0(t)} |g(x,\phi_{h,t})|^2 \ dx 
+ \int_{\Omega_1(t)} |g(x,\phi_{h,t})|^2 \ dx
\\
\leq |\Omega_0(t)| g^*
+ M\iint_{\Omega_1(t)} g(x,\phi_{h,t}) \cdot \phi_{h,t} \ dx
\\
\leq |\Omega| g^*
+ M(g(\cdot,\phi_{h,t}), \phi_{h,t}),
\end{multline}
where
\begin{equation}
  g^* := \max\{|g(x,v)|^2 : x \in \Omega, |v| \leq 1\}
\end{equation}
Similarly, we have
\begin{equation}
\|\phi_{h,t}\|_{\frac{1}{H}}^2  \leq 
\frac{|\Omega|}{H_*} + \frac{M}{H_*} (g(\cdot,\phi_{h,t}), \phi_{h,t}).
\end{equation}
Then \eqref{eq:energybound2} becomes
\begin{multline}
\label{eq:energybound3}
\frac{d}{dt}\left(\phi_{h,t},\phi_h^D \right)_{\frac{1}{H}}
+ \frac{\beta}{2\epsilon^2} \| \nabla \cdot \phi_h \|^2
+ \| \phi_{h,t} \|^2_{\frac{1}{H}}
\\
\leq
A_1 + A_2 \left( g(\cdot, \phi_{h,t}), \phi_{h,t} \right) + A_3 \| F \|^2,
\end{multline}
where
\begin{equation*}
  \begin{split}
A_1 := & \frac{3(\epsilon C_P )^2 H^*|\Omega| g^*}{2\beta H_*}
+ \left( 2 + \frac{3(f^* C_P)^2}{2\beta}\right) \frac{|\Omega|}{H^*} \\
A_2 := & \frac{3(\epsilon C_P )^2 H^* M}{2\beta H_*} 
+ \left( 2 + \frac{3(f^* C_P)^2}{2\beta}\right) \frac{M}{H^*} \\
A_3 : = & \frac{3(\epsilon C_P )^2 H^* M}{2\beta H_*}.
  \end{split}
\end{equation*}
The estimate~\eqref{eq:energybound3} also holds with $A_2$ replaced by $\tilde A_2 \geq A_2$, which we shall specify shortly.

Now, we have that
\(
\left( g(\cdot, \phi_{h,t}) , \phi_{h,t}) \right)
= \left( F, \phi_{h,t} \right) - \frac{d}{dt} E(t),
\)
so that
\begin{multline}
\label{eq:energybound3}
\frac{d}{dt}\left[ \left(\phi_{h,t},\phi_h^D \right)_{\frac{1}{H}} + \tilde A_2 E(t) \right]
+ \frac{\beta}{2\epsilon^2} \| \nabla \cdot \phi_h \|^2
+ \| \phi_{h,t} \|^2_{\frac{1}{H}}
\leq
A_1 + \tilde A_2 \left(F , \phi_{h,t} \right) + A_3 \| F \|^2.
\end{multline}
Then, using Young's inequality with appropriate weighting and dividing through by $\tilde A_2$ gives
\begin{equation}
\frac{d}{dt} A(t) + E(t) \leq \tilde{A_1} + \tilde{A}_3 \| F \|^2,
\end{equation}
where
\begin{equation}
  \begin{split}
    A(t) := & E(t) + \frac{1}{\tilde A_2} \left( \phi_{h,t} , \phi_h^D \right)_{\frac{1}{H}}, \\
    \tilde{A}_1 := & \frac{A_1}{\tilde A_2}, \\
    \tilde{A}_3 := & \frac{\tilde A_2}{2} + \frac{\tilde A_3}{\tilde A_2}.
  \end{split}
\end{equation}

Since we observe that
\begin{equation}
\left|\left(\phi_{h,t},\phi_h^D \right)_{\frac{1}{H}}\right| \leq \frac{C_P}{\sqrt{H_*}}\|\phi_{h,t}\|_{\frac{1}{H}}\|\nabla \cdot \phi\| \leq \frac{C_P \epsilon}{\sqrt{\beta H_*}}E(t),
\end{equation}
we set
\begin{equation}
\tilde A_2 = \max\left\{A_2,\frac{2C_P \epsilon}{\sqrt{\beta H_*}} \right\},
\end{equation}
which readily gives that
\begin{equation}
\label{eq:equivalence}
\frac{1}{2}E(t) \leq A(t) \leq \frac{3}{2}E(t).
\end{equation}
So, $A(t)$ is asymptotically equivalent to the energy, and, from \eqref{eq:energybound3}, we have the bound
\begin{equation}
   \frac{d}{dt}A(t) + \frac{2}{3\tilde A_2}A(t) + \leq \tilde A_1 + \tilde A_3 \| F \|^2,
\end{equation}
which implies
\begin{equation}
  A(T) \leq e^{-\frac{2t}{3\tilde A_2}} \left( A(0) + \tilde A_1 \right)
+ \int_0^T e^{-\frac{2t}{3\tilde A_2}(T-t)} \tilde A_3 \| F \|^2 dt
\end{equation}
and hence that
\begin{equation}
  \label{eq:ETbound}
  E(T) \leq e^{-\frac{2t}{3\tilde A_2}} \left( 3 E(0) + 2\tilde A_1 \right)
  + 2 \tilde A_3 \int_0^T e^{-\frac{2t}{3\tilde A_2}(T-t)} \| F \|^2 dt
\end{equation}
In particular, if $F \in L^\infty(0,T;L^2(\Omega))$, then the fact
that $\int_0^t e^{2(s-t)/3\tilde A_3} ds$ is bounded above for all $t$
shows that $E(t)$ remains bounded for all time.  This demonstrates the
geotryptic balance of the model under nonlinear damping.
