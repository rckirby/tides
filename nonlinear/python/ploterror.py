import numpy as np
import matplotlib
#matplotlib.use('agg')
import matplotlib.pyplot as plt

Ns = np.asarray([8, 16, 32, 64])
deg = 1
damps = ["glin", "gquad", "gcub"]
damplabels = ["Linear damping", "Quadratic damping", "Cubic damping"]
linetypes = ["-", ":", "--"]


def get(deg, N, damping):
    return np.load("data/error_%s_%d_%d.npy" % (damping, N, deg))

uerrs = np.asarray(
    [[get(deg, N, damping)[-1,1] for N in Ns] for damping in damps]
).T

perrs = np.asarray(
    [[get(deg, N, damping)[-1,2] for N in Ns] for damping in damps]
).T

for i, (lbl, lt) in enumerate(zip(damplabels, linetypes)):
    plt.loglog(1./Ns, uerrs[:, i], lt, label=lbl)

plt.title("Velocity error at "+ r'$T=10.0$')
plt.xlabel(r'$h$')
plt.ylabel(r'$||u(\cdot, T) - u_h(\cdot, T)||$')
plt.legend()
plt.savefig("pic/uerr_final_%d.png" % deg) 

plt.close()

for i, (lbl, lt) in enumerate(zip(damplabels, linetypes)):
    plt.loglog(1./Ns, perrs[:, i], lt, label=lbl)

plt.title("Elevation error at "+ r'$T=10.0$')
plt.xlabel(r'$h$')
plt.ylabel(r'$||\eta(\cdot, T) - \eta_h(\cdot, T)||$')
plt.legend()
plt.savefig("pic/perr_final_%d.png" % deg) 
plt.close()

# max norm error:

uerrs = np.asarray(
    [[np.max(get(deg, N, damping)[:, 1]) for N in Ns] for damping in damps])

perrs = np.asarray(
    [[np.max(get(deg, N, damping)[:, 2]) for N in Ns] for damping in damps])


for i, (lbl, lt) in enumerate(zip(damplabels, linetypes)):
    plt.loglog(1./Ns, uerrs[i, :], lt, label=lbl)

plt.title("Max velocity error over [0, 10]")
plt.xlabel(r'$h$')
plt.ylabel(r'$\max_{0 \leq t \leq 10} ||u(\cdot, t) - u_h(\cdot, t)||$')
plt.legend()
plt.tight_layout()
plt.savefig("pic/uerr_max_%d.png" % deg) 

plt.close()

for i, (lbl, lt) in enumerate(zip(damplabels, linetypes)):
    plt.loglog(1./Ns, perrs[i, :], lt, label=lbl)

plt.title("Max elevation error over [0, 10]")
plt.xlabel(r'$h$')
plt.ylabel(r'$\max_{0 \leq t \leq 10} ||\eta(\cdot, t) - \eta_h(\cdot, t)||$')
plt.legend()
plt.tight_layout()
plt.savefig("pic/perr_max_%d.png" % deg) 
plt.close()
