import numpy as np
import matplotlib
#matplotlib.use('agg')
import matplotlib.pyplot as plt

damps = ["glin", "gquad", "gcub"]
damplabels = ["Linear damping", "Quadratic damping", "Cubic damping"]
linetypes = ["-", "-.", ":"]


for i, (lbl, lt, g) in enumerate(zip(damplabels, linetypes, damps)):
    blah = np.load("data/stability_%s.npy" % g)
    plt.semilogy(blah[0][::100], blah[1][::100], lt, label=lbl)

plt.title("Energy over time for different damping functions")
plt.xlabel("t")
plt.ylabel("Energy")
plt.legend()

plt.savefig("pic/stability.png")
