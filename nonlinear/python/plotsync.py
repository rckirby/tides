import numpy as np
import matplotlib
#matplotlib.use('agg')
import matplotlib.pyplot as plt

deg = 1
damps = ["glin", "gquad", "gcub"]
N = 10
damplabels = ["Linear damping", "Quadratic damping", "Cubic damping"]
linetypes = ["-", ":", "--"]


for i, (lbl, lt, g) in enumerate(zip(damplabels, linetypes, damps)):
    blah = np.load("data/sync_%s.npy" % g)
    plt.loglog(blah[0], blah[1], lt, label=lbl)

plt.title("Synchronization of solutions")
plt.xlabel(r'$t$')
plt.ylabel(r'$E(t)$ for difference')
plt.legend()
plt.savefig("pic/sync.png")
