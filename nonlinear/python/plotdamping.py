import numpy as np
import matplotlib
#matplotlib.use('agg')
import matplotlib.pyplot as plt

damps = ["glin", "gquad", "gcub"]
damplabels = ["Linear damping", "Quadratic damping", "Cubic damping"]
linetypes = ["-", "-.", ":"]

for i, (lbl, lt, g) in enumerate(zip(damplabels, linetypes, damps)):
    blah = np.load("data/damping_%s.npy" % g)
    plt.loglog(blah[0], blah[1], lt, label=lbl)

plt.title("Energy over time for different damping functions")
plt.xlabel(r'$t$')
plt.ylabel(r'$E(t)$')
plt.legend()

plt.savefig("pic/damping.png")
