from firedrake import *
import numpy as np
from tools import *

Beta = Constant(0.1)
Eps = Constant(0.1)
f = Constant(1.0)
C = Constant(10.0)

N = 20
mesh = UnitSquareMesh(N, N)

V = FunctionSpace(mesh, "RT", 1)
W = FunctionSpace(mesh, "DG", 0)
Z = V*W

dt = 0.5 / N
k = Constant(dt)

up_orig = randomIC(Z, Beta, Eps)

T = 1000.0

def run(g):
    print "Running", g.__name__
    up = Function(Z)
    up.assign(up_orig)
    up_ = Function(Z)
    up_.assign(up)
    u, p = split(up)
    u_, p_ = split(up_)
    v, w = TestFunctions(Z)

    t = Constant(0.0)
    x, y = SpatialCoordinate(mesh)
    
    Forcing = Beta/Eps**2*sin(t)*x*y*div(v)*dx
    
    F = getF(u, p, u_, p_, v, w, Beta, Eps, f, C, k, g, Forcing)
    
    bcs = [DirichletBC(Z.sub(0), 0, "on_boundary")]

    prob = NonlinearVariationalProblem(F, up, bcs=bcs)

    solver = NonlinearVariationalSolver(
        prob,
        solver_prefix="",
        solver_parameters={"ksp_type": "preonly",
                           "pc_type": "lu",
                           "mat_type": "aij"}
    )

    tcur = 0.0

    Et = [(tcur, E(up, Beta, Eps))]
    
    #print "   %5f\t%.8f" % (tcur, E(up, Beta, Eps))
    while T - 1.e-8 > tcur + dt:
        t.assign(tcur+0.5*dt)
        solver.solve()
        up_.assign(up)
        tcur += dt
        Et.append((tcur, E(up, Beta, Eps)))
        #print "   %5f\t%.8f" % (tcur, E(up, Beta, Eps))

    np.save("data/stability_%s.npy" % g.__name__, np.asarray(Et).T)
    

from multiprocessing import Pool
p = Pool(3)
p.map(run, [glin, gquad, gcub])
#run(glin)

