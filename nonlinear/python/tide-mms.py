from firedrake import *
import numpy as np
from tools import *

Beta = Constant(0.1)
Eps = Constant(0.1)
f = Constant(1.0)
C = Constant(10.0)

T = 100


def run(blah):
    g, N, order = blah
    print "Running", g.__name__, "on %d x %d mesh with degree %d" % (N, N, order)
    mesh = UnitSquareMesh(N, N)
    
    V = FunctionSpace(mesh, "RT", order)
    W = FunctionSpace(mesh, "DG", order-1)
    Z = V*W
    
    dt = 0.5 / N
    k = Constant(dt)
    
    t, pex, uex, f1, f2 = uf(mesh, g, C, Eps, Beta, f)
    
    up = Function(Z)
    up.project(as_vector([uex[0], uex[1], pex]))
    up_ = Function(Z)
    up_.assign(up)
    u, p = split(up)
    u_, p_ = split(up_)
    v, w = TestFunctions(Z)
    
    Forcing = -f1*w*dx - inner(f2, v)*dx
    
    F = getF(u, p, u_, p_, v, w, Beta, Eps, f, C, k, g, Forcing)
    
    bcs = [DirichletBC(Z.sub(0), 0, "on_boundary")]
    
    prob = NonlinearVariationalProblem(F, up, bcs=bcs)
    
    solver = NonlinearVariationalSolver(
        prob,
        solver_prefix="",
        solver_parameters={"ksp_type": "preonly",
                           "pc_type": "lu",
                           "mat_type": "aij"}
    )
    
    tcur = 0.0
    
    Et = [(tcur, sqrt(assemble(inner(u-uex, u-uex)*dx)), sqrt(assemble((p-pex)**2*dx)))]
    #print "   %5f\t%.8f" % (tcur, E(up, Beta, Eps))
    for i in range(N*200):
        t.assign(tcur+0.5*dt)
        solver.solve()
        up_.assign(up)
        tcur += dt
        t.assign(tcur)
        Et.append((tcur, sqrt(assemble(inner(u-uex, u-uex)*dx)), sqrt(assemble((p-pex)**2*dx))))
        #print "   %5f\t%.8f\t%.8f" % (tcur, Et[-1][1], Et[-1][2])
        
    np.save("data/error_%s_%d_%d.npy" % (g.__name__, N, order), np.asarray(Et).T)
    

from multiprocessing import Pool

# cases = [(g, N, deg) for g in [glin, gquad, gcub] for N in (4, 8, 16, 32, 64) for deg in (1, 2)]
cases = [(g, N, deg) for g in [glin, gquad, gcub] for N in (64,) for deg in (1, 2)]

p = Pool(6)
p.map(run, cases)


