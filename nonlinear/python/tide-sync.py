from firedrake import *
import numpy as np
from tools import *

Beta = Constant(0.1)
Eps = Constant(0.1)
f = Constant(1.0)
C = Constant(10.0)

N = 20
mesh = UnitSquareMesh(N, N)

V = FunctionSpace(mesh, "RT", 1)
W = FunctionSpace(mesh, "DG", 0)
Z = V*W

dt = 0.5 / N
k = Constant(dt)

up_orig0 = randomIC(Z, Beta, Eps)
up_orig1 = randomIC(Z, Beta, Eps)

T = 100.0

diff = Function(Z)

def run(g):
    print "Running", g.__name__
    up0 = Function(Z)
    up0.assign(up_orig0)
    up0_ = Function(Z)
    up0_.assign(up0)
    u0, p0 = split(up0)
    u0_, p0_ = split(up0_)

    up1 = Function(Z)
    up1.assign(up_orig1)
    up1_ = Function(Z)
    up1_.assign(up1)
    u1, p1 = split(up1)
    u1_, p1_ = split(up1_)

    v, w = TestFunctions(Z)

    t = Constant(0.0)
    x, y = SpatialCoordinate(mesh)
    
    Forcing = Beta/Eps**2*sin(t)*x*y*div(v)*dx
    
    F0 = getF(u0, p0, u0_, p0_, v, w, Beta, Eps, f, C, k, g, Forcing)
    F1 = getF(u1, p1, u1_, p1_, v, w, Beta, Eps, f, C, k, g, Forcing)
    
    bcs = [DirichletBC(Z.sub(0), 0, "on_boundary")]

    prob0 = NonlinearVariationalProblem(F0, up0, bcs=bcs)
    prob1 = NonlinearVariationalProblem(F1, up1, bcs=bcs)

    solver0 = NonlinearVariationalSolver(
        prob0,
        solver_prefix="",
        solver_parameters={"ksp_type": "preonly",
                           "pc_type": "lu",
                           "mat_type": "aij"}
    )

    solver1 = NonlinearVariationalSolver(
        prob1,
        solver_prefix="",
        solver_parameters={"ksp_type": "preonly",
                           "pc_type": "lu",
                           "mat_type": "aij"}
    )

    tcur = 0.0

    for i in (0, 1):
        diff.dat.data[i][:] = up0.dat.data[i][:] - up1.dat.data[i][:]
    
    Et = [(tcur, E(diff, Beta, Eps))]
    
    while T - 1.e-8 > tcur + dt:
        solver0.solve()
        solver1.solve()
        up0_.assign(up0)
        up1_.assign(up1)
        tcur += dt
        for i in (0, 1):
            diff.dat.data[i][:] = up0.dat.data[i][:] - up1.dat.data[i][:]
        Et.append((tcur, E(diff, Beta, Eps)))

    np.save("data/sync_%s.npy" % g.__name__, np.asarray(Et).T)
    

from multiprocessing import Pool
p = Pool(3)
p.map(run, [glin, gquad, gcub])


