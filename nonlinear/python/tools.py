from firedrake import *
import numpy as np


def glin(C, u):
    return C*u


def gquad(C, u):
    return C*sqrt(inner(u, u))*u


def gcub(C, u):
    return C*inner(u, u)*u


def getF(u, p, u_, p_, v, w, Beta, Eps, f, C, k, g, Forcing):
    Theta = Constant(0.5)
    uu = Theta*u + (1-Theta)*u_
    pp = Theta*p + (1-Theta)*p_
    F = (
        inner((u-u_)/k, v)*dx - Beta/Eps**2 * pp*div(v)*dx
        + f/Eps*inner(perp(uu), v)*dx
        + (p-p_)/k*w*dx + div(uu)*w*dx
        + inner(Theta*g(C, u)+Theta*g(C, u_), v)*dx
        + Forcing
    )

    return F


def E(up, Beta, Eps):
    u, p = split(up)
    return 0.5*assemble(inner(u, u)*dx+Beta/Eps**2*p**2*dx)
    

def randomIC(Z, Beta, Eps):
    up = Function(Z).project(Expression([0, 0, "cos(pi*x[0])*cos(pi*x[1])"]))

    up.dat.data[0][:] = np.random.randn(up.dat.data[0].size)
    up.dat.data[1][:] = np.random.randn(up.dat.data[1].size)

    u, p = split(up)

    # zero mean pressure
    pbar = assemble(p*dx)
    up.dat.data[1][:] -= pbar

    # normalize for unit energy
    up.dat.data[0][:] /= sqrt(assemble(inner(u, u)*dx))
    up.dat.data[1][:] /= sqrt(assemble(Beta/Eps**2*p**2*dx))

    return up


def uf(m, g, C, Eps, Beta, f):
    t = Constant(0.0)
    x, y = SpatialCoordinate(m)

    p_exact = sin(pi*x)*sin(2*pi*y)*cos(pi*t)
    u_exact = as_vector(
        [sin(pi*x)*cos(pi*y)*cos(pi*t), cos(pi*x)*sin(pi*y)*cos(pi*t)]
    )
    
    f1 = diff(p_exact, t) + div(u_exact)
    f2 = diff(u_exact, t) + Eps/Beta**2*grad(p_exact) + f/Eps*perp(u_exact) + g(C, u_exact)

    return t, p_exact, u_exact, f1, f2

