from firedrake import *
import numpy as np
from tools import *

N = 20
dt = 0.5 / N
k = Constant(dt)

mesh = UnitSquareMesh(N, N)

Beta = Constant(0.1)
Eps = Constant(0.1)
f = Constant(1.0)
C = Constant(1.0)

V = FunctionSpace(mesh, "RT", 1)
W = FunctionSpace(mesh, "DG", 0)
Z = V*W

up = randomIC(Z, Beta, Eps)
up_ = Function(Z)

up_.assign(up)

u, p = split(up)
u_, p_ = split(up_)

v, w = TestFunctions(Z)

F = getF(u, p, u_, p_, v, w, Beta, Eps, f, C, k, glin)

T = 1.0
tcur = 0.0

bcs = [DirichletBC(Z.sub(0), 0, "on_boundary")]

prob = NonlinearVariationalProblem(F, up, bcs=bcs)

solver = NonlinearVariationalSolver(
    prob,
    solver_prefix="",
    solver_parameters={"ksp_type": "preonly",
                       "pc_type": "lu",
                       "mat_type": "aij"}
    )

print "%5f\t%.8f" % (tcur, E(up, Beta, Eps))
while T - 1.e-8 > tcur + dt:
    solver.solve()
    up_.assign(up)
    tcur += dt
    print "%5f\t%.8f" % (tcur, E(up, Beta, Eps))

    

