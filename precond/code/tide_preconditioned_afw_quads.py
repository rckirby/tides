from firedrake import *
import numpy as np
from petsc4py import *


def tide(parameters, order, n, k, eps, prec_type):
    mesh = UnitSquareMesh(n, n, quadrilateral=True)
    
    V = FunctionSpace(mesh, "SminusF", order)
    Q = FunctionSpace(mesh, "DPC", order-1)
    Z=V*Q

    x, y = SpatialCoordinate(mesh)
    
    up_ = Function(Z)
    up = Function(Z)
    
    vq = TestFunction(Z)

    u_, p_ = split(up_)
    u, p = split(up)
    v, q = split(vq)
    

    k = Constant(k)
    Eps = Constant(eps)
    Beta = Constant(0.1)
    C = Constant(1.0)
    
    f = Constant(1.0)

    beps2 = Beta / Eps / Eps
    
    F = (
        inner(u, v) * dx
        + k / Eps * f * inner(perp(u),v) * dx
        - k * beps2 * inner(p, div(v)) * dx
        + C * k * inner(u,v) * dx
        + beps2 * inner(p, q) * dx
        + k * beps2 * inner(div(u), q) * dx
        - beps2 * inner(sin(np.pi*x)*cos(np.pi*y),q)*dx #g
    )
    
    bcs = [DirichletBC(Z.sub(0), 0, (1,2,3,4))]

    uu, pp = TrialFunctions(Z)
    if prec_type == "Riesz":
        Jpc = (
            (Constant(1.0) + C * k) * inner(uu,v)*dx
            + k**2 * beps2 * inner(div(uu),div(v)) *dx
            + beps2 * inner(pp,q)*dx
        )
    elif prec_type == "Riesz_Lite":
        Jpc = (
            inner(uu,v)*dx
             + k**2 * beps2 * inner(div(uu),div(v)) *dx
             + beps2 * inner(pp,q)*dx
        )
    elif prec_type == "Mass":
        Jpc = (
            inner(uu,v)*dx + beps2 * inner(pp,q)*dx
        )
    
    up_problem = NonlinearVariationalProblem(F, up, bcs=bcs, Jp=Jpc)
    up_solver = NonlinearVariationalSolver(up_problem,
                                           solver_parameters = parameters)
    
    try:
        up_solver.solve()
        iterations = up_solver.snes.getLinearSolveIterations()
    except:
        iterations = -1
    return(iterations)

# for multiprocessing
def f(x):
    (param, order, N, k, eps, prec_type) = x
    print("Running k=%e, N=%d, eps=%e, prec=%s"%(k, N, eps, prec_type)) 
    return tide(param, order, N, k, eps, prec_type)
    

if __name__ == "__main__":
    from multiprocessing import Pool
    
    param = {"mat_type": "aij",
             "snes_type": "ksponly",
             "ksp_type": "gmres",
             "ksp_gmres_restart": 100,
             "pc_type": "lu"
    }
    
    prec_types = ["Riesz", "Riesz_Lite"]

    k = [10.0**N for N in range(-6, 2)]
    orders = [2]
    Ns = [2**N for N in range(1, 8)]
    eps = [10**N for N in range(-3, 0)]

    opts = [(param, order, N, k_iter, eps_iter, prec_type)
            for order in orders
            for k_iter in k
            for N in Ns
            for eps_iter in eps
            for prec_type in prec_types]

    p = Pool(16)
    results_list = p.map(f, opts)

    results = {}
    for prec_type in prec_types:
        results[prec_type] = {}
        for order in orders:
            results[prec_type][order] = {}
            for eps_iter in eps:
                results[prec_type][order][eps_iter] = {}
                for N in Ns:
                    results[prec_type][order][eps_iter][N] = {}
                    for k_iter in k:
                        results[prec_type][order][eps_iter][N][k_iter] = 0

    for (_, order, N, k_iter, eps_iter, prec_type), r in zip(opts, results_list):
        results[prec_type][order][eps_iter][N][k_iter] = r

    import pickle

    with open('tide_Sminus_quad.p', 'wb') as f:
        pickle.dump(results, f)


    
