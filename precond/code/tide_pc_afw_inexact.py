from firedrake import *
import numpy as np
from petsc4py import *

import HypreAMS_preconditioner, HDivRiesz

def tide(parameters, quad, order, n, k, eps, prec_type):
    mesh = UnitSquareMesh(n//16, n//16, quadrilateral=quad)
    mh = MeshHierarchy(mesh, 4)
    mesh = mh[-1]
    if quad:
        V = FunctionSpace(mesh, "SminusF", order)
        Q = FunctionSpace(mesh, "DPC", order-1)
    else:
        V = FunctionSpace(mesh, "RT", order)
        Q = FunctionSpace(mesh, "DG", order-1)

    Z=V*Q

    x, y = SpatialCoordinate(mesh)

    up_ = Function(Z)
    up = Function(Z)

    vq = TestFunction(Z)

    u_, p_ = split(up_)
    u, p = split(up)
    v, q = split(vq)

    k = Constant(k)
    Eps = Constant(eps)
    Beta = Constant(0.1)
    C = Constant(1.0)

    f = Constant(1.0)

    beps2 = Beta / Eps / Eps

    F = (
        inner(u, v) * dx
        + k / Eps * f * inner(perp(u),v) * dx
        - k * beps2 * inner(p, div(v)) * dx
        + C * k * inner(u,v) * dx
        + beps2 * inner(p, q) * dx
        + k * beps2 * inner(div(u), q) * dx
        - beps2 * inner(sin(np.pi*x)*cos(np.pi*y),q)*dx #g
    )

    bcs = [DirichletBC(Z.sub(0), 0, (1,2,3,4))]

    uu, pp = TrialFunctions(Z)
    if prec_type == "Riesz":
        Jpc = (
            (Constant(1.0) + C * k) * inner(uu,v)*dx
            + k**2 * beps2 * inner(div(uu),div(v)) *dx
            + beps2 * inner(pp,q)*dx
        )
        appctx={'alpha': Constant(1.0+C*k),
                'beta': k**2 * beps2}
    elif prec_type == "Riesz_Lite":
        Jpc = (
            inner(uu,v)*dx
             + k**2 * beps2 * inner(div(uu),div(v)) *dx
             + beps2 * inner(pp,q)*dx
        )
        appctx={'alpha': Constant(1.0),
                'beta': k**2 * beps2}


    up_problem = NonlinearVariationalProblem(F, up, bcs=bcs, Jp=Jpc)
    up_solver = NonlinearVariationalSolver(up_problem,
                                           solver_parameters = parameters, appctx=appctx)

    try:
        up_solver.solve()
        iterations = up_solver.snes.getLinearSolveIterations()
    except:
        iterations = -1
    return(iterations)

# for multiprocessing
def f(x):
    (param, quad, order, N, k, eps, prec_type) = x
    print("Running k=%e, N=%d, eps=%e, prec=%s"%(k, N, eps, prec_type)) 
    return tide(param, quad, order, N, k, eps, prec_type)


param_split_lu = {"mat_type": "aij",
                  "snes_type": "ksponly",
                  "ksp_type": "gmres",
                  "ksp_gmres_restart": 100,
                  "pc_type": "fieldsplit",
                  "pc_fieldsplit_type": "additive",
                  "fieldsplit_0": {
                      "ksp_type": "preonly",
                      "pc_type": "lu"},
                  "fieldsplit_1": {
                      "ksp_type": "preonly",
                      "pc_type": "lu"},
}

param_lu = {"mat_type": "aij",
            "snes_type": "ksponly",
            "ksp_type": "gmres",
            "ksp_gmres_restart": 100,
            "pc_type": "lu"
            }

param_split_hypre = {
    "mat_type": "matfree",
    "snes_type": "ksponly",
    "ksp_type": "gmres",
    "pc_type": "fieldsplit",
    "pc_fieldsplit_type": "additive",
    "pc_fieldsplit_0_fields": "0",
    "pc_fieldsplit_1_fields": "1",
    "fieldsplit_0": {
        "ksp_type": "preonly",
        "pc_type": "python",
        "pc_python_type": "HypreAMS_preconditioner.HypreAMS"},
    "fieldsplit_1": {
        "ksp_type": "preonly",
        "pc_type": "python",
        "pc_python_type": "firedrake.AssembledPC",
        "assembled_pc_type": "jacobi"}
}

param_mg = {"mat_type": "matfree",
          "pmat_type": "matfree",
          "snes_type": "ksponly",
          "ksp_type": "gmres",
          "pc_type": "fieldsplit",
          "pc_fieldsplit_type": "additive",
          "fieldsplit_0":{
              "ksp_type": "preonly",
              "pc_type": "python",
              "pc_python_type": "HDivRiesz.Riesz",
              "aux": {
                  "pc_type": "mg",
                  "pc_mg_type": "full",
                  "mg_levels": {
                      "ksp_type": "richardson",
                      "ksp_norm_type": "unpreconditioned",
                      #"ksp_monitor_true_residual": None,
                      "ksp_richardson_scale": 1/3,
                      "ksp_max_it": 1,
                      "ksp_convergence_test": "skip",
                      "pc_type": "python",
                      "pc_python_type": "firedrake.PatchPC",
                      "patch_pc_patch_save_operators": True,
                      "patch_pc_patch_partition_of_unity": False,
                      "patch_pc_patch_construct_type": "star",
                      "patch_pc_patch_construct_dim": 0,
                      "patch_pc_patch_sub_mat_type": "seqdense",
                      "patch_sub_ksp_type": "preonly",
                      "patch_sub_pc_type": "lu"},
                  "mg_coarse_pc_type": "python",
                  "mg_coarse_pc_python_type": "firedrake.AssembledPC",
                  "mg_coarse_assembled_pc_type": "lu",
                  "mg_coarse_assembled_pc_factor_mat_solver_type": "mumps",
              },
          },
            "fieldsplit_1":{
                "ksp_type": "preonly",
                "pc_type": "python",
                "pc_python_type": "firedrake.AssembledPC",
                "assembled_pc_type": "jacobi"}
}


if __name__ == "__main__":
    from multiprocessing import Pool

    prec_types = ["Riesz", "Riesz_Lite"]

    k = [10.0**N for N in range(-6, 2)]
    orders = [1]
    Ns = [2**N for N in range(4, 8)]
    eps = [10**N for N in range(-3, 0)]
    quads = [True, False]

    opts = [(param_mg, quad, order, N, k_iter, eps_iter, prec_type)
            for quad in quads
            for order in orders
            for k_iter in k
            for N in Ns
            for eps_iter in eps
            for prec_type in prec_types]

    p = Pool(16)
    results_list = p.map(f, opts)

    results = {}
    for quad in [True, False]:
        results[quad] = {}
        for prec_type in prec_types:
            results[quad][prec_type] = {}
            for order in orders:
                results[quad][prec_type][order] = {}
                for eps_iter in eps:
                    results[quad][prec_type][order][eps_iter] = {}
                    for N in Ns:
                        results[quad][prec_type][order][eps_iter][N] = {}
                        for k_iter in k:
                            results[quad][prec_type][order][eps_iter][N][k_iter] = 0

    for (_, quad, order, N, k_iter, eps_iter, prec_type), r in zip(opts, results_list):
        results[quad][prec_type][order][eps_iter][N][k_iter] = r

    import pickle


    with open('tide_RT_inexact.p', 'wb') as f:
        pickle.dump(results, f)
