import firedrake
from firedrake.petsc import PETSc
import numpy as np
from pyop2.datatypes import ScalarType
import loopy


def kernel_gen(key):
    kernel_dict = {
        'triangle' : loopy.make_function("[] -> {[]}",
                                         """
A[0, 0] = 0.0
A[0, 1] = -1.0
A[0, 2] = 1.0
A[1, 0] = -1.0
A[1, 1] = 0.0
A[1, 2] = 1.0
A[2, 0] = -1.0
A[2, 1] = 1.0
A[2, 2] = 0.0
""",
                                         [loopy.GlobalArg("A", dtype=ScalarType,
                                                          shape=(3, 3))],
                                         name="my_kernel",
                                         seq_dependencies=True,
                                         target=loopy.CTarget()),
        'quadrilateral' : loopy.make_function("[] -> {[]}",
                                         """
A[0, 0] = -1.0
A[0, 1] = 1.0
A[0, 2] = 0.0
A[0, 3] = 0.0
A[1, 0] = 0.0
A[1, 1] = 0.0
A[1, 2] = -1.0
A[1, 3] = 1.0
A[2, 0] = -1.0
A[2, 1] = 0.0
A[2, 2] = 1.0
A[2, 3] = 0.0
A[3, 0] = 0.0
A[3, 1] = -1.0
A[3, 2] = 0.0
A[3, 3] = 1.0

""",
                                         [loopy.GlobalArg("A", dtype=ScalarType,
                                                          shape=(4, 4))],
                                         name="my_kernel",
                                         seq_dependencies=True,
                                         target=loopy.CTarget()),        
        'tetrahedron' :
        loopy.make_function("[] -> {[]}",
                            """
A[0, 0] = 0.0
A[0, 1] = 0.0
A[0, 2] = -1.0
A[0, 3] = 1.0

A[1, 0] = 0.0
A[1, 1] = -1.0
A[1, 2] = 0.0
A[1, 3] = 1.0

A[2, 0] = 0.0
A[2, 1] = -1.0
A[2, 2] = 1.0
A[2, 3] = 0.0

A[3, 0] = -1.0
A[3, 1] = 0.0
A[3, 2] = 0.0
A[3, 3] = 1.0

A[4, 0] = -1.0
A[4, 1] = 0.0
A[4, 2] = 1.0
A[4, 3] = 0.0

A[5, 0] = -1.0
A[5, 1] = 1.0
A[5, 2] = 0.0
A[5, 3] = 0.0
""",
                            [loopy.GlobalArg("A", dtype=ScalarType,
                                             shape=(6, 4))],
                            name="my_kernel",
                            seq_dependencies=True,
                            target=loopy.CTarget()),
    }
    return kernel_dict[key]


class HypreAMS(firedrake.PCBase):
    def initialize(self, pc):
        from pyop2 import op2, petsc_base
        from firedrake import FunctionSpace
        from firedrake.assemble import allocate_matrix, create_assembly_callable

        _, P = pc.getOperators()

        context = P.getPythonContext()
        prefix = pc.getOptionsPrefix()

        test, trial = context.a.arguments()
        #print(dir(test.function_space()))
        #print(test.function_space()[1].ufl_element().family())
        element = str(test.function_space().ufl_element().family()) #not sure this is right
        mesh = test.function_space().mesh()

        TestSpace = FunctionSpace(mesh, element, 1)
        P1 = FunctionSpace(mesh, "Lagrange", 1)

        TestSpace_map = TestSpace.cell_node_map()
        P1_map = P1.cell_node_map()

        edges = op2.Set(mesh.num_edges())
        vertices = op2.Set(mesh.num_vertices())

        dedges = op2.DataSet(edges, dim=1)
        dvertices = op2.DataSet(vertices, dim=1)

        dedges.set = TestSpace_map.toset
        dvertices.set = P1_map.toset

        sparsity = op2.Sparsity((dedges, dvertices),
                                [(TestSpace_map,P1_map)])
        my_matrix = petsc_base.Mat(sparsity, float)

        #Should I convert this to a function outside of the HypreAMS class?
        mesh_key = str(mesh.ufl_cell())

        my_kernel = op2.Kernel(kernel_gen(mesh_key), "my_kernel")

        op2.par_loop(my_kernel, mesh.cell_set,
                     my_matrix(op2.WRITE, (TestSpace.cell_node_map(),
                                           P1.cell_node_map())))

        my_matrix.assemble()
        my_matrix = my_matrix.handle
        
        if not context.on_diag:
            raise ValueError("Only makes sense to invert diagonal block")

        mat_type = PETSc.Options().getString(prefix + "assembled_mat_type", "aij")
        self.P = allocate_matrix(context.a, bcs=context.row_bcs,
                                 form_compiler_parameters=context.fc_params,
                                 mat_type=mat_type)
        self._assemble_P = create_assembly_callable(context.a, tensor=self.P,
                                                    bcs=context.row_bcs,
                                                    form_compiler_parameters=context.fc_params,
                                                    mat_type=mat_type)
        self._assemble_P()
        self.mat_type = mat_type
        Pmat = self.P.petscmat
        Pmat.setNullSpace(P.getNullSpace())
        tnullsp = P.getTransposeNullSpace()
        if tnullsp.handle != 0:
            Pmat.setTransposeNullSpace(tnullsp)

        pc = PETSc.PC().create()
        pc.setOptionsPrefix(prefix+"assembled_")
        pc.setOperators(Pmat, Pmat)

        pc.setType('hypre')
        pc.setHYPREType('ams')
        pc.setHYPREDiscreteGradient(my_matrix)
        pc.setCoordinates(np.reshape(
            mesh._plex.getCoordinates().getArray(),
            (-1,mesh.geometric_dimension())))
        pc.setUp()

        self.pc = pc

    def apply(self, pc, x, y):
        self.pc.apply(x, y)

    def applyTranspose(self, pc, x, y):
        self.pc.applyTranspose(x, y)

    def view(self, pc, viewer=None):
        super(HypreAMS, self).view(pc, viewer)
        viewer.pushASCIITab()
        if hasattr(self, "pc"):
            viewer.printfASCII("PC to apply inverse\n")
            self.pc.view(viewer)
        viewer.popASCIITab()

    def update(self, pc):
        pass
