import pickle

# spit out files for Riesz and Riesz_lite IP
# when N=128, for each epsilon, vary the k

# for cell in ['tri', 'quad']:
#     with open('tide_RT_'+cell+'.p', 'rb') as f:
#         res = pickle.load(f)
#     for pctype in ['Mass', 'Riesz', 'Riesz_Lite']:
#         foo = res[pctype]
#         for order in foo:
#             foo_order = foo[order]
#             for eps in foo_order:
#                 with open(cell+'.'+pctype+'.deg'+str(order)+'.eps'+str(eps)+'.csv', 'w') as fout:
#                     fout.write('k,iterations\n')
#                     for blah in foo_order[eps][128]:
#                         fout.write('%e,%d\n'%(blah, foo_order[eps][128][blah]))

# Now do mesh refinement studies for various $k$ for fixed epsilon      
# for cell in ['tri', 'quad']:
#     with open('tide_RT_'+cell+'.p', 'rb') as f:
#         res = pickle.load(f)
#     for pctype in ['Mass', 'Riesz', 'Riesz_Lite']:
#         foo = res[pctype]
#         for order in foo:
#             blah = foo[order][0.01]
#             for k in sorted(blah[2].keys()):
#                 with open(cell+'.'+pctype+'.deg'+str(order)+'.k'+str(k)+'.csv', 'w') as fout:
#                     fout.write('N,iterations\n')
#                     for bar in blah:
#                         fout.write('%d,%d\n'%(bar, blah[bar][k])) 

# Now do mesh refinement studies for various $k$ for fixed epsilon      
with open('tide_Sminus_quad.p', 'rb') as f:
    res = pickle.load(f)
    for pctype in ['Riesz', 'Riesz_Lite']:
        foo = res[pctype]
        for order in foo:
            blah = foo[order][0.01]
            for k in sorted(blah[2].keys()):
                with open('Sminus.'+pctype+'.deg'+str(order)+'.k'+str(k)+'.csv', 'w') as fout:
                    fout.write('N,iterations\n')
                    for bar in blah:
                        fout.write('%d,%d\n'%(bar, blah[bar][k])) 


# with open('tide_RT_inexact.p', 'rb') as f:
#     res = pickle.load(f)

# for quad, cell in zip([False, True], ['tri', 'quad']):
#     foo = res[quad]
#     for pctype in ['Riesz', 'Riesz_Lite']:
#         foo = res[quad][pctype]
#         for order in foo:
#             blah = foo[order][0.01]
#             for k in sorted(blah[16].keys()):
#                 with open(cell+'.mg.'+pctype+'.deg'+str(order)+'.k'+str(k)+'.csv', 'w') as fout:
#                     fout.write('N,iterations\n')
#                     for bar in blah:
#                         fout.write('%d,%d\n'%(bar, blah[bar][k])) 

# with open('tide_RT_nonlin.p', 'rb') as f:
#     res = pickle.load(f)

# for quad, cell in zip([False, True], ['tri', 'quad']):
#     foo = res[quad]
#     for pctype in ['Riesz', 'Riesz_Lite']:
#         foo = res[quad][pctype]
#         for order in foo:
#             blah = foo[order][0.01]
#             for k in sorted(blah[16].keys()):
#                 with open(cell+'.nonlin.'+pctype+'.deg'+str(order)+'.k'+str(k)+'.csv', 'w') as fout:
#                     fout.write('N,Nonlin,Lin\n')
#                     for bar in blah:
#                         fout.write('%d,%d,%d\n'%(bar, blah[bar][k][0], blah[bar][k][1])) 

