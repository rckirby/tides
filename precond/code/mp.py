from firedrake import *

N = 16
base_msh = UnitSquareMesh(N, N, quadrilateral=True)
mh = MeshHierarchy(base_msh, 4)
msh = mh[0]

V = FunctionSpace(msh, "BDMCF", 1)
W = FunctionSpace(msh, "DQ", 0)
Z = V * W

# We obtain test and trial functions on the subspaces of the mixed function
# spaces as follows: ::

u, p = TrialFunctions(Z)
v, w = TestFunctions(Z)

# Next we declare our source function ``f`` over the DG space and initialise it
# with our chosen right hand side function value. ::

x, y = SpatialCoordinate(msh)
pex = sin(pi*x) * sin(pi*y)
f = -div(grad(pex))

# After dropping the vanishing boundary term on the right hand side, the
# bilinear and linear forms of the variational problem are defined as: ::

a = inner(u, v)*dx - inner(p, div(v))*dx + inner(div(u), w)*dx
L = inner(f, w)*dx

uph = Function(Z)

#apc = inner(u, v) * dx + inner(div(u), div(v)) * dx + inner(p, w) * dx

problem = LinearVariationalProblem(a, L, uph)

# params = {"snes_type": "ksponly",
#           "ksp_monitor": None,
#           "mat_type": "matfree",
#           "pmat_type": "matfree",
#           "ksp_type": "minres",
#           "pc_type": "fieldsplit",
#           "pc_fieldsplit_type": "additive",
#           "fieldsplit_0": {"ksp_monitor": None,
#                            "ksp_type": "cg",
#                            "ksp_rtol": 1.0e-10,
#                            "ksp_atol": 0.0,
#                            "ksp_norm_type": "unpreconditioned",
#                            #"ksp_monitor_true_residual": None,
#                            "pc_type": "mg",
#                            "pc_mg_type": "full",
#                            "mg_levels_ksp_type": "richardson",
#                            "mg_levels_ksp_norm_type": "unpreconditioned",
#                            #"mg_levels_ksp_monitor_true_residual": None,
#                            "mg_levels_ksp_richardson_scale": 1/3,
#                            "mg_levels_ksp_max_it": 1,
#                            "mg_levels_ksp_convergence_test": "skip",
#                            "mg_levels_pc_type": "python",
#                            "mg_levels_pc_python_type": "firedrake.PatchPC",
#                            "mg_levels_patch_pc_patch_save_operators": True,
#                            "mg_levels_patch_pc_patch_partition_of_unity": False,    
#                            "mg_levels_patch_pc_patch_construct_type": "star",
#                            "mg_levels_patch_pc_patch_construct_dim": 0,
#                            "mg_levels_patch_pc_patch_sub_mat_type": "seqdense",
#                            "mg_levels_patch_sub_ksp_type": "preonly",
#                            "mg_levels_patch_sub_pc_type": "lu",
#                            "mg_coarse_pc_type": "python",
#                            "mg_coarse_pc_python_type": "firedrake.AssembledPC",
#                            "mg_coarse_assembled_pc_type": "lu",
#                            "mg_coarse_assembled_pc_factor_mat_solver_type": "mumps"},
#           "fieldsplit_1": {
#               "ksp_type": "cg",
#               "pc_type": "python",
#               "pc_python_type": "firedrake.AssembledPC",
#               "assembled_pc_type": "jacobi"}
#           }

params = {"snes_type": "ksponly",
          "mat_type": "aij",
          "pc_type": "none",
          "pc_factor_shift_type": "inblocks",
          "ksp_monitor": None,
          "ksp_converged_reason": None,
          "ksp_type": "gmres"}

solver = LinearVariationalSolver(problem, solver_parameters=params)

solver.solve()
uh, ph = split(uph)


print(sqrt(assemble((pex-ph)**2*dx)))
