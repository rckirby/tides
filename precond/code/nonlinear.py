from firedrake import *
import numpy as np
from petsc4py import *

import HypreAMS_preconditioner, HDivRiesz

def tide(parameters, quad, order, n, k, eps, prec_type):
    mesh = UnitSquareMesh(n//16, n//16, quadrilateral=quad)
    mh = MeshHierarchy(mesh, 4)
    mesh = mh[-1]
    if quad:
        V = FunctionSpace(mesh, "SminusF", order)
        Q = FunctionSpace(mesh, "DPC", order-1)
    else:
        V = FunctionSpace(mesh, "RT", order)
        Q = FunctionSpace(mesh, "DG", order-1)

    Z=V*Q

    x, y = SpatialCoordinate(mesh)

    up_ = Function(Z)
    up = Function(Z)

    vq = TestFunction(Z)

    u_, p_ = split(up_)
    u, p = split(up)
    v, q = split(vq)

    k = Constant(k)
    Eps = Constant(eps)
    Beta = Constant(0.1)
    C = Constant(1.0)

    f = Constant(1.0)

    beps2 = Beta / Eps / Eps

    Flin = (
        inner(u, v) * dx
        + k / Eps * f * inner(perp(u),v) * dx
        - k * beps2 * inner(p, div(v)) * dx
        + beps2 * inner(p, q) * dx
        + k * beps2 * inner(div(u), q) * dx
        - beps2 * inner(sin(np.pi*x)*cos(np.pi*y),q)*dx #g
    )
    F = Flin +  k * inner(dot(u, u) * u,v) * dx


    bcs = [DirichletBC(Z.sub(0), 0, (1,2,3,4))]

    uu, pp = TrialFunctions(Z)
    if prec_type == "Riesz":
        Jpc = (
            (Constant(1.0) + C * k) * inner(uu,v)*dx
            + k**2 * beps2 * inner(div(uu),div(v)) *dx
            + beps2 * inner(pp,q)*dx
        )
        appctx={'alpha': Constant(1.0+C*k),
                'beta': k**2 * beps2}
    elif prec_type == "Riesz_Lite":
        Jpc = (
            inner(uu,v)*dx
             + k**2 * beps2 * inner(div(uu),div(v)) *dx
             + beps2 * inner(pp,q)*dx
        )
        appctx={'alpha': Constant(1.0),
                'beta': k**2 * beps2}


    solve(Flin==0, up, bcs=bcs,
          solver_parameters={'snes_type': 'ksponly',
                             'mat_type': 'aij',
                             'ksp_type': 'preonly',
                             'pctype': 'lu'})

    up_problem = NonlinearVariationalProblem(F, up, bcs=bcs, Jp=Jpc)
    up_solver = NonlinearVariationalSolver(up_problem,
                                           solver_parameters = parameters, appctx=appctx)

    try:
        up_solver.solve()
        iterations = (up_solver.snes.getIterationNumber(),
                      up_solver.snes.getLinearSolveIterations())
    except:
        iterations = (-1,-1)
    return(iterations)

# for multiprocessing
def f(x):
    (param, quad, order, N, k, eps, prec_type) = x
    print("Running k=%e, N=%d, eps=%e, prec=%s"%(k, N, eps, prec_type)) 
    return tide(param, quad, order, N, k, eps, prec_type)


param_lu = {"mat_type": "aij",
            "snes_type": "ksponly",
            "ksp_type": "gmres",
            "ksp_gmres_restart": 100,
            "pc_type": "lu"
            }


if __name__ == "__main__":
    from multiprocessing import Pool

    prec_types = ["Riesz", "Riesz_Lite"]

    k = [10.0**N for N in range(-6, 2)]
    orders = [1]
    Ns = [2**N for N in range(4, 8)]
    eps = [10**N for N in range(-3, 0)]
    quads = [True, False][0:]

    opts = [(param_lu, quad, order, N, k_iter, eps_iter, prec_type)
            for quad in quads
            for order in orders
            for k_iter in k
            for N in Ns
            for eps_iter in eps
            for prec_type in prec_types]

    p = Pool(6)
    results_list = p.map(f, opts)

    results = {}
    for quad in [True, False]:
        results[quad] = {}
        for prec_type in prec_types:
            results[quad][prec_type] = {}
            for order in orders:
                results[quad][prec_type][order] = {}
                for eps_iter in eps:
                    results[quad][prec_type][order][eps_iter] = {}
                    for N in Ns:
                        results[quad][prec_type][order][eps_iter][N] = {}
                        for k_iter in k:
                            results[quad][prec_type][order][eps_iter][N][k_iter] = None

    for (_, quad, order, N, k_iter, eps_iter, prec_type), r in zip(opts, results_list):
        results[quad][prec_type][order][eps_iter][N][k_iter] = r

    import pickle


    with open('tide_RT_nonlin.p', 'wb') as f:
        pickle.dump(results, f)
