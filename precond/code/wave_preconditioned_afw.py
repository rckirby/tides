from firedrake import *
import numpy as np
from petsc4py import *

from HypreAMS_preconditioner import HypreAMS

def wave(n, k, parameters):
    mesh = UnitSquareMesh(n,n)
    
    order = 1
    
    V = FunctionSpace(mesh, "RT", order)
    Q = FunctionSpace(mesh, "DG", order-1)
    Z=V*Q

    x,y = SpatialCoordinate(mesh)
    
    up = Function(Z)
    
    vq = TestFunction(Z)

    u, p = split(up)
    v, q = split(vq)
    

    k = Constant(k)

    # This is the system for one step of the acoustic wave equation
    # in mixed form assuming we've used backward Euler, Crank-Nicolson, etc
    # as a time-discretization.  I've put some generic RHS on one of
    # the equation so we get a nonzero solution to the linear system.

   
    F = (
        inner(u, v) * dx
        + k * inner(p, div(v)) * dx
        + k * inner(div(u), q) * dx
        - inner(p, q) * dx
        - inner(sin(np.pi*x)*cos(np.pi*y),q)*dx 
    )

    uu, pp = TrialFunctions(Z)

    # Riesz map preconditioner incorporating time step weight
    # Arnold/Falk/Winther show (really, assert) that the Jacobian of F
    # has a condition number bound independent of the parameter k
    # if we work in the norm defined by this weighted inner product:
        
    Jpc = (
        inner(uu,v)*dx
        + (inner(div(uu),div(v))*k**2)*dx
        + inner(pp,q)*dx
    )

    # Note that if we don't pass in the keyword argument Jp = Jpc then
    # Firedrake and PETSc will use the original Jacobian to define the
    # preconditioner, which is not what we want in this case.
    up_problem = NonlinearVariationalProblem(F, up, Jp=Jpc)
    up_solver = NonlinearVariationalSolver(up_problem,
                                           solver_parameters=parameters)
    
    up_solver.solve()
    iter = up_solver.snes.getLinearSolveIterations()
    print(iter)
    return(iter)

    

if __name__ == "__main__":
    param = {
        # Don't assemble the global sparse matrix.
        "mat_type": "matfree",
        # This problem is linear, so no Newton iteration
        "snes_type": "ksponly",
        "ksp_type": "gmres",
        # Prints out residual history
        "ksp_monitor": None,
        # PETSc-speak for block preconditioners
        "pc_type": "fieldsplit",
        # Additive = "block Jacobi" or "use the diagonal blocks"
        "pc_fieldsplit_type": "additive",
        "pc_fieldsplit_0_fields": "0",
        "pc_fieldsplit_1_fields": "1",
        # The 0,0 block of the preconditioner
        # is the (weighted) H(div) inner product
        # and amenable to the Hypre AMS preconditioner.
        "fieldsplit_0": {
            # The system is SPD, so conjugate gradients are
            # natural if we want a (nearly) exact application of
            # the preconditioner.  In our case, it also seems
            # sufficient to apply a sweep of AMS.
            "ksp_type": "preonly",
            # Tell PETSc that we've implemented a custom
            # PC in Python and where to find it.
            "pc_type": "python",
            "pc_python_type": "__main__.HypreAMS"},
        # The 1 block is in fact diagonal in the lowest order
        # case so one step of Jacobi converges.  Note that
        # if we leave the outer matrix unassembled, we'll need to
        # force the system to build the diagonal block
        "fieldsplit_1": {
            "ksp_type": "preonly",
            "pc_type": "python",
            "pc_python_type": "firedrake.AssembledPC",
            "assembled_pc_type": "jacobi"}
    }

    wave(10, 0.01, param)
