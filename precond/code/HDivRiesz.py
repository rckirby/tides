from firedrake import AuxiliaryOperatorPC, inner, div, dx, DirichletBC, Constant


class Riesz(AuxiliaryOperatorPC):
  def form(self, pc, test, trial):
    _, P = pc.getOperators()
    context = P.getPythonContext()
    alpha = context.appctx.get('alpha', Constant(1.0))
    beta = context.appctx.get('beta', Constant(1.0))
    a = alpha * inner(test, trial)*dx + beta * inner(div(test), div(trial))*dx
    return (a, DirichletBC(test.function_space(), 0, "on_boundary"))
